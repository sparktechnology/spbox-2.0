
//disable the "enter" key press, since it might cause log out in some cases
function stopRKey(evt) {
  var evt = (evt) ? evt : ((event) ? event : null);
  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
  if ((evt.keyCode == 13) && (node.type=="text"))  {return false;}
}

//print out the welcome message
if (window.console) {
  console.log("Welcome to WeTicket!");
}

//the onload functions
$(document).ready(function(){
	$("#query-button").click(function(){
		var keyword = $("#query-input").val();
		location.href = "/query?keyword=" + keyword;

	});

	// Very basic implementation of message because it is used by admin only
	var messagePolling = false;
	var processMessage = function(){
		if(localStorage.getItem("userRole") === "ADMIN" && !messagePolling){
			messagePolling = true;
			$.rest("GET", "/api/v1/getUnprocessedInfo", {}, function(r){
				messagePolling = false;
				var reqs = r.publishRequests;
				if(!isNaN(reqs) && reqs > 0){
					$(".mobile-notification").removeClass("hidden");
					$(".notification-icon").removeClass("hidden");
					$(".notification-container").removeClass("hidden");
					$(".unprocessed-info").html(reqs);
				} else {
					$(".mobile-notification").addClass("hidden");
					$(".notification-icon").addClass("hidden");
					$(".notification-container").addClass("hidden");
				}
			}, function(e){
				messagePolling = false;
			});
		}
	};
	setInterval(processMessage, 2000);
	processMessage();
	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	})

	$(".logout-button").click(function(){

		facebookLogOut();
		googleSignOut();

		$.rest("GET", "/api/v1/logout", {},
			function(data){
				$("#user-info").html("请在此处订票邮箱");

				$("#login-email-menu").val("");
				$("#login-password-menu").val("");
				window.location.assign("/");
				localStorage.clear();
			},
			function(data){
				alert("退出登陆失败");
			}
		);

	});
});

$.rest = function(method, uri, data, success, error)
{
	$.ajax({

		type: method,

		url: uri,

		dataType: 'json',

		data: method.toUpperCase() === "GET" ? data : (jQuery.isPlainObject(data) ? JSON.stringify(data) : data),

		contentType: "application/json; charset=utf-8",

		success: function (data) {
			// we go here because everything is OK
			if (success) {
				success(data);
			}
		},

		error: function (data) {
			// something wrong in backend
			if (error) {
				error(data);
			}
		}
	});
}


//First, checks if it isn't implemented yet.
if (!String.prototype.format) {
  String.prototype.format = function() {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function(match, number) { 
      return typeof args[number] != 'undefined'
        ? args[number]
        : match
      ;
    });
  };
}

function getPrimaryColor(url, callback){
	var colorThief = new ColorThief();
	var img = document.createElement('img');
	img.src = url;
	img.onload = function(){
		var color = colorThief.getColor(img);
		callback(color);
	}
}

