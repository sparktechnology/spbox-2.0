
/*******************************************************************
 *  Third party login app/client ID definition
 */

var facebookAppId = '1482694155367364';


/*******************************************************************
 *  Facebook Login
 */


  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
        FB.api('/me', {fields: 'email, id, name, gender'}, function(response) {
        // Facebook API to retrieve user info
            userFacebookLoginOrCreateNew(response.email, response.name, response.gender, response.id);
        });
    });
  }
  function userFacebookLoginOrCreateNew(email, name, gender, facebookId){
      var user_data = { "email": email, "fbId":facebookId };
      $.rest("GET", "/api/v1/fbLogin", user_data,
          function(data){
              if (data.needToRegister){
                  registerNewUserWithFacebookId(email, name, gender, facebookId);
              } else {
                  logInSuccess(data);
              }
          },
          function(data){
              alert("邮箱或密码错误！");
          }
      );
  }

  function registerNewUserWithFacebookId(email, name, gender, facebookId){
      var data = {"email": email,  "name": name, "gender":gender, "facebookId":facebookId};
      $.rest("POST", "/api/v1/registerWithFacebookId", data,
          function(data){
              logInSuccess(data);
          },
          function(rsp){
              alert(rsp.responseText);
          }
      );
  }

  function facebookLogOut(){
      FB.getLoginStatus(function(response) {
          if (response.status === 'connected') {
              FB.logout(function(response) {
                  //user logged out of FB
                  localStorage.clear();
                  console.log("logged out");
              });
          }
      });
  }


  window.fbAsyncInit = function() {
  FB.init({
    appId      : facebookAppId,
    cookie     : true,  // enable cookies to allow the server to access
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.2' // use version 2.2
  });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));


/*******************************************************************
 *  Google Login
 */


 function googleLogin(googleClientId) {
      gapi.load('auth2', function(){
      // Retrieve the singleton for the GoogleAuth library and set up the client.
      auth2 = gapi.auth2.init({
          client_id: googleClientId,
          cookiepolicy: 'single_host_origin',
          scope: 'profile email'
      });
      attachSignin(document.getElementById('googleLoginBtn'));
    });
  };

  function attachSignin(element) {
      auth2.attachClickHandler(element, {},
          function(googleUser) {
              var googleToken = googleUser.getAuthResponse().id_token;
              var profile = googleUser.getBasicProfile();
              var googleId = profile.getId();
              var userName = profile.getName();
              var email = profile.getEmail();

              userGoogleLoginOrCreateNew(email, googleToken, userName, googleId);
          }, function(error) {
              alert(JSON.stringify(error, undefined, 2));
          }
      );
  }

  function googleSignOut() {
      var auth2 = gapi.auth2.getAuthInstance();
      if(auth2.isSignedIn.get()){
          auth2.signOut().then(function () {
              localStorage.clear();
              console.log('User signed out.');
          });
      }
  }

  function userGoogleLoginOrCreateNew(email, googleToken, userName, googleId){
      var user_data = { "email": email, "googleId":googleId, "googleToken" : googleToken};
      $.rest("GET", "/api/v1/googleLogin", user_data,
          function(data){
              if (data.needToRegister){
                  registerNewUserWithGoogleId(email, googleToken, userName, googleId);
              } else {
                  logInSuccess(data);
              }
          },
          function(data){
              alert("邮箱或密码错误！");
          }
      );
  }

  function registerNewUserWithGoogleId(email, googleToken, userName, googleId){
      var data = {"email": email,  "name": userName, "googleId":googleId, "googleToken":googleToken};
      $.rest("POST", "/api/v1/registerWithGoogleId", data,
          function(data){
              logInSuccess(data);
          },
          function(rsp){
              alert(rsp.responseText);
          }
      );
  }

  function logInSuccess(data){
      window.loginUser = data.email;
      localStorage.setItem("userRole", data.role);
      localStorage.setItem("userImage", data.userImage);
      window.location.reload();
  }