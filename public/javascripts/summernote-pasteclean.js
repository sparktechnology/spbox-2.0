/**
 * Summernote PasteClean
 *
 * This is a plugin for Summernote (www.summernote.org) WYSIWYG editor.
 * It will clean up the content your editors may paste in for unknown sources
 * into your CMS. It strips Word special characters, style attributes, script
 * tags, and other annoyances so that the content can be clean HTML with
 * no unknown hitchhiking scripts or styles.
 *
 * @author Jason Byrne, FloSports <jason.byrne@flosports.tv>
 *
 */

(function (factory) {
  /* global define */
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['jquery'], factory);
  } else {
    // Browser globals: jQuery
    factory(window.jQuery);
  }
}(function ($) {

    var badTags = [
            'font',
            'style',
            'embed',
            'param',
            'script',
            'html',
            'body',
            'head',
            'meta',
            'img',
            'title',
            'link',
            'iframe',
            'applet',
            'noframes',
            'noscript',
            'form',
            'input',
            'select',
            'option',
            'std',
            'xml:',
            'st1:',
            'o:',
            'w:',
            'v:'
        ],
        badAttributes = [
            'style',
            'start',
            'charset'
        ];

    $.summernote.addPlugin({
        name: 'pasteClean',
        init: function(layoutInfo) {

            var $note = layoutInfo.holder();

            $note.on('summernote.paste', function(e, evt) {
                evt.preventDefault();
                // Capture pasted data
                var text = evt.originalEvent.clipboardData.getData('text/plain'),
                    html = evt.originalEvent.clipboardData.getData('text/html');
                // Clean up html input
                if (html) {
                    // Regular expressions
                    var tagStripper         = new RegExp('<[ /]*(' + badTags.join('|') + ')[^>]*>', 'gi'),
                        attributeStripper   = new RegExp(' (' + badAttributes.join('|') + ')(="[^"]*"|=\'[^\']*\'|=[^ ]+)?', 'gi'),
                        commentStripper     = new RegExp('<!--(.*)-->', 'g');

                    // clean it up
                    html = html.toString()
                        // Remove comments
                        .replace(commentStripper, '')
                        // Remove unwanted tags
                        .replace(tagStripper, '')
                        // remove unwanted attributes
                        .replace(attributeStripper, ' ')
                        // remove Word classes
                        .replace(/( class=(")?Mso[a-zA-Z]+(")?)/g, ' ')
                        // remove whitespace (space and tabs) before tags
                        .replace(/[\t ]+\</g, "<")
                        // remove whitespace between tags
                        .replace(/\>[\t ]+\</g, "><")
                        // remove whitespace after tags
                        .replace(/\>[\t ]+$/g, ">")
                        // smart single quotes and apostrophe
                        .replace(/[\u2018\u2019\u201A]/g, "'")
                        // smart double quotes
                        .replace(/[\u201C\u201D\u201E]/g, '"')
                        // ellipsis
                        .replace(/\u2026/g, '...')
                        // dashes
                        .replace(/[\u2013\u2014]/g, '-');
                }
                if(/\s/.test(html)){ // wipe html if html contains only whitespaces
                    html='';
                }
                // Do the paste
                var $dom = $('<div class="pasted"/>').html(html || text);
                $note.summernote('insertNode', $dom[0]);
                return false;
            });

        }
    });
}));
