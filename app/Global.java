import akka.actor.Cancellable;
import org.joda.time.DateTime;
import org.joda.time.Seconds;
import play.*;
import play.db.jpa.JPA;
import play.libs.Akka;
import scala.concurrent.duration.Duration;
import spbox.cep.ComplexEventProcessor;
import spbox.model.entity.activity.Activity;
import spbox.web.utilities.TimeUtils;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Global extends GlobalSettings {

    private Cancellable scheduler;

	@Override
    public void onStart(Application app) {

        scheduler = Akka.system().scheduler().schedule(
                Duration.create(nextExecutionInSeconds(8, 0), TimeUnit.SECONDS),
                Duration.create(24, TimeUnit.HOURS),
                () -> {

                    JPA.withTransaction(() -> {
                        updateActivityStatus();
                    });

                    Logger.info(" Scheduled Task - Activity Status Update ");

                }, Akka.system().dispatcher()
        );
    }

    @Override
    public void onStop(Application application) {
        //Stop the scheduler
        if (scheduler != null) {
            scheduler.cancel();
        }

        ComplexEventProcessor.destoryProvider();
    }

    private void updateActivityStatus(){
        String sql = buildQuerySQL(Activity.ActivityStatusEnum.PUBLISH);
        List<Activity> activities = (List<Activity>) JPA.em().createQuery(sql).getResultList();

        for(Activity activity: activities){
            if(TimeUtils.getUTCDate(activity.endTime).before(TimeUtils.getUTCDate(new Date()))){
                activity.status = Activity.ActivityStatusEnum.EXPIRED;
                JPA.em().merge(activity);
            }
        }
    }

    private String buildQuerySQL(Activity.ActivityStatusEnum... status){
        String sql = "select distinct a from Activity a where ";

        for (Activity.ActivityStatusEnum s : status) {
            sql = sql + "a.status = '" + s + "'";
            if (Arrays.asList(status).indexOf(s) != Arrays.asList(status).size() - 1)
                sql = sql + " or ";
        }
        return sql;
    }

    private static int nextExecutionInSeconds(int hour, int minute){
        return Seconds.secondsBetween(
                new DateTime(),
                nextExecution()
        ).getSeconds();
    }

    private static DateTime nextExecution(){
        DateTime next = new DateTime().withTimeAtStartOfDay();

        return (next.isBeforeNow())
                ? next.plusHours(24)
                : next;
    }
    
}