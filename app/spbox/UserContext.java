package spbox;

import play.mvc.Http;
import spbox.model.entity.user.User;
import spbox.web.utilities.StringUtils;

public class UserContext {

    protected static String UserContextKey = "CURRENT_AUTHENTICATED_USER";

    public static String GetCurrentUserEmail(){
        return Http.Context.current().session().getOrDefault("email", StringUtils.BLANK);
    }

    public static Boolean isCurrentUserAdministrator(){
        String role = Http.Context.current().session().getOrDefault("role", StringUtils.BLANK);
        if(role.equals(User.UserRoleEnum.ADMIN.name()))
            return true;
        else
            return false;
    }

    public static Boolean isCurrentUserOrganizer(){
        String role = Http.Context.current().session().getOrDefault("role", StringUtils.BLANK);
        if(role.equals(User.UserRoleEnum.ORGANIZER.name()))
            return true;
        else
            return false;
    }

    public static Boolean isCurrentUserBlocked(){
        String role = Http.Context.current().session().getOrDefault("role", StringUtils.BLANK);
        if(role.equals(User.UserRoleEnum.BLOCKED.name()))
            return true;
        else
            return false;
    }

    public static Boolean isCurrentUserGeneralUser(){
        String role = Http.Context.current().session().getOrDefault("role", StringUtils.BLANK);
        if(role.equals(User.UserRoleEnum.GENERAL.name()))
            return true;
        else
            return false;
    }

    public static String GetCurrentUserRole(){
        return Http.Context.current().session().getOrDefault("role", StringUtils.BLANK);
    }

    public static void SetCurrentUser(User user)
    {
        if(user == null){
            Http.Context.current().session().clear();
            Http.Context.current().args.remove(UserContextKey);
        }
        else {
            Http.Context.current().session().put("email", user.email);
            Http.Context.current().session().put("role", user.role.name());

            Http.Context.current().args.put(UserContextKey, user);
        }
    }

    public static void removeCurrentUser(User user)
    {
        if(user != null){
            Http.Context.current().session().clear();
            Http.Context.current().args.remove(UserContextKey);
        }
    }

}
