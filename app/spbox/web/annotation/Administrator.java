package spbox.web.annotation;

import play.libs.F;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;
import play.twirl.api.Html;
import spbox.UserContext;
import spbox.model.entity.user.User;
import spbox.web.view.html.info;
import spbox.web.view.html.login;

public class Administrator extends play.mvc.Action.Simple {
    public F.Promise<Result> call(Http.Context ctx) throws Throwable {

        if(UserContext.GetCurrentUserEmail().isEmpty() || UserContext.GetCurrentUserRole().isEmpty()){
            Result unauthorized = Results.ok(login.render("登录"));
            return F.Promise.pure(unauthorized);
        }

        if(UserContext.GetCurrentUserRole().equals(User.UserRoleEnum.BLOCKED.name())){
            Result unauthorized = ok(login.render("登录"));
            return F.Promise.pure(unauthorized);
        }
        if(!UserContext.GetCurrentUserRole().equals(User.UserRoleEnum.ADMIN.name())){
            String shtml = "您不是管理员或活动组织者，无法执行该操作";
            Result notAllowed = Results.forbidden(info.render("非法操作", Html.apply(shtml), null, null));
            return F.Promise.pure(notAllowed);
        }
        return delegate.call(ctx);
    }
}
