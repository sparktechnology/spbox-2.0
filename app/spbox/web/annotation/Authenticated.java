package spbox.web.annotation;


import play.libs.F;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;
import spbox.UserContext;
import spbox.web.view.html.login;

public class Authenticated extends play.mvc.Action.Simple {
    public F.Promise<Result> call(Http.Context ctx) throws Throwable {

        if(UserContext.GetCurrentUserEmail().isEmpty() || UserContext.GetCurrentUserRole().isEmpty()){
            Result unauthorized = Results.ok(login.render("登录"));
            return F.Promise.pure(unauthorized);
        }

        return delegate.call(ctx);
    }
}
