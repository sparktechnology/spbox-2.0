package spbox.web.annotation;

import play.libs.F;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;
import play.twirl.api.Html;
import spbox.UserContext;
import spbox.web.view.html.info;
import spbox.web.view.html.login;

public class Organizer extends play.mvc.Action.Simple {
    public F.Promise<Result> call(Http.Context ctx) throws Throwable {

        if (UserContext.isCurrentUserBlocked()) {
            Result unauthorized = Results.ok(login.render("登录"));
            return F.Promise.pure(unauthorized);
        }
        if (!UserContext.isCurrentUserOrganizer() && !UserContext.isCurrentUserAdministrator()) {
            String shtml = "您不是活动组织者，无法执行该操作";
            Result notAllowed = Results.forbidden(info.render("非法操作", Html.apply(shtml), null, null));
            return F.Promise.pure(notAllowed);
        }
        return delegate.call(ctx);
    }
}
