package spbox.web.annotation;


import play.libs.F;
import play.mvc.Http;
import play.mvc.Result;
import spbox.cep.EventMonitor;


public class ControllerHandler extends play.mvc.Action.Simple {

    public F.Promise<Result> call(Http.Context ctx) throws Throwable {

        EventMonitor.getPageViewMonitor();
        EventMonitor.getOrganizerRatingMonitor();

        return delegate.call(ctx);
    }
}
