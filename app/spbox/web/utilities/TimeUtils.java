package spbox.web.utilities;


import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class TimeUtils {

    public static Date getUTCDate(Date date){
        try {
            if(date != null) {

                DateTime jodaDate = new DateTime(date).withZone(DateTimeZone.getDefault());

                DateTime zoned = jodaDate.toDateTime(DateTimeZone.UTC);

                return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(zoned.toString("yyyy-MM-dd HH:mm:ss"));
            }else{
                return null;
            }
        }catch(Exception e){
            return date;
        }
    }

    public static Date getActivityLocalDate(Date date, TimeZone timeZone){
        try {
            if(date != null && timeZone != null) {

                DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.s")
                        .withLocale(Locale.ROOT)
                        .withChronology(ISOChronology.getInstanceUTC());

                DateTime dt = formatter.parseDateTime(date.toString());

                DateTime jodaDate = dt.toDateTime(DateTimeZone.forID(timeZone.getID()));

                return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(jodaDate.toString("yyyy-MM-dd HH:mm:ss"));
            }else{
                return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(new DateTime(DateTimeZone.forID(timeZone.getID())).toString("yyyy-MM-dd HH:mm:ss"));
            }
        }catch(Exception e){
            return date;
        }
    }

    public static Date getUTCEndDate(Date startDate, int field, int amount){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        if(field != 0 && amount != 0) {
            calendar.add(field, amount);
        }
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        return TimeUtils.getUTCDate(calendar.getTime());
    }
}
