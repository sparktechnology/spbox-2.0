package spbox.web.utilities;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.ning.http.util.Base64;
import play.Logger;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Hashtable;

/**
 * 二维码工具类
 * 
 */
public class QRCodeUtil {

	private static final String CHARSET = "utf-8";
	private static final String FORMAT_NAME = "JPG";
	// 二维码尺寸
	private static final int QRCODE_SIZE = 300;
	// LOGO宽度
	public static final int WIDTH = 60;
	// LOGO高度
	public static final int HEIGHT = 60;

	private static BufferedImage createImage(String content, Image icon) throws Exception {
		Hashtable<EncodeHintType, Object> hints = new Hashtable<EncodeHintType, Object>();
		hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
		hints.put(EncodeHintType.CHARACTER_SET, CHARSET);
		hints.put(EncodeHintType.MARGIN, 1);
		BitMatrix bitMatrix = new MultiFormatWriter().encode(content,
				BarcodeFormat.QR_CODE, QRCODE_SIZE, QRCODE_SIZE, hints);
		int width = bitMatrix.getWidth();
		int height = bitMatrix.getHeight();
		BufferedImage image = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_RGB);
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				image.setRGB(x, y, bitMatrix.get(x, y) ? 0xFF000000
						: 0xFFFFFFFF);
			}
		}
		if (icon == null) {
			return image;
		}
		// 插入图片
		QRCodeUtil.insertImage(image, icon);
		return image;
	}

	private static void insertImage(BufferedImage source, Image icon) throws Exception {


		// 插入LOGO
		Graphics2D graph = source.createGraphics();
		int width = icon.getWidth(null);
		int height = icon.getHeight(null);
		int x = (QRCODE_SIZE - width) / 2;
		int y = (QRCODE_SIZE - height) / 2;

		graph.drawImage(icon, x, y, width, height, null);
		Shape shape = new RoundRectangle2D.Float(x, y, width, width, 6, 6);
		graph.setStroke(new BasicStroke(3f));
		graph.draw(shape);
		graph.dispose();
	}

	public static Image GetIconImage(String resource, boolean compress) {

		String key = "imagecache." + resource +"." + compress;
		Image cached = (Image)play.cache.Cache.get(key);
		if(cached != null){
			return cached;
		}
		Image src = null;
		try {
			InputStream logoStream = play.Play.application().resourceAsStream(resource);
			src = ImageIO.read(logoStream);
		} catch (IOException e) {
			Logger.error("Failed to get icon file", e);
			return null;
		}
		int width = src.getWidth(null);
		int height = src.getHeight(null);

		if(compress) {
			if (width > WIDTH) {
				width = WIDTH;
			}
			if (height > HEIGHT) {
				height = HEIGHT;
			}
			Image image = src.getScaledInstance(width, height,
					Image.SCALE_SMOOTH);
			BufferedImage tag = new BufferedImage(width, height,
					BufferedImage.TYPE_INT_RGB);
			Graphics g = tag.getGraphics();
			g.drawImage(image, 0, 0, null); // 绘制缩小后的图
			g.dispose();
			src = image;
		}

		play.cache.Cache.set(key, src);
		return src;

	}

	public static String encodeBase64(String content, Image icon) {
		try {
			BufferedImage image = QRCodeUtil.createImage(content, icon);
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			ImageIO.write(image, "JPG", out);
			String base64bytes = Base64.encode(out.toByteArray());
			return "data:image/jpg;base64," + base64bytes;
		} catch (Exception e) {
			Logger.error("Failed to generate QRCode.", e);
			return null;
		}
	}

	public static byte[] encodeImage(String content, Image icon) {
		try {
			BufferedImage image = QRCodeUtil.createImage(content, icon);
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			ImageIO.write(image, "JPG", out);
			return out.toByteArray();
		} catch (Exception e) {
			Logger.error("Failed to generate QRCode.", e);
			return null;
		}
	}
}

