package spbox.web.utilities;

import com.stripe.Stripe;
import com.stripe.exception.*;
import com.stripe.model.Charge;
import play.Logger;
import play.Play;
import spbox.model.entity.ticket.TicketType;
import java.util.HashMap;
import java.util.Map;

public class PaymentUtils {

    public boolean processPayment(String stripeToken, String currency, String userEmail, Integer amount) {

        try {

            Logger.debug("Processing payment with token: " + stripeToken);

            String paymentDescription = this.getPaymentDescription(userEmail);

            Stripe.apiKey = Play.application().configuration().getString("stripe_api_key_server");
            Map<String, Object> chargeParams = this.stripeChargeParam(amount, currency, stripeToken, paymentDescription);
            Charge.create(chargeParams);

            Logger.debug("Charge successfully for token: " + stripeToken);
            return true;

        } catch (CardException e) {
            // Since it's a decline, CardException will be caught
            Logger.warn("Status is: " + e.getCode());
            Logger.warn("Message is: " + e.getMessage());
        } catch (InvalidRequestException e) {
            // Invalid parameters were supplied to Stripe's API
            Logger.error("Invalid request occurred: " + e.getLocalizedMessage());
        } catch (AuthenticationException e) {
            // Authentication with Stripe's API failed
            Logger.error("Authentication failed: " + e.getLocalizedMessage());
        } catch (APIConnectionException e) {
            // Network communication with Stripe failed
            Logger.error("Network error: " + e.getLocalizedMessage());
        } catch (StripeException e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            Logger.error("General failure: " + e.getLocalizedMessage());
        } catch (Exception e) {
            // Something else happened, completely unrelated to Stripe
            Logger.error("Unknown error occurred: " + e.getLocalizedMessage());
        }

        return false;
    }

    private Map<String, Object> stripeChargeParam(int amount, String currency, String source, String description){
        Map<String, Object> chargeParams = new HashMap<String, Object>();
        chargeParams.put("amount", amount);
        chargeParams.put("currency", currency);
        chargeParams.put("source", source);
        chargeParams.put("description", description);

        return chargeParams;
    }

    private int calculateTotalAmount(Double ticketPrice, Integer quantity, Boolean taxIncluded, Double taxPercentage){
        int totalAmount = (int) Math.floor(ticketPrice * 100 * quantity);
        if(!taxIncluded){
            totalAmount += totalAmount * taxPercentage;
        }
        return totalAmount;
    }

    private String getPaymentDescription(String userEmail){
        return Play.application().configuration().getString("stripe_ticket_charge_heading") + userEmail;
    }

}
