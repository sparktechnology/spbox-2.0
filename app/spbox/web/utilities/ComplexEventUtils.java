package spbox.web.utilities;

import spbox.cep.ComplexEventProcessor;
import spbox.cep.event.OrganizerRatingEvent;
import spbox.cep.event.PageViewEvent;

public class ComplexEventUtils {

    public void sendAndSavePageViewEvent(String pageUrl, String ipAddress){
        PageViewEvent pageViewEvent = new PageViewEvent(pageUrl, ipAddress);
        pageViewEvent.saveEventToDynamoDB();
        ComplexEventProcessor.getEpRuntime().sendEvent(pageViewEvent);
    }

    public void sendPageViewEvent(String pageUrl, String ipAddress){
        PageViewEvent pageViewEvent = new PageViewEvent(pageUrl, ipAddress);
        ComplexEventProcessor.getEpRuntime().sendEvent(pageViewEvent);
    }

    public void sendOrganizerRatingEvent(String email, Integer rating){
        OrganizerRatingEvent organizerRatingEvent = new OrganizerRatingEvent(email, rating);
        ComplexEventProcessor.getEpRuntime().sendEvent(organizerRatingEvent);
    }
}
