package spbox.web.utilities;

public class StringUtils {

    public static final String BLANK = "";

    public static boolean isBlank(String string){
        if(string == null || string.trim().equals(""))
            return true;
        else
            return false;
    }

    public static boolean isNotBlank(String string){
        if(string != null && !string.trim().equals(""))
            return true;
        else
            return false;
    }
}
