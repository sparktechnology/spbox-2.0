package spbox.web.utilities;

import play.Logger;
import play.Play;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import play.libs.Akka;
import play.libs.Crypto;
import play.libs.mailer.Email;
import play.libs.mailer.MailerClient;
import scala.concurrent.duration.Duration;
import spbox.BuiltInArticles;
import spbox.model.entity.Image;
import spbox.model.entity.activity.Activity;
import spbox.model.entity.email.Article;
import spbox.model.entity.ticket.Ticket;
import spbox.model.entity.ticket.TicketType;
import spbox.model.entity.user.User;
import spbox.model.service.ticket.TicketTypeService;
import spbox.model.service.user.UserService;

import javax.inject.Inject;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Mailer {

    @Inject
    private MailerClient mailer;

    @Inject
    private UserService userService;

    @Inject
    private BuiltInArticles builtInArticles;

    @Inject
    private TicketTypeService ticketTypeService;

    @Inject
    private Crypto crpto;

    @Transactional
    public void sendEmailNotificationToOrganizer(TicketType type, int quantity, String buyer, String websiteHost) {

        if (quantity == 0) {
            return;
        }

        String organizerEmail = type.activity.organizer;

        if (organizerEmail == null) {
            Logger.warn("Organizer is unknown");
            return;
        }

        if (!organizerEmail.contains("@")) {
            // Invalid email name
            Logger.warn("Organizer email does not contain '@' {}", organizerEmail);
            return;
        }

        User organizer = userService.findOneByEmail(organizerEmail);
        if (organizer == null) {
            Logger.warn("Organizer is unknown");
            return;
        }


        Article template = builtInArticles.GetOrganizerTemplate();

        String userName = organizer.name != null && !organizer.name.equals("") ? organizer.name : organizer.email.split("@")[0];
        final Email email = new Email();

        email.setSubject(template.title.replaceAll("\\{activity_name\\}", Matcher.quoteReplacement(type.activity.name)).replaceAll("\\{company_name\\}", Messages.get(Play.application().configuration().getString("email.company.name.en"))));
        email.setFrom(Play.application().configuration().getString("email.company.name.en")+" <info@havefunio.com>");
        email.addTo(userName + "<" + organizer.email + ">");

        String redirectUrl = "http://" + websiteHost + "/redirect?token=" +
                this.crpto.encryptAES(organizer.email + "|quick-auth|" + new Date())
                + "&redirectUrl=" + play.utils.UriEncoding.encodePathSegment("/ticket_manager/" + type.activity.bzid, "UTF-8");

        //String content = String.format("<html><body><p>尊敬的%s,</p><p></p><p>恭喜收到以下活动门票的订购:</p><p>    </p><p>活动：%s</p><p>类型：%s ($%s)</p><p>票数：%s</p><p>用户：%s</p>",
        //		userName, type.activity.name, type.type_name, type.price, quantity, buyer);
        //content += "<p>您可以登录 <a href='" + redirectUrl +"'>票务管理</a> 管理所有已购买的门票</p>";
        //content += "</body></html>";
        String content = "<html><body>" + template.content
                .replaceAll("\\{host\\}", websiteHost)
                .replaceAll("\\{user\\}", userName)
                .replaceAll("\\{activity\\}", Matcher.quoteReplacement(type.activity.name))
                .replaceAll("\\{type\\}", type.name)
                .replaceAll("\\{quantity\\}", (quantity + ""))
                .replaceAll("\\{buyer\\}", buyer)
                .replaceAll("\\{price\\}", ("\\$" + type.price))
                .replaceAll("\\{company_name\\}", Messages.get(Play.application().configuration().getString("email.company.name.en")))
                .replaceAll("\\{company_name_en\\}", Play.application().configuration().getString("email.company.name.en"))
                .replaceAll("\\{official_site\\}", Play.application().configuration().getString("email.official_site"))
                .replaceAll("\\{link\\}", ("<a href='" + redirectUrl + "'>票务管理</a>")) + "</body></html>";


        email.setBodyHtml(content);
        String id = this.mailer.send(email);
    }

    @Transactional
    public void sendEmailNotificationToOrganizer(Activity activity, String websiteHost) {

        String organizerEmail = activity.organizer;

        if (organizerEmail == null) {
            Logger.warn("Organizer is unknown");
            return;
        }

        if (!organizerEmail.contains("@")) {
            // Invalid email name
            Logger.warn("Organizer email does not contain '@' {}", organizerEmail);
            return;
        }

        User organizer = userService.findOneByEmail(organizerEmail);


        if (organizer == null) {
            Logger.warn("Organizer is unknown");
            return;
        }

        Article template = builtInArticles.GetNewActivityTemplate();

        String userName = organizer.name != null && !organizer.name.equals("") ? organizer.name : organizer.email.split("@")[0];
        final Email email = new Email();

        email.setSubject(template.title.replaceAll("\\{activity_name\\}", Matcher.quoteReplacement(activity.name)).replaceAll("\\{activity_status\\}", activity.status.getMsg()).replaceAll("\\{company_name\\}", Messages.get(Play.application().configuration().getString("email.company.name.en"))));
        email.setFrom(Play.application().configuration().getString("email.company.name.en")+" <info@havefunio.com>");
        email.addTo(userName + "<" + organizer.email + ">");

        String redirectUrl = "http://" + websiteHost + "/redirect?token=" +
                this.crpto.encryptAES(organizer.email + "|quick-auth|" + new Date())
                + "&redirectUrl=" + play.utils.UriEncoding.encodePathSegment("/activity_manager?id=" + activity.bzid, "UTF-8");

        String content = template.content;
        String ticketGroupPattern = "\\{ticket_type_list_begin\\}(.+)\\{ticket_type_list_end\\}";
        Matcher matcher = Pattern.compile(ticketGroupPattern, Pattern.DOTALL).matcher(content);

        if (matcher.find()) {
            Logger.debug("Got ticket type template " + matcher.group(1));
            String ticketGroupTemplate = matcher.group(1);
            String ticketGroup = "";

            //play-mailer does not support cid, we cannot insert an image here, so instead, we use image link to our website
            for (TicketType tt : ticketTypeService.findAllByActivityDbid(activity.dbid)) {
                if (tt != null) {
                    try {

                        ticketGroup += ticketGroupTemplate
                                .replaceAll("\\{type\\}", tt.name)
                                .replaceAll("\\{price\\}", String.valueOf(tt.price))
                                .replaceAll("\\{company_name\\}", Messages.get(Play.application().configuration().getString("email.company.name.en")))
                                .replaceAll("\\{company_name_en\\}", Play.application().configuration().getString("email.company.name.en"))
                                .replaceAll("\\{official_site\\}", Play.application().configuration().getString("email.official_site"))
                                .replaceAll("\\{quantity\\}", String.valueOf(tt.totalQuantity));


                    } catch (Exception e) {
                        Logger.warn("Failed to generate ticket type list: " + activity.bzid);
                    }
                }
            }

            content = content.replaceAll("(?s)\\{ticket_type_list_begin\\}.+\\{ticket_type_list_end\\}", ticketGroup);

        } else {
            Logger.warn("Cannot get ticket group template.");
        }

        //String content = String.format("<html><body><p>尊敬的%s,</p><p></p><p>恭喜收到以下活动门票的订购:</p><p>    </p><p>活动：%s</p><p>类型：%s ($%s)</p><p>票数：%s</p><p>用户：%s</p>",
        //		userName, type.activity.name, type.type_name, type.price, quantity, buyer);
        //content += "<p>您可以登录 <a href='" + redirectUrl +"'>票务管理</a> 管理所有已购买的门票</p>";
        //content += "</body></html>";
        content = "<html><body>" + content
                .replaceAll("\\{host\\}", websiteHost)
                .replaceAll("\\{user\\}", userName)
                .replaceAll("\\{activity\\}", Matcher.quoteReplacement(activity.name))
                .replaceAll("\\{date\\}", activity.dateTime)
                .replaceAll("\\{company_name\\}", Messages.get(Play.application().configuration().getString("email.company.name.en")))
                .replaceAll("\\{company_name_en\\}", Play.application().configuration().getString("email.company.name.en"))
                .replaceAll("\\{official_site\\}", Play.application().configuration().getString("email.official_site"))
                .replaceAll("\\{link\\}", ("<a href='" + redirectUrl + "'>活动管理</a>")) + "</body></html>";

        email.setBodyHtml(content);
        String id = this.mailer.send(email);
    }

    @Transactional
    private void SendUserConfirmEmail(User user, String websiteHost, String initialPwd, Boolean createdByAdmin, Article template) {

        if (!user.email.contains("@")) {
            // Invalid email name
            Logger.warn("Email does not contain '@' {}", user.email);
            return;
        }

        if (template != null) {
            String userName = user.name != null && !user.name.equals("") ? user.name : user.email.split("@")[0];
            final Email email = new Email();
            email.setSubject(template.title.replaceAll("\\{company_name\\}", Messages.get(Play.application().configuration().getString("email.company.name.en"))));
            email.setFrom(Play.application().configuration().getString("email.company.name.en")+" <info@havefunio.com>");
            email.addTo(userName + "<" + user.email + ">");

            String linkUrl = "http://" + websiteHost + "/verify_account?token="
                    + this.crpto.encryptAES(user.email + "|account-verify|" + new Date());

            String content = "<html><body>" +
                    template.content
                            .replaceAll("\\{host\\}", websiteHost)
                            .replaceAll("\\{user\\}", userName)
                            .replaceAll("\\{link_url\\}", linkUrl)
                            .replaceAll("\\{company_name\\}", Messages.get(Play.application().configuration().getString("email.company.name.en")))
                            .replaceAll("\\{company_name_en\\}", Play.application().configuration().getString("email.company.name.en"))
                            .replaceAll("\\{official_site\\}", Play.application().configuration().getString("email.official_site"))
                    + "</body></html>";

            if (createdByAdmin) {
                String password_reset_url = "http://" + websiteHost + "/passwordReset?token="
                        + this.crpto.encryptAES(user.email + "|password-recovery|" + new Date());
                content = content.replaceAll("\\{password_reset_link_url\\}",password_reset_url);
                content = content.replaceAll("\\{pwdinfo\\}", initialPwd);
            }

            email.setBodyHtml(content);
            this.mailer.send(email);
        }
    }

    @Transactional
    public void SendUserVerificationEmail(User user, String websiteHost, String initialPwd, Boolean createdByAdmin) {

        Article template = createdByAdmin ? builtInArticles.GetAdminGeneratedUserConfirmTemplate()
                : builtInArticles.GetUserConfirmTemplate();

        // Run email notice in background
        Akka.system().scheduler().scheduleOnce(Duration.create(0, TimeUnit.SECONDS), () -> {
            try {
                Logger.info("Sending user verification to user " + user.email);
                SendUserConfirmEmail(user, websiteHost, initialPwd, createdByAdmin, template);
            } catch (Exception e) {
                Logger.error("Failed to user verification email for user {}", user.email, e);
            }
        }, Akka.system().dispatcher());
    }

    @Transactional
    public void SendPasswordResetEmail(User user, String websiteHost) {

        Article template = builtInArticles.GetResetPasswordTemplate();

        // Run email notice in background
        Akka.system().scheduler().scheduleOnce(Duration.create(0, TimeUnit.SECONDS), () -> {
            try {
                Logger.info("Sending password reset email to " + user.email);
                String url = "http://" + websiteHost + "/passwordReset?token="
                        + this.crpto.encryptAES(user.email + "|password-recovery|" + new Date());
                final Email mail = new Email();
                mail.setSubject("["+Messages.get(Play.application().configuration().getString("email.company.name.en"))+"] - 密码重置");
                mail.setFrom(Play.application().configuration().getString("email.company.name.en")+" <info@havefunio.com>");
                String userName = user.name != null && !user.name.equals("") ? user.name : user.email.split("@")[0];
                mail.addTo(userName + "<" + user.email + ">");


                String content = "<html><body>" + template.content
                        .replaceAll("\\{host\\}", websiteHost)
                        .replaceAll("\\{user\\}", userName)
                        .replaceAll("\\{company_name\\}", Messages.get(Play.application().configuration().getString("email.company.name.en")))
                        .replaceAll("\\{company_name_en\\}", Play.application().configuration().getString("email.company.name.en"))
                        .replaceAll("\\{official_site\\}", Play.application().configuration().getString("email.official_site"))
                        .replaceAll("\\{link_url\\}", url);
                content += "</body></html>";

                mail.setBodyHtml(content);
                this.mailer.send(mail);

            } catch (Exception e) {
                Logger.error("Failed to send email for user {}", user.email, e);
            }

        }, Akka.system().dispatcher());
    }

    public void sendEmail(List<Ticket> tickets, TicketType ticketType, User user, String websiteHost) {
        if (tickets.size() == 0) {
            return;
        }

        if (!user.email.contains("@")) {
            // Invalid email name
            Logger.debug("User email does not contain '@' {}", user.email);
            return;
        }

        String organizerEmail = ticketType.activity.organizer;


        User organizer = userService.findOneByEmail(organizerEmail);
        if (organizer == null) {
            Logger.warn("Organizer is unknown");
            return;
        }

        Article template = builtInArticles.GetEmailTemplate(ticketType.activity.bzid, ticketType.activity.property.emailCustomized);

        String organizerName = organizer.name != null && !organizer.name.equals("") ? organizer.name : organizer.email.split("@")[0];
        String organizerPhone = organizer.mobile;
        String userName = user.name != null && !user.name.equals("") ? user.name : user.email.split("@")[0];
        String server_url = "http://" + websiteHost;


        String content = template.content;
        String ticketGroupPattern = "\\{tickets_begin\\}(.+)\\{tickets_end\\}";
        Matcher matcher = Pattern.compile(ticketGroupPattern, Pattern.DOTALL).matcher(content);

        if (matcher.find()) {
            Logger.debug("Got tickets template " + matcher.group(1));
            String ticketGroupTemplate = matcher.group(1);
            String ticketGroup = "";

            //play-mailer does not support cid, we cannot insert an image here, so instead, we use image link to our website
            for (Ticket t : tickets) {
                if (t != null) {
                    try {

                        ticketGroup += ticketGroupTemplate
                                .replaceAll("\\{ticket_number\\}", t.bzid)
                                .replaceAll("\\{ticket_verify_link\\}", server_url + "/verifyTicket/" + t.bzid)
                                .replaceAll("\\{company_name\\}", Messages.get(Play.application().configuration().getString("email.company.name.en")))
                                .replaceAll("\\{company_name_en\\}", Play.application().configuration().getString("email.company.name.en"))
                                .replaceAll("\\{official_site\\}", Play.application().configuration().getString("email.official_site"));


                    } catch (Exception e) {
                        Logger.warn("Failed to generate ticket QRCode: " + t.bzid);
                        JPA.em().getTransaction().rollback();
                    }
                }
            }

            content = content.replaceAll("(?s)\\{tickets_begin\\}.+\\{tickets_end\\}", ticketGroup);

        } else {
            Logger.warn("Cannot get ticket group template.");
        }

        final Email email = new Email();
        email.setSubject(template.title.replaceAll("\\{activity_name\\}", Matcher.quoteReplacement(tickets.get(0).activity.name)).replaceAll("\\{company_name\\}", Messages.get(Play.application().configuration().getString("email.company.name.en"))));
        email.setFrom(Play.application().configuration().getString("email.company.name.en")+" <info@havefunio.com>");
        email.addTo(userName + "<" + user.email + ">");

        content = "<html><body>" + content
                .replaceAll("\\{host\\}", websiteHost)
                .replaceAll("\\{user\\}", userName)
                .replaceAll("\\{organizer\\}", organizerName)
                .replaceAll("\\{organizer_email\\}", organizerEmail)
                .replaceAll("\\{organizer_tel\\}", organizerPhone)
                .replaceAll("\\{date\\}", GetCurrentDateInCn(new Date(),tickets.get(0).activity.venue.city.timeZone))
                .replaceAll("\\{activity\\}", Matcher.quoteReplacement(tickets.get(0).activity.name))
                .replaceAll("\\{activity_link\\}", server_url + "/activity/" + tickets.get(0).activity.bzid+"/?preview=false")
                .replaceAll("\\{type\\}", ticketType.name)
                .replaceAll("\\{quantity\\}", tickets.size() + "")
                .replaceAll("\\{price\\}", "\\$" + ticketType.price)
                .replaceAll("\\{company_name\\}", Messages.get(Play.application().configuration().getString("email.company.name.en")))
                .replaceAll("\\{company_name_en\\}", Play.application().configuration().getString("email.company.name.en"))
                .replaceAll("\\{official_site\\}", Play.application().configuration().getString("email.official_site"))
                .replaceAll("\\{link\\}", ("<a href='" + server_url + "/tickets'>我的门票<a>")) + "</body></html>";


        email.setBodyHtml(content);
        String id = this.mailer.send(email);
    }

    private String GetCurrentDateInCn(Date date, String timeZone) {
        if (date == null) {
            return spbox.web.utilities.StringUtils.BLANK;
        }
        DateFormat df = new SimpleDateFormat("yyyy年MM月dd日", Locale.CHINA);
        df.setTimeZone(TimeZone.getTimeZone(timeZone));
        return df.format(date);
    }
}
