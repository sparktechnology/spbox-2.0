package spbox.web.utilities.activity;

import com.amazonaws.services.s3.AmazonS3Client;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import play.Play;
import play.data.DynamicForm;
import play.db.jpa.JPA;
import play.i18n.Messages;
import play.mvc.Result;
import spbox.UserContext;
import spbox.model.entity.activity.Activity;
import spbox.model.entity.activity.ActivityImage;
import spbox.model.entity.activity.ActivityIndexPreviewImage;
import spbox.model.entity.ticket.TicketType;
import spbox.model.entity.ticket.TicketTypeManual;
import spbox.model.entity.user.User;
import spbox.model.s3.S3;
import spbox.model.service.activity.ActivityImageService;
import spbox.model.service.activity.ActivityService;
import spbox.model.service.ticket.TicketTypeManualService;
import spbox.model.service.ticket.TicketTypeService;
import spbox.model.service.ticket.TicketService;
import spbox.model.service.user.UserService;
import spbox.web.utilities.StringUtils;

import javax.imageio.ImageIO;
import javax.inject.Inject;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Blob;
import java.util.*;

import static play.mvc.Results.badRequest;
import static play.mvc.Results.forbidden;
import static play.mvc.Results.unauthorized;

public class ActivityUtility {

    @Inject
    private TicketTypeService ticketTypeService;

    @Inject
    private TicketTypeManualService ticketTypeManualService;

    @Inject
    private TicketService ticketService;

    @Inject
    private ActivityImageService activityImageService;

    @Inject
    private ActivityService activityService;

    @Inject
    private UserService userService;

    public boolean isEditable(Activity activity) {
        return activity != null
                && (UserContext.isCurrentUserAdministrator() || UserContext.GetCurrentUserEmail()
                .equals(activity.organizer));
    }

    public void updateUserInformation(String organizerPhone, String organizerName) {
        User user = userService.findOneByEmail(UserContext.GetCurrentUserEmail());

        if (UserContext.isCurrentUserGeneralUser()) {
            user.role = User.UserRoleEnum.ORGANIZER;
            UserContext.SetCurrentUser(user);
        }

        if (organizerPhone != null)
            user.mobile = organizerPhone;

        if (organizerName != null)
            user.name = organizerName;

        userService.doUpdate(user);
    }


    public void updateActivityPrice(String activityBzid) {
        Activity activity = activityService.findOneByBzid(activityBzid);

        Double cheapestTicketType = ticketTypeService.findCheapestUniqueTicketPriceByActivityBzid(activityBzid);
        Double mostExpensiveTicketType = ticketTypeService.findMostExpensiveUniqueTicketPriceByActivityBzid(activityBzid);

        activity.priceLeast = cheapestTicketType;
        activity.priceMost = mostExpensiveTicketType;

        activityService.doUpdate(activity);
    }

    public Result extractTicketTypeFromPost(Activity activity, DynamicForm activityForm, String key)
            throws Exception {
        if (activityForm.get(key + "_count") != null && activityForm.get(key + "_count").trim() != "") {

            Integer key_count = Integer.valueOf(activityForm.get(key + "_count"));

            if (key.trim().equals("new")) {
                return addTicketType(key_count, activity, activityForm);
            }

            if (key.trim().equals("delete")) {
                return deleteTicketType(activity.organizer, key_count, activityForm);
            }

            if (key.trim().equals("update")) {
                return editTicketType(activity.organizer, key_count, activityForm);
            }

        }
        return null;
    }

    private Result addTicketType(Integer count, Activity activity, DynamicForm activityForm) {
        for (int i = 0; i < count; i++) {
            String ticket_type_name = activityForm.get("new_" + i + "_type_name");
            String ticket_type_price = activityForm.get("new_" + i + "_price");
            String ticket_type_quantity = activityForm.get("new_" + i + "_quantity");
            String ticket_type_include_tax = activityForm.get("new_" + i + "_include_tax");

            if (activity != null) {
                TicketType ticketType = new TicketType();
                ticketType.activity = activity;
                ticketType.name = ticket_type_name;
                ticketType.price = Double.valueOf(ticket_type_price);
                ticketType.totalQuantity = Integer.valueOf(ticket_type_quantity);
                ticketType.taxIncluded = Boolean.valueOf(ticket_type_include_tax);
                ticketType.currency = "CAD";

                if (Double.valueOf(ticket_type_price) == 0) {
                    ticketType.category = TicketType.TicketTypeCategoryEnum.FREE;
                } else {
                    ticketType.category = TicketType.TicketTypeCategoryEnum.PAID;
                }

                try {
                    ticketTypeService.doCreate(ticketType);
                } catch (Exception e) {
                    return badRequest("info.error.ticketType.create");
                }
            }

        }
        return null;
    }

    private Result editTicketType(String OrganizerEmail, Integer count, DynamicForm activityForm) {
        for (int i = 0; i < count; i++) {
            String ticket_type_id = activityForm.get("update_" + i + "_id");
            String ticket_type_name = activityForm.get("update_" + i + "_type_name");
            String ticket_type_price = activityForm.get("update_" + i + "_price");
            String ticket_type_quantity = activityForm.get("update_" + i + "_quantity");
            String ticket_type_include_tax = activityForm.get("update_" + i + "_include_tax");
            try {
                if (StringUtils.isNotBlank(ticket_type_id)) {
                    TicketType ticketType = ticketTypeService.findOneByBzid(ticket_type_id);
                    if (ticketType == null) {
                        return badRequest("info.error.ticketType.notFound");
                    } else {
                        if (!UserContext.isCurrentUserAdministrator() && !UserContext.GetCurrentUserEmail().equalsIgnoreCase(OrganizerEmail)) {
                            return unauthorized("info.error.illegal");
                        }
                        if (ticketService.findCountByTicketTypeBzid(ticket_type_id) >= Long.valueOf(ticket_type_quantity)) {
                            return forbidden("info.error.ticketType.update.forbidden");
                        }
                        ticketType.name = ticket_type_name;
                        ticketType.totalQuantity = Integer.valueOf(ticket_type_quantity);
                        ticketType.price = Double.valueOf(ticket_type_price);
                        ticketType.taxIncluded = Boolean.valueOf(ticket_type_include_tax);

                        if (Double.valueOf(ticket_type_price) == 0) {
                            ticketType.category = TicketType.TicketTypeCategoryEnum.FREE;
                        } else {
                            ticketType.category = TicketType.TicketTypeCategoryEnum.PAID;
                        }
                        ticketTypeService.doUpdate(ticketType);
                    }
                } else return badRequest("info.error.ticketType.id.empty");

            } catch (Exception e) {
                return badRequest("info.error.ticketType.update");
            }
        }
        return null;
    }

    private Result deleteTicketType(String OrganizerEmail, Integer count, DynamicForm activityForm) {
        for (int i = 0; i < count; i++) {
            String ticketTypeId = activityForm.get("delete_" + i + "_id");
            try {
                if (StringUtils.isNotBlank(ticketTypeId)) {
                    TicketType ticketType = ticketTypeService.findOneByBzid(ticketTypeId);
                    if (ticketType == null) {
                        return badRequest("info.error.ticketType.notFound");
                    } else {
                        if (!UserContext.isCurrentUserAdministrator() && !UserContext.GetCurrentUserEmail().equalsIgnoreCase(OrganizerEmail)) {
                            return unauthorized("info.error.illegal");
                        } else if (ticketService.findCountByTicketTypeDbid(ticketType.dbid) != 0){
                            return forbidden("info.error.ticketType.delete.forbidden");
                        }
                        TicketTypeManual ticketTypeManual = ticketTypeManualService.findOneByTicketTypeDbid(ticketType.dbid);
                        if(ticketTypeManual != null) {
                            ticketTypeManualService.doDelete(ticketTypeManual);
                        }
                        ticketTypeService.doDelete(ticketType);
                    }
                } else return badRequest("info.error.ticketType.id.empty");
            } catch (Exception e) {
                return badRequest("info.error.ticketType.delete");
            }
        }
        return null;
    }

    public String extractDateTime(Date startTime, Date endTime) {

        StringBuilder dateTimeBuilder = new StringBuilder();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startTime);
        dateTimeBuilder.append(calendar.get(Calendar.YEAR)).append(Messages.get("message.year")).append(calendar.get(Calendar.MONTH)+1)
                .append(Messages.get("message.month")).append(calendar.get(Calendar.DATE)).append(Messages.get("message.day"));

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(endTime);

        boolean isAppended = false;
        if(calendar2.get(Calendar.YEAR) != calendar.get(Calendar.YEAR)){
            if(!isAppended){
                dateTimeBuilder.append(Messages.get("message.to"));
                isAppended = true;
            }
            dateTimeBuilder.append(calendar2.get(Calendar.YEAR)).append(Messages.get("message.year"));
        }

        if(calendar2.get(Calendar.MONTH) != calendar.get(Calendar.MONTH)){
            if(!isAppended){
                dateTimeBuilder.append(Messages.get("message.to"));
                isAppended = true;
            }
            dateTimeBuilder.append(calendar2.get(Calendar.MONTH)+1).append(Messages.get("message.month"));
        }

        if(calendar2.get(Calendar.DATE) != calendar.get(Calendar.DATE)){
            if(!isAppended){
                dateTimeBuilder.append(Messages.get("message.to"));
                isAppended = true;
            }
            dateTimeBuilder.append(calendar2.get(Calendar.DATE)).append(Messages.get("message.day"));
        }

        return dateTimeBuilder.toString();
    }

    public List<Activity> getRecommendedActivity(Activity activity, Activity.ActivityStatusEnum... status) {

        List<Activity> recommendedActivity = activityService.findAllRecommended(activity, Activity.ActivityStatusEnum.PUBLISH);

        return recommendedActivity;
    }

    public void saveFileBlob(ActivityImage.ActivityImageCategoryEnum category, File file, Activity activity) throws Exception{
        FileInputStream fin = new FileInputStream(file);
        Blob blob = Hibernate.getLobCreator(JPA.em().unwrap(Session.class)).createBlob(fin, file.length());

        ActivityImage activityImage = new ActivityImage();
        activityImage.activity = activity;

        activityImage.category = category;

        //activityImage.content = blob;
        activityImageService.doCreate(activityImage);

        blob.free();
    }

    public void saveBlobAsIndexImage(Blob blob, Activity activity) throws Exception{

        ActivityIndexPreviewImage activityIndexImage = new ActivityIndexPreviewImage();
        //activityIndexImage.content = blob;

        activity.indexPreviewImage = activityIndexImage;
        blob.free();
    }

    public void saveBlob(ActivityImage.ActivityImageCategoryEnum category, Blob blob, Activity activity) throws Exception{

        ActivityImage activityImage = new ActivityImage();
        activityImage.activity = activity;

        activityImage.category = category;

        //activityImage.content = blob;
        activityImageService.doCreate(activityImage);

        blob.free();
    }

    public void saveFileToS3(ActivityImage.ActivityImageCategoryEnum category, File file, Activity activity){
        String keyname = activity.bzid + "_ACTIVITY/" + category.name()+ "/" + activity.dbid + "_" + file.getName();

        S3.upload(keyname, file);

        ActivityImage activityImage = new ActivityImage();
        activityImage.activity = activity;

        activityImage.category = category;

        activityImage.keyname = keyname;
        activityImageService.doCreate(activityImage);
    }

    public void saveFileAsIndexImageToS3(File file, Activity activity){
        String keyname =  activity.bzid + "_ACTIVITY/" + ActivityImage.ActivityImageCategoryEnum.INDEX_PREVIEW+ "/" + activity.dbid + "_" + file.getName();

        S3.upload(keyname, file);

        ActivityIndexPreviewImage activityIndexImage = new ActivityIndexPreviewImage();
        activityIndexImage.keyname = keyname;

        activity.indexPreviewImage = activityIndexImage;
    }

    public void saveBufferedImageAsIndexImageToS3(BufferedImage image, Activity activity) throws Exception{
        String keyname =  activity.bzid + "_ACTIVITY/" + ActivityImage.ActivityImageCategoryEnum.INDEX_PREVIEW+ "/" + activity.dbid + "_indexPreview" ;

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ImageIO.write(image, "jpeg", os);
        byte[] buffer = os.toByteArray();
        S3.upload(keyname, buffer);

        ActivityIndexPreviewImage activityIndexImage = new ActivityIndexPreviewImage();
        activityIndexImage.keyname = keyname;

        activity.indexPreviewImage = activityIndexImage;
    }
}
