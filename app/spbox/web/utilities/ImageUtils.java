package spbox.web.utilities;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import play.Play;
import play.cache.Cache;
import play.db.jpa.JPA;
import play.mvc.Result;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.sql.Blob;
import java.util.*;

import static play.mvc.Results.ok;

public class ImageUtils {

    public BufferedImage createCheckedImage(Date date) throws IOException {
        String folder = "public/images/check/";

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR_OF_DAY) + 1;
        int minute = calendar.get(Calendar.MINUTE);
        String surfix = "am";

        if (hour == 24) {
            hour = 0;
        } else if (hour == 12) {
            surfix = "pm";
        } else if (hour > 12) {
            surfix = "pm";
            hour -= 12;
        }

        ArrayList<String> tokens = new ArrayList<String>();

        tokens.add(monthNames[month].substring(0, 3).toLowerCase());
        tokens.add(null);
        tokens.addAll(toStrTokens(day));
        tokens.add(null);
        tokens.addAll(toStrTokens(hour));
        tokens.add("colon");
        tokens.addAll(toStrTokens(minute));
        tokens.add(surfix);

        ArrayList<BufferedImage> tokenImgs = new ArrayList<BufferedImage>();
        for (String t : tokens) {
            if (t == null) {
                tokenImgs.add(null);
            } else {
                tokenImgs.add(loadImage(folder + t + ".png"));
            }
        }

        double rotation = Math.toRadians(18);

        BufferedImage frame = loadImage(folder + "checked.png");
        int frameWidth = frame.getWidth();
        int frameHeight = frame.getHeight();

        double h1 = Math.sin(rotation) * frameWidth;
        double h2 = Math.cos(rotation) * frameHeight;

        double w1 = Math.sin(rotation) * frameHeight;
        double w2 = Math.cos(rotation) * frameWidth;

        double width = w1 + w2;
        double height = h1 + h2;
        BufferedImage image = new BufferedImage((int) width, (int) height, BufferedImage.TYPE_INT_ARGB);

        Graphics2D g = (Graphics2D) image.getGraphics();

        g.translate(0, h1);
        g.rotate(-rotation);

        g.drawImage(frame, 0, 0, null);

        // positions
        int space = 10;
        int gap = 5;
        int centerX = frameWidth / 2;
        int centerY = frameHeight / 2 + 10;

        int textWidth = 0;
        for (BufferedImage img : tokenImgs) {
            textWidth += img == null ? space : img.getWidth() + gap;
        }

        int x = centerX - textWidth / 2;
        for (BufferedImage img : tokenImgs) {
            if (img != null) {

                g.drawImage(img, x, centerY, null);
            }
            x += img == null ? space : img.getWidth() + gap;
        }

        return image;

    }

    public BufferedImage loadImage(String path) throws IOException {
        String key = "imgOp-" + path;
        BufferedImage img = (BufferedImage) Cache.get(key);
        if (img == null) {
            img = ImageIO.read(Play.application().resourceAsStream(path));
            Cache.set(key, img);
        }

        return img;
    }


    public final String[] monthNames = {"January", "February", "March", "April", "May", "June", "July",
            "August", "September", "October", "November", "December"};

    public java.util.List<String> toStrTokens(int val) {
        java.util.List<String> list = new ArrayList<String>();
        String valStr = val + "";
        for (int i = 0; i < valStr.length(); i++) {
            list.add(valStr.charAt(i) + "");
        }
        return list;
    }

    public Result imageResult(BufferedImage img, String format) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(img, format, baos);
        return ok(baos.toByteArray()).as("image/" + format);
    }

    public File previewImageFile(File image){
        BufferedImage bufferedImage = decodeToImage(image);
        BufferedImage temp = squareCutV2(bufferedImage);
        return encodeToFile(temp, "jpeg");
    }

    public BufferedImage previewImageBufferedImage(File image){
        BufferedImage bufferedImage = decodeToImage(image);
        BufferedImage temp = squareCutV2(bufferedImage);
        return temp;
    }

    public Blob previewImage(File image){
        BufferedImage bufferedImage = decodeToImage(image);
        BufferedImage temp = squareCutV2(bufferedImage);
        return encodeToBlob(temp, "jpeg", image.length());
    }

    private BufferedImage decodeToImage(File image) {
        try {
            return ImageIO.read(image);
        } catch (final IOException ioe) {
            throw new UncheckedIOException(ioe);
        }
    }

    private Blob encodeToBlob(BufferedImage bufferedImage, String type, long length) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(bufferedImage, type, baos);
            InputStream is = new ByteArrayInputStream(baos.toByteArray());
            Blob blob = Hibernate.getLobCreator(JPA.em().unwrap(Session.class)).createBlob(is, length);
            return blob;
        }catch (IOException e){
            throw new UncheckedIOException(e);
        }
    }

    private File encodeToFile(BufferedImage bufferedImage, String type) {
        try {
            File outputfile = new File("preview."+type);
            ImageIO.write(bufferedImage, type, outputfile);
            return outputfile;
        }catch (IOException e){
            throw new UncheckedIOException(e);
        }
    }

    private BufferedImage squareCut(BufferedImage img) {
        int width = img.getWidth();
        int height = img.getHeight();
        int size = Math.min(img.getWidth(), img.getHeight());

        Image tmp = img.getScaledInstance(size, size/3*2, Image.SCALE_SMOOTH);

        BufferedImage sImg = new BufferedImage(size, size/3*2, img.getType());

        int dx = width - size;
        int dy = height - size;

        Graphics g = sImg.getGraphics();
        g.drawImage(tmp, -dx/3*2, -dy/3*2, width, height/3*2, null);
        g.dispose();

        return sImg;
    }

    private BufferedImage squareCutV2(BufferedImage img){
        return squareCutV2(img, 1.5f);
    }

    private BufferedImage squareCutV2(BufferedImage img, float ratio){
        int w = img.getWidth();
        int h = img.getHeight();
        float dx, dy, rW, rH;
        if (w > h * ratio){
            dx = (w - h * ratio)/2;
            dy = 0;
            rW = h * ratio;
            rH = h;
        }
        else{
            dx = 0;
            dy = (h - w / ratio)/2;
            rW = w;
            rH = w / ratio;
        }

        BufferedImage sImg = new BufferedImage((int) rW, (int) rH, img.getType());
        Graphics g = null;
        try {
            g = sImg.getGraphics();
            g.drawImage(img, -(int) dx, -(int) dy, (int) (rW + dx), (int) (rH+ dy), null);
        }
        finally {
            if(g != null) {
                g.dispose();
            }
        }


        return sImg;
    }

}
