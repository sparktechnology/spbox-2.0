package spbox.web.utilities;

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.commons.codec.binary.Base64;
import play.Play;
import play.libs.Json;

public class OAuthUtils {

    private static final String GoogleClientId =  Play.application().configuration().getString("google.client.id");

    public static boolean verifyGoogleToken(String token) {

        String[] googleIdTokens = token.split("\\.");

        JsonNode json = Json.parse(new String(Base64.decodeBase64(googleIdTokens[1])));

        if (json.findPath("aud").textValue().equals(GoogleClientId))
            return true;

        return false;
    }
}
