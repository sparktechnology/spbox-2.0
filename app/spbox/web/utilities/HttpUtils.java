package spbox.web.utilities;


import com.fasterxml.jackson.databind.node.ObjectNode;
import play.libs.Json;
import play.mvc.Http;

public class HttpUtils {

    public static ObjectNode buildHttpResult(String key, String value){
        ObjectNode result = Json.newObject();
        result.put(key, value);

        return result;
    }

    public static String getClientIP(Http.Request request){
        String clientIP = null;

        if(request != null){
            if(request.getHeader("X-Forwarded-For") != null){
                if(request.getHeader("X-Forwarded-For").contains(","))
                    clientIP = request.getHeader("X-Forwarded-For").split(",")[0];
                else
                    clientIP = request.getHeader("X-Forwarded-For");
            }else{
                clientIP = request.remoteAddress();
            }
        }
        return clientIP;
    }
}
