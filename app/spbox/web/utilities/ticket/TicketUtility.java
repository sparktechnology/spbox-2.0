package spbox.web.utilities.ticket;

import com.fasterxml.jackson.databind.JsonNode;
import play.Logger;
import play.i18n.Messages;
import play.libs.F;
import play.libs.ws.WS;
import spbox.UserContext;
import spbox.model.entity.activity.Activity;
import spbox.model.entity.ticket.Ticket;
import spbox.model.entity.ticket.TicketType;
import spbox.model.entity.user.User;
import spbox.model.service.ticket.TicketService;
import spbox.model.service.ticket.TicketTypeService;
import spbox.web.utilities.Mailer;
import spbox.web.utilities.PaymentUtils;
import spbox.web.utilities.TimeUtils;

import javax.inject.Inject;
import java.util.*;

import static play.mvc.Http.Context.Implicit.request;

public class TicketUtility {

    @Inject
    private Mailer mailer;

    @Inject
    private TicketService ticketService;

    @Inject
    private TicketTypeService ticketTypeService;

    @Inject
    private PaymentUtils payment;

    public String generateInvalidTicketUIDetail(Ticket ticket){
        StringBuilder shtmlBuilder = new StringBuilder();
        shtmlBuilder.append("").append(ticket.title).append(" - 该票已失效！<br><br>")
                .append("类型： ").append(ticket.ticketType.name).append("($ " + ticket.price + ")<br>")
                .append("检票时间： ").append(ticket.checkedTime).append("<br>")
                .append("检票员： ").append(ticket.checkedBy).append("<br><br>");
        return shtmlBuilder.toString();
    }

    public String generateValidTicketUIDetail(Ticket ticket){
        StringBuilder shtmlBuilder = new StringBuilder();
        shtmlBuilder.append("").append(ticket.title).append(" - 票据有效！<br><br>")
                .append("时间： ").append(ticket.activity.dateTime).append("<br>")
                .append("类型： ").append(ticket.ticketType.name).append("($ " + ticket.price + ")<br>")
                .append("持有者： ").append(ticket.user.email).append("<br><br>").append("");

        if (UserContext.isCurrentUserAdministrator() || ticket.activity.organizer.equalsIgnoreCase(UserContext.GetCurrentUserEmail())) {
            shtmlBuilder.append("<center><a class='btn btn-lg btn-danger' href='/cutTicket/").append(ticket.bzid)
                    .append("' role='button'>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp检票&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</a></center>");
        }
        return shtmlBuilder.toString();
    }

    public String processPayment(User user, Activity activity, JsonNode tickets, String paymentToken, Integer amount) {
        if (amount == 0) {
            return this.processFreeTicket(user, activity, tickets);
        } else {
            return this.processRegularTicket(user, activity, tickets, paymentToken, amount);
        }
    }

    public String processFreeTicket(User user, Activity activity, JsonNode tickets){
        String bundleId = UUID.randomUUID().toString();
        for (JsonNode t: tickets) {
            String typeName = t.get("type_name").asText();
            int quantity = t.get("quantity").asInt();

            TicketType ticketType = ticketTypeService.findOneByNameAndActivityBzid(typeName, activity.bzid);
            List<Ticket> createdTickets = this.createTickets(activity, ticketType, user, quantity, bundleId, true);

            Logger.debug("Creating {} ticket(s) for {} with bundle {}", quantity, user.email, bundleId);

            if (tickets == null) {
                return Messages.get("info.error.ticket.create");
            } else {
                this.updateTicketTypeBookedQuantity(ticketType, Integer.valueOf(quantity));
                this.sendEmailNotification(activity, ticketType, user, createdTickets);
            }
        }
        return null;
    }

    public String processRegularTicket(User user, Activity activity, JsonNode tickets, String paymentToken, Integer amount){
        String bundleId = UUID.randomUUID().toString();
        String currency = "CAD";
        Map<TicketType, List<Ticket>> createdTickets = new HashMap<TicketType, List<Ticket>>();

        for (JsonNode t: tickets) {
            String typeName = t.get("type_name").asText();
            int quantity = t.get("quantity").asInt();

            TicketType ticketType = ticketTypeService.findOneByNameAndActivityBzid(typeName, activity.bzid);
            List<Ticket> createdTicketsTemp = this.createTickets(activity, ticketType, user, quantity, bundleId, true);
            currency = ticketType.currency;

            if(!createdTickets.containsKey(ticketType)){
                createdTickets.put(ticketType, new ArrayList<Ticket>());
            }

            if(createdTicketsTemp != null && !createdTicketsTemp.isEmpty()){
                createdTickets.get(ticketType).addAll(createdTicketsTemp);
            }else{
                return Messages.get("info.error.ticket.pay");
            }
        }

        boolean paymentProcessed = payment.processPayment(paymentToken, currency, user.email, amount);

        if(!paymentProcessed){
            deleteTicketOnPaymentFailure(createdTickets);
            return Messages.get("info.error.ticket.pay");
        }else {
            for(TicketType ticketType: createdTickets.keySet()){
                this.updateTicketTypeBookedQuantity(ticketType, createdTickets.get(ticketType).size());
                this.sendEmailNotification(activity, ticketType, user, createdTickets.get(ticketType));
            }
            return null;
        }
    }

    public boolean createFreeTicket(String paymentToken) {
        Logger.debug("Free ticket does not require payment gateway, will create ticket directly.");

        // verify reCAPTCHA instead
        String request = "secret=" + "6LfYIAkTAAAAANOnqqqme-mJyzU9qDlIoYuSh0gl" + "&response=" + paymentToken;
        F.Promise<JsonNode> jsonPromise = WS.url("https://www.google.com/recaptcha/api/siteverify")
                .setContentType("application/x-www-form-urlencoded; charset=utf-8")
                .post(request).map(response -> {
                    Logger.debug("verify reCAPTCHA got return message: {}", response.getBody());
                    return response.asJson();
                });
        JsonNode result = jsonPromise.get(1000);

        return result.get("success").asBoolean();
    }

    public List<Ticket> createTickets(Activity activity, TicketType ticketType, User user, int quantity, String bundleId, boolean ticketPaid) {

        // Pre-generate uuids and sort to avoid SQL deadlock in MySQL
        // In MySQL, if we insert record with unordered primary key, while there are concurrent insert exists, deadlocks !!!!.

        String[] ticketIds = new String[quantity];
        for (int i = 0; i < quantity; i++) {
            ticketIds[i] = UUID.randomUUID().toString();
        }
        Arrays.sort(ticketIds);

        ArrayList<Ticket> tickets = new ArrayList<Ticket>();
        try {
            Logger.debug("Updating ticket count ..., bundle {}", bundleId);

            Logger.debug("Creating tickets for bundle {}", bundleId);
            for (String ticketId : ticketIds) {
                Ticket ticket = new Ticket();
                ticket.bzid = ticketId;
                ticket.activity = activity;
                ticket.user = user;
                ticket.ticketType = ticketType;
                ticket.bundleId = bundleId;
                ticket.title = activity.name;
                ticket.price = ticketType.price;
                ticket.seat = ticketType.seat;

                ticket.paid = ticketPaid;
                ticket.qrcode = null;

                ticket.purchaseTime = TimeUtils.getUTCDate(new Date());

                ticketService.doCreate(ticket);

                tickets.add(ticket);
            }
            return tickets;
        } catch (Exception e) {
            Logger.error("Something bad happened when creating the tickets for bundle " + bundleId, e);
        }
        return null;
    }

    private void deleteTicketOnPaymentFailure(Map<TicketType, List<Ticket>> createdTickets){
        for(TicketType ticketType: createdTickets.keySet()){
            for(Ticket ticket: createdTickets.get(ticketType)) {
                ticketService.doDelete(ticket);
                ticketType.bookedQuantity = ticketType.bookedQuantity - 1;
            }
            ticketTypeService.doUpdate(ticketType);
        }
    }

    private void updateTicketTypeBookedQuantity(TicketType ticketType, Integer quantity){
        if(ticketType.totalQuantity - ticketType.bookedQuantity >= quantity) {
            ticketType.bookedQuantity = ticketType.bookedQuantity + quantity;
            ticketTypeService.doUpdate(ticketType);
        }
    }

    public void sendEmailNotification(Activity activity, TicketType ticketType, User user, List<Ticket> tickets) {
        String host = request().host();

        if (activity.property.emailToBuyerEnabled) {
            this.sendEmailNotificationToBuyer(tickets, ticketType, user, host);
        }
        if (activity.property.emailToOrganizerEnabled) {
            this.sendEmailNotificationToOrganizer(tickets, ticketType, user, host);
        }
    }

    public void sendEmailNotificationToBuyer(List<Ticket> tickets, TicketType ticketType, User user, String host) {
        try {
            Logger.info("Sending email notification to user " + user.email);
            mailer.sendEmail(tickets, ticketType, user, host);
        } catch (Exception e) {
            Logger.error("Failed to send email for user {}", user.email, e);
        }
    }

    public void sendEmailNotificationToOrganizer(List<Ticket> tickets, TicketType ticketType, User user, String host) {
        try {
            Logger.info("Sending email notification to organizer ");
            mailer.sendEmailNotificationToOrganizer(ticketType, tickets.size(), user.email, host);
        } catch (Exception e) {
            Logger.error("Failed to send email for organizer", e);
        }
    }

}
