package spbox.web.controller;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import play.Logger;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import play.libs.Crypto;
import play.mvc.*;
import play.twirl.api.Html;
import spbox.BuiltInArticles;
import spbox.OrganizerRate;
import spbox.PageViewCount;
import spbox.UserContext;
import spbox.cep.dynamodb.DynamoDB;
import spbox.model.entity.Image;
import spbox.model.entity.PaymentMethod;
import spbox.model.entity.activity.Activity;
import spbox.model.entity.activity.ActivityCategory;
import spbox.model.entity.activity.Review;
import spbox.model.entity.email.Article;
import spbox.model.entity.ticket.TicketType;
import spbox.model.entity.ticket.TicketTypeManual;
import spbox.model.entity.user.User;
import spbox.model.searcher.user.UserSearcher;
import spbox.model.service.ImageService;
import spbox.model.service.PaymentMethodService;
import spbox.model.service.activity.ActivityService;
import spbox.model.service.activity.ActivityCategoryService;
import spbox.model.service.activity.ReviewService;
import spbox.model.service.ticket.TicketTypeManualService;
import spbox.model.service.ticket.TicketTypeService;
import spbox.model.service.user.UserService;
import spbox.web.annotation.Administrator;
import spbox.web.annotation.ControllerHandler;
import spbox.web.utilities.*;
import spbox.web.view.html.index;
import spbox.web.view.html.index_activities;
import spbox.web.view.html.info;
import spbox.web.view.html.article;
import spbox.web.view.html.system_manager;
import spbox.web.view.html.payment_method_manager;
import spbox.web.view.html.edit_payment_method;
import spbox.web.view.statistics.html.organizer_rating_manager;
import spbox.web.view.statistics.html.page_view_manager;
import spbox.web.view.ticket.html.manual_ticket_manager;

import javax.inject.Inject;
import java.awt.image.BufferedImage;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

public class SPboxManage extends Controller {

    @Inject
    private TicketTypeService ticketTypeService;

    @Inject
    private ReviewService reviewService;

    @Inject
    private TicketTypeManualService ticketTypeManualService;

    @Inject
    private ImageUtils imageUtils;

    @Inject
    private PaymentMethodService paymentMethodService;

    @Transactional
    @With({ControllerHandler.class, Administrator.class})
    public Result systemManagerUI() {
        try {
            return ok(system_manager.render("page.systemManager"));
        } catch (Exception e) {
            Logger.error(Messages.get("info.error.systemManager.view"));
            return ok(info.render("info.error.systemManager.view", Html.apply(Messages.get("info.error.systemManager.view")), null, null));
        }
    }

    @Transactional
    @With({ControllerHandler.class, Administrator.class})
    public Result luceneIndexManage() {
        try {
            FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(JPA.em());
            fullTextEntityManager.createIndexer().startAndWait();
            return ok(info.render("info.success.lucene.index.create", Html.apply(Messages.get("info.success.lucene.index.create")), "/", "page.index"));
        } catch (Exception e) {
            Logger.error(Messages.get("info.error.lucene.index.create") + e);
            return ok(info.render("info.error.lucene.index.create", Html.apply(Messages.get("info.error.lucene.index.create")), "/", "page.index"));
        }
    }


    @Transactional
    @With({ControllerHandler.class, Administrator.class})
    public Result manualDataManage() {
        try {
            List<TicketType> ticketTypes = ticketTypeService.findAll();
            List<TicketTypeManual> ticketTypeManuals = ticketTypeManualService.findAll();

            Map<Activity, Map<TicketType, TicketTypeManual>> ticketTypeMap = new LinkedHashMap<Activity, Map<TicketType, TicketTypeManual>>();

            ticketTypeManuals.forEach(ttm -> {
                if (!ticketTypeMap.containsKey(ttm.ticketType.activity)) {
                    ticketTypeMap.put(ttm.ticketType.activity, new LinkedHashMap<TicketType, TicketTypeManual>());
                }
                ticketTypeMap.get(ttm.ticketType.activity).put(ttm.ticketType, ttm);
                ticketTypes.remove(ttm.ticketType);
            });

            if (!ticketTypes.isEmpty()) {
                ticketTypes.forEach(tt -> {
                    if (!ticketTypeMap.containsKey(tt.activity)) {
                        ticketTypeMap.put(tt.activity, new LinkedHashMap<TicketType, TicketTypeManual>());
                    }
                    TicketTypeManual ticketTypeManual = new TicketTypeManual();
                    ticketTypeManual.ticketType = tt;
                    ticketTypeManual.bookedQuantity = 0;

                    ticketTypeManualService.doCreate(ticketTypeManual);
                    ticketTypeMap.get(tt.activity).put(tt, ticketTypeManual);
                });
            }
            return ok(manual_ticket_manager.render("info.success.manualData.ticket.create", ticketTypeMap));
        } catch (Exception e) {
            Logger.error(Messages.get("info.success.manualData.ticket.create") + e);
            return ok(info.render("info.success.manualData.ticket.create", Html.apply(Messages.get("info.success.manualData.ticket.create")), "/", "page.index"));
        }
    }

    @Transactional
    @With({ControllerHandler.class, Administrator.class})
    public Result editManualData(String manDbid) {
        TicketTypeManual ttm = ticketTypeManualService.findOneByDbid(Long.valueOf(manDbid));
        if (ttm.bookedQuantity + 1 != ttm.ticketType.totalQuantity) {
            ttm.bookedQuantity = ttm.bookedQuantity + 1;
            ticketTypeManualService.doUpdate(ttm);
        }
        return manualDataManage();
    }

    @Transactional
    @With({ControllerHandler.class, Administrator.class})
    public Result pageViewManage() {
        return ok(page_view_manager.render("page.pageView", PageViewCount.getPageViewCount()));
    }

    @Transactional
    @With({ControllerHandler.class, Administrator.class})
    public Result organizerRatingManage() {
        return ok(organizer_rating_manager.render("page.organizerRating", OrganizerRate.getOrganizerRate()));
    }

    @Transactional
    @With({ControllerHandler.class, Administrator.class})
    public Result loadPageViewHistoryData() {

        List<Map<String, AttributeValue>> pageViewHistroy= DynamoDB.scanTable("spbox");

        for(Map<String, AttributeValue> pageView : pageViewHistroy){
            String pageUrl =  pageView.get("pageUrl").getS();
            String ipAddress = pageView.get("ipAddress").getS();

            complexEventUtils.sendPageViewEvent(pageUrl, ipAddress);
        }

        return ok(info.render("info.success.pageView.load", Html.apply(Messages.get("info.success.pageView.load")), "/system_manager", "page.systemManager"));
    }

    @Transactional
    @With({ControllerHandler.class, Administrator.class})
    public Result loadOrganizerRatingHistoryData() {

        List<Review> reviews = reviewService.findAll();
        reviews.forEach(r -> complexEventUtils.sendOrganizerRatingEvent(r.activity.organizer, r.rating));
        return ok(info.render("info.success.organizerRating.load", Html.apply(Messages.get("info.success.organizerRating.load")), "/system_manager", "page.systemManager"));
    }

    @Transactional
    @With({ControllerHandler.class, Administrator.class})
    public Result paymentMethodManage() {

        List<PaymentMethod> paymentMethods = paymentMethodService.findAll();
        return ok(payment_method_manager.render("page.paymentMethodManager", paymentMethods));
    }

    @Transactional
    @With({ControllerHandler.class, Administrator.class})
    public Result editPaymentMethodUI(String dbid) {
        try {
            if (dbid.equalsIgnoreCase("new")) {
                return ok(edit_payment_method.render("page.paymentMethodManager.create", null));
            } else {

                PaymentMethod pm = paymentMethodService.findOneByDbid(Long.valueOf(dbid));

                if (pm != null) {
                    return ok(edit_payment_method.render("page.paymentMethodManager.update", pm));
                } else {
                    return ok(info.render("info.error.paymentMethod.notFound", Html.apply(Messages.get("info.error.paymentMethod.notFound")), "/payment_method_manager", "page.paymentMethodManager.list"));
                }
            }
        } catch (Exception e) {
            Logger.error(Messages.get("info.error.paymentMethod.createOrUpdate") + e.getLocalizedMessage());
            return ok(info.render("info.error.paymentMethod.createOrUpdate", Html.apply(Messages.get("info.error.paymentMethod.createOrUpdate")), "/payment_method_manager", "page.paymentMethodManager.list"));
        }
    }

    @Transactional
    @With({ControllerHandler.class, Administrator.class})
    @BodyParser.Of(BodyParser.Json.class)
    public Result editPaymentMethod() {
        try {
            Http.RequestBody body = request().body();
            String id = body.asJson().findPath("id").textValue();
            String paymentMethodCN = body.asJson().findPath("paymentMethodCN").textValue();
            String paymentMethodEN = body.asJson().findPath("paymentMethodEN").textValue();

            if (StringUtils.isBlank(paymentMethodCN) || StringUtils.isBlank(paymentMethodEN)) {
                return badRequest("info.error.paymentMethod.createOrUpdate");
            }


            if (StringUtils.isBlank(id)) {
                if (StringUtils.isBlank(UserContext.GetCurrentUserEmail())) {
                    return unauthorized("false");
                } else if (!UserContext.isCurrentUserAdministrator()) {
                    return unauthorized("false");
                } else {

                    PaymentMethod paymentMethod = new PaymentMethod();
                    paymentMethod.CnName = paymentMethodCN;
                    paymentMethod.EnName = paymentMethodEN;

                    paymentMethodService.doCreate(paymentMethod);

                    Logger.info("成功创建： " + paymentMethodCN + " " + paymentMethodEN.toUpperCase());
                    ObjectNode result = HttpUtils.buildHttpResult("success", "paymentMethod created.");
                    return ok(result);
                }

            } else {
                if (StringUtils.isBlank(UserContext.GetCurrentUserEmail())) {
                    Logger.warn("请先登录");
                    return unauthorized("false");
                } else if (!UserContext.isCurrentUserAdministrator()) {
                    Logger.warn("不是管理员请勿更新");
                    return unauthorized("false");
                } else {

                    PaymentMethod paymentMethod = paymentMethodService.findOneByDbid(Long.valueOf(id));

                    paymentMethod.CnName = paymentMethodCN;
                    paymentMethod.EnName = paymentMethodEN;

                    paymentMethodService.doUpdate(paymentMethod);
                    Logger.info("Update paymentMethod {} successful.", paymentMethodCN + " " + paymentMethodEN.toUpperCase());
                    ObjectNode result = HttpUtils.buildHttpResult("success", "paymentMethod updated.");
                    return ok(result);
                }
            }
        } catch (Exception e) {
            Logger.error(Messages.get("info.error.paymentMethod.createOrUpdate"), e);
            return badRequest("info.error.paymentMethod.createOrUpdate");
        }
    }

    @Transactional
    @With({ControllerHandler.class, Administrator.class})
    public Result deletePaymentMethod(String dbid) {
        try {
            if (StringUtils.isBlank(dbid)) {
                return ok(info.render("info.error.paymentMethod.id.empty", Html.apply(Messages.get("info.error.paymentMethod.id.empty")), "/payment_method_manager", "page.paymentMethodManager"));
            } else {
                PaymentMethod paymentMethod = paymentMethodService.findOneByDbid(Long.valueOf(dbid));
                if (paymentMethod != null) {

                    StringBuilder shtmlBuilder = new StringBuilder();
                    shtmlBuilder.append(" <").append(paymentMethod.CnName ).append(" ")
                            .append(paymentMethod.EnName).append("> 删除成功");


                    paymentMethodService.doDelete(paymentMethod);

                    return ok(info.render("info.success.paymentMethod.delete", Html.apply(shtmlBuilder.toString()), "/payment_method_manager", "page.paymentMethodManager"));
                } else {
                    return ok(info.render("info.error.paymentMethod.notFound", Html.apply(Messages.get("info.error.paymentMethod.notFound")), "/payment_method_manager", "page.paymentMethodManager"));
                }
            }
        } catch (Exception e) {
            Logger.error(Messages.get("info.error.paymentMethod.delete") + e.getLocalizedMessage());
            return ok(info.render("info.error.paymentMethod.delete", Html.apply(Messages.get("info.error.paymentMethod.delete")), "/payment_method_manager", "page.paymentMethodManager"));
        }
    }

    @Inject
    private UserService userService;

    @Inject
    private BuiltInArticles builtInArticles;

    @Inject
    private ActivityService activityService;

    @Inject
    private ActivityCategoryService activityCategoryService;

    @Inject
    private UserSearcher userSearcher;

    @Inject
    private ComplexEventUtils complexEventUtils;

    @Inject
    private Crypto crpto;

    @Transactional
    @With(ControllerHandler.class)
    public Result indexUI() {
        try {

            complexEventUtils.sendAndSavePageViewEvent("index", request().remoteAddress());

            List<ActivityCategory> activityCategories = activityCategoryService.findAll();

            return ok(index.render("page.index.havefunio", activityCategories));
        } catch (Exception e) {
            Logger.error(Messages.get("info.error.index.activity.list") + e.getLocalizedMessage());
            return ok(info.render("info.error.index.activity.list", Html.apply(Messages.get("info.error.index.activity.list")), null, null));
        }
    }

    @Transactional
    @With(ControllerHandler.class)
    public Result activityCategoryUI(String category, String priceType, String eventTime, String sort, String maxResult, String page) {
        try {

            Long categoryId;

            if(category.equals("all")){
                categoryId = Long.valueOf(0);
            } else {
                categoryId = activityCategoryService.findOneDbidByEnName(category);
            }

            Date sortEndDate = null;
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            Date sortStartDate = TimeUtils.getUTCDate(calendar.getTime());

            // Time filter
            if (eventTime.equals("today")){
                sortEndDate = TimeUtils.getUTCEndDate(sortStartDate, 0, 0);
            } else if (eventTime.equals("tomorrow")){
                sortEndDate = TimeUtils.getUTCEndDate(sortStartDate, Calendar.DATE, 1);
            } else if (eventTime.equals("one-week")){
                sortEndDate = TimeUtils.getUTCEndDate(sortStartDate, Calendar.DATE, 7);
            } else if (eventTime.equals("one-month")){
                sortEndDate = TimeUtils.getUTCEndDate(sortStartDate, Calendar.MONTH, 1);
            }

            List<Activity> activities = activityService.findAllBriefByCategoryAndStatusAndSortWithPagination(categoryId, priceType, sortStartDate, sortEndDate, sort, maxResult, page, Activity.ActivityStatusEnum.PUBLISH);

            return ok(index_activities.render("page.index.havefunio", activities));
        } catch (Exception e) {
            Logger.error(Messages.get("info.error.index.activity.list") + e.getLocalizedMessage());
            return ok(info.render("info.error.index.activity.list", Html.apply(Messages.get("info.error.index.activity.list")), null, null));
        }
    }


    @Transactional
    @With(ControllerHandler.class)
    public Result aboutUI() {
        Article notice = builtInArticles.GetNotice();
        if (notice != null) {
            return ok(info.render(notice.title, Html.apply(notice.content), null, null));
        } else {
            return ok(info.render("info.error.page.notFound", Html.apply(Messages.get("info.error.page.notFound")), null, null));
        }
    }

    @Transactional
    @With(ControllerHandler.class)
    public Result termUI() {
        Article terms = builtInArticles.GetTerms();
        if (terms != null) {
            return ok(article.render(terms.title, Html.apply(terms.content), null, null));
        } else {
            return ok(article.render("info.error.page.notFound", Html.apply(Messages.get("info.error.page.notFound")), null, null));
        }
    }

    @Transactional
    @With(ControllerHandler.class)
    public Result login(String user, String password) {
        try {
            User userCheck = userService.findOneByEmail(user.toLowerCase());
            if (userCheck == null){
                Logger.info("User {} not found", user);
                ObjectNode result = HttpUtils.buildHttpResult("error", "user not found");
                return notFound(result);
            } else if (userCheck.role.equals(User.UserRoleEnum.BLOCKED)){
                UserContext.SetCurrentUser(null);
                Logger.info("User {} blocked", user);
                ObjectNode result = HttpUtils.buildHttpResult("error", "user blocked");
                return forbidden(result);
            } else {
                User authenticatedUser = userService.authenticate(user.toLowerCase(), this.crpto.sign(password));
                UserContext.SetCurrentUser(authenticatedUser);
                Logger.info("User {} login success", user.toLowerCase());
                return ok(authenticatedUser.toJsonObject());
            }
        } catch (Exception e) {
            Logger.error("Failed to authenticate user ", e);
            ObjectNode result = HttpUtils.buildHttpResult("error", "authentication failed.");
            return badRequest(result);
        }
    }

    @Transactional
    @With(ControllerHandler.class)
    public Result fbLogin(String email, String fbId) {
        try {
            User authenticatedUser = userService.findOneByEmail(email);

            if (authenticatedUser == null) {
                // Creating a new account using facebook information
                Logger.info("User {} is not in database", authenticatedUser);
                ObjectNode result = HttpUtils.buildHttpResult("needToRegister", "true");
                return ok(result);
            }

            if (authenticatedUser.role.equals(User.UserRoleEnum.BLOCKED)) {
                UserContext.SetCurrentUser(null);
                Logger.info("User {} blocked.", authenticatedUser);
                ObjectNode result = HttpUtils.buildHttpResult("error", "user blocked");
                return notFound(result);
            }

            if (authenticatedUser.facebookId == null) {
                authenticatedUser.facebookId = fbId;
                userService.doUpdate(authenticatedUser);
                UserContext.SetCurrentUser(authenticatedUser);
                Logger.info("User {} login success", authenticatedUser);
                return ok(authenticatedUser.toJsonObject());
            }
            UserContext.SetCurrentUser(authenticatedUser);
            Logger.info("User {} login success", authenticatedUser);
            return ok(authenticatedUser.toJsonObject());

        } catch (Exception e) {
            Logger.error("Failed to authenticate user ", e);

            ObjectNode result = HttpUtils.buildHttpResult("error", "authentication failed.");
            return badRequest(result);
        }
    }

    @Transactional
    @With(ControllerHandler.class)
    public Result googleLogin(String email, String googleId, String googleToken) {
        if(!OAuthUtils.verifyGoogleToken(googleToken)) {
            ObjectNode result = HttpUtils.buildHttpResult("error", "authentication failed.");
            return badRequest(result);
        }

        try {
            User authenticatedUser = userService.findOneByEmail(email);

            if (authenticatedUser == null) {
                // Creating a new account using facebook information
                Logger.info("User {} is not in database", authenticatedUser);
                ObjectNode result = HttpUtils.buildHttpResult("needToRegister", "true");
                return ok(result);
            }

            if (authenticatedUser.role.equals(User.UserRoleEnum.BLOCKED)) {
                UserContext.SetCurrentUser(null);
                Logger.info("User {} blocked.", authenticatedUser);
                ObjectNode result = HttpUtils.buildHttpResult("error", "user blocked");
                return notFound(result);
            }

            if (StringUtils.isBlank(authenticatedUser.googleId)) {
                authenticatedUser.googleId = googleId;
                userService.doUpdate(authenticatedUser);
                UserContext.SetCurrentUser(authenticatedUser);
                Logger.info("User {} login success", authenticatedUser);
                return ok(authenticatedUser.toJsonObject());
            }

            // Current user logging in
            UserContext.SetCurrentUser(authenticatedUser);
            Logger.info("User {} login success", authenticatedUser);
            return ok(authenticatedUser.toJsonObject());

        } catch (Exception e) {
            Logger.error("Failed to authenticate user ", e);

            ObjectNode result = HttpUtils.buildHttpResult("error", "authentication failed.");
            return badRequest(result);
        }
    }

    @Transactional
    @With(ControllerHandler.class)
    public Result logout() {
        UserContext.SetCurrentUser(null);
        ObjectNode result = HttpUtils.buildHttpResult("success", "true");
        return ok(result);
    }

    @Transactional
    @With(ControllerHandler.class)
    public Result searchEmail(String email) {
        try {
            User u = userSearcher.searchUser(email);

            if (u == null) {
                return unauthorized("user not found");
            } else if (u.role.equals(User.UserRoleEnum.BLOCKED)) {

                Logger.info("Blocked Email");
                return forbidden("user not found");

            } else {
                Logger.info("Existing email");

                return ok(u.toJsonObject());
            }
        } catch (Exception e) {
            Logger.error(Messages.get("info.error.user.notFound") + e.getLocalizedMessage());
            return badRequest(info.render("info.error.user.notFound", Html.apply(Messages.get("info.error.user.notFound")), null, null));
        }
    }

    @Transactional
    @With(ControllerHandler.class)
    public Result authenticate(String email, String password) {
        try {
            User authenticatedUser = userService.authenticate(email.toLowerCase(), this.crpto.sign(password));
            if(authenticatedUser != null){
                return ok(authenticatedUser.toJsonObject());
            } else {
                ObjectNode result = HttpUtils.buildHttpResult("error", "user not found or blocked");
                return notFound(result);
            }
        } catch (Exception e) {
            Logger.error("Failed to authenticate user ", e);

            ObjectNode result = HttpUtils.buildHttpResult("error", "authentication failed.");
            return badRequest(result);
        }
    }

    /*
    @Transactional
    @With(ControllerHandler.class)
    public Result doQuery(String keyword) {

        if (StringUtils.isBlank(keyword)) {
            return indexUI();
        }

        List<Activity> result = activitySearcher.searchActivity(keyword);
        Map<Activity, String> activityMap = new HashMap<>();

        return ok(index.render("page.index.havefunio", activityMap));
    }
    */

    @Transactional
    @With(ControllerHandler.class)
    public Result checkedImage(String date) {
        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date checkedDate = df.parse(date);

            BufferedImage img = imageUtils.createCheckedImage(checkedDate);

            return imageUtils.imageResult(img, "png");
        } catch (Exception e) {

            e.printStackTrace();
        }

        return badRequest();

    }

    @Inject
    private ImageService imageService;

    @Transactional
    public Result GetImageUI(String bzid) {

        try {
            Image img = imageService.findOneByBzid(bzid);
            if (img == null) {
                return notFound();
            }

            return ok(img.data).as(img.contentType);

        } catch (Exception e) {
            Logger.error("Failed to get image {}", e);
            return badRequest(e.getMessage());
        }
    }

}
