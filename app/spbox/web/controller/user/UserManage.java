package spbox.web.controller.user;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import play.Logger;
import play.Play;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import play.libs.Crypto;
import play.libs.Json;
import play.mvc.*;
import play.twirl.api.Html;
import spbox.OrganizerRate;
import spbox.UserContext;
import spbox.model.entity.PaymentMethod;
import spbox.model.entity.activity.Activity;
import spbox.model.entity.activity.ActivityImage;
import spbox.model.entity.activity.Review;
import spbox.model.entity.ticket.Ticket;
import spbox.model.entity.user.User;
import spbox.model.entity.user.UserPaymentRequirement;
import spbox.model.service.PaymentMethodService;
import spbox.model.service.activity.ActivityImageService;
import spbox.model.service.activity.ActivityService;
import spbox.model.service.activity.ReviewService;
import spbox.model.service.ticket.TicketService;
import spbox.model.service.user.UserPaymentRequirementService;
import spbox.model.service.user.UserService;
import spbox.web.annotation.Administrator;
import spbox.web.annotation.Authenticated;
import spbox.web.annotation.ControllerHandler;
import spbox.web.utilities.*;
import spbox.web.view.html.info;
import spbox.web.view.user.html.*;

import javax.inject.Inject;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class UserManage extends Controller {

    @Inject
    private UserService userService;

    @Inject
    private TicketService ticketService;

    @Inject
    private ReviewService reviewService;

    @Inject
    private ActivityService activityService;

    @Inject
    private ActivityImageService activityImageService;

    @Inject
    private UserPaymentRequirementService userPaymentRequirementService;

    @Inject
    private PaymentMethodService paymentMethodService;

    @Transactional
    @With({ControllerHandler.class, Administrator.class})
    public Result userManagerUI(String role) {
        try {
            if (role.equals("all")) {
                List<User> users = userService.findAll();
                return ok(user_manager.render("page.userManager.list", role, users));
            } else {
                List<User> users = userService.findByRole(User.UserRoleEnum.valueOf(role));
                return ok(user_manager.render("page.userManager.list", role, users));
            }
        } catch (Exception e) {
            Logger.error(Messages.get("info.error.user.list"), e);
            return ok(info.render("info.error.user.list", Html.apply(Messages.get("info.error.user.list")), "/system_manager", "page.systemManager"));
        }
    }

    @Transactional
    @With({ControllerHandler.class, Administrator.class})
    public Result deleteUser(String email) {
        try {
            if (StringUtils.isBlank(email)) {
                return ok(info.render("info.error.user.email.empty", Html.apply(Messages.get("info.error.user.email.empty")), "/user_manager/all", "page.userManager"));
            } else {
                User user = userService.findOneByEmail(email);
                if (user != null) {
                    StringBuilder shtmlBuilder = new StringBuilder();
                    shtmlBuilder.append("用户 <").append(user.email).append("> 删除成功");

                    deleteUserPaidTickets(email);
                    deleteUserReview(email);
                    userService.doDelete(user);

                    return ok(info.render("info.success.user.delete", Html.apply(shtmlBuilder.toString()), "/user_manager/all", "page.userManager"));
                } else {
                    return ok(info.render("info.error.user.notFound", Html.apply(Messages.get("info.error.user.notFound")), "/user_manager/all", "page.userManager"));
                }
            }

        } catch (Exception e) {
            Logger.error(Messages.get("info.error.user.delete") + e.getLocalizedMessage());
            return ok(info.render("info.error.user.delete", Html.apply(Messages.get("info.error.user.delete")), "/user_manager/all", "page.userManager"));
        }
    }

    private void deleteUserPaidTickets(String email){
        List<Ticket> tickets = ticketService.findAllPaidByUserEmail(email);

        for(Ticket ticket: tickets){
            ticketService.doDelete(ticket);
        }
    }

    private void deleteUserReview(String email){
        List<Review> reviews = reviewService.findAllByUserEmail(email);

        for(Review review: reviews){
            reviewService.doDelete(review);
        }
    }


    @Inject
    private Crypto crpto;

    @Transactional
    @With({ControllerHandler.class, Authenticated.class})
    public Result getUpdateUserInfoUI(String email) {
        try {

            User user = userService.findOneByEmail(email);
            if(UserContext.isCurrentUserAdministrator()) {
                return ok(register.render("page.user.update", user, true));
            }
            return ok(register.render("page.user.update", user, false));
        } catch (Exception e) {
            Logger.error(Messages.get("info.error.user.notFound"), e);
            return badRequest(info.render("info.error.user.notFound", Html.apply(Messages.get("info.error.user.notFound")), null, null));
        }
    }

    @Transactional
    @With({ControllerHandler.class, Authenticated.class})
    public Result getUserInfo() {
        try {
            User u = userService.findOneByEmail(UserContext.GetCurrentUserEmail());
            return ok(u.toJsonObject());
        } catch (Exception e) {
            Logger.error(Messages.get("info.error.user.notFound"), e);
            return badRequest("info.error.user.notFound");
        }
    }

    @Transactional
    @With({ControllerHandler.class, Authenticated.class})
    public Result updateUserInfo() {
        try {
            User u = userService.findOneByEmail(UserContext.GetCurrentUserEmail());

            try {
                JsonNode update_field = request().body().asJson();

                if(update_field.has("name"))
                    u.name = update_field.findPath("name").textValue();

                if(update_field.has("tel"))
                    u.mobile = update_field.findPath("tel").textValue();

                if(update_field.has("gender"))
                    u.gender = update_field.findPath("gender").textValue();

                if(update_field.has("age"))
                    u.age = update_field.findPath("age").asInt();

                if(update_field.has("address"))
                    u.address = update_field.findPath("address").textValue();

                userService.doUpdate(u);

                return ok("true");
            } catch (Exception e) {
                // Make sure we always have log to check
                Logger.error("Failed to udpate user {}", u.email, e);
                return badRequest("info.error.user.update");
            }

        } catch (Exception e) {
            Logger.error(Messages.get("info.error.user.update"), e);
            return badRequest("info.error.user.update");
        }
    }

    @Transactional
    @With({ControllerHandler.class, Authenticated.class})
    @BodyParser.Of(BodyParser.Json.class)
    public Result changePassword() {
        try {
            Http.RequestBody body = request().body();
            String email = body.asJson().findPath("email").textValue();
            String password = body.asJson().findPath("newPassword").textValue();
            String oldPassword = body.asJson().findPath("oldPassword").textValue();

            password = crpto.sign(password);
            oldPassword = crpto.sign(oldPassword);

            User u = userService.findOneByEmail(email);
            if (!u.password.equals(oldPassword)) {
                return unauthorized("info.error.user.changePassword");
            }

            u.password = password;
            userService.doUpdate(u);

            ObjectNode result = Json.newObject();
            result.put("success", "true");
            return ok(result);

        } catch (Exception e) {
            Logger.error(Messages.get("info.error.user.changePassword"), e);
            ObjectNode result = Json.newObject();
            result.put("error", "update password failed.");
            return badRequest(result);
        }
    }

    @Transactional
    @With({ControllerHandler.class, Authenticated.class})
    @BodyParser.Of(BodyParser.Json.class)
    public Result doUpdateOrganizerSetting() {
        try {
            Http.RequestBody body = request().body();
            String email = body.asJson().findPath("email").textValue();
            Integer paymentMethodDbid = body.asJson().findPath("paymentMethodDbid").asInt();
            String paymentEmail = body.asJson().findPath("paymentEmail").textValue();

            User u = userService.findOneByEmail(email);
            PaymentMethod paymentMethod = paymentMethodService.findOneByDbid(paymentMethodDbid.longValue());

            UserPaymentRequirement userPaymentRequirement = userPaymentRequirementService.findOneByUserEmail(email);

            if(userPaymentRequirement != null){
                userPaymentRequirement.paymentMethod = paymentMethod;
                userPaymentRequirement.paymentEmail = paymentEmail;
                userPaymentRequirementService.doUpdate(userPaymentRequirement);
            }else{
                UserPaymentRequirement paymentRequirement = new UserPaymentRequirement();
                paymentRequirement.paymentMethod = paymentMethod;
                paymentRequirement.paymentEmail = paymentEmail;
                paymentRequirement.user = u;
                userPaymentRequirementService.doCreate(paymentRequirement);
            }

            ObjectNode result = Json.newObject();
            result.put("success", "true");
            return ok(result);

        } catch (Exception e) {
            Logger.error(Messages.get("info.error.paymentMethod.createOrUpdate"), e);
            ObjectNode result = Json.newObject();
            result.put("error", "update organizer setting failed.");
            return badRequest(result);
        }
    }

    @Inject
    private Mailer mailer;

    @Inject
    private UserContext userContext;

    @Transactional
    @With(ControllerHandler.class)
    public Result registerUI() {
        try {
            if (!UserContext.GetCurrentUserEmail().isEmpty() && UserContext.isCurrentUserAdministrator()) {
                return ok(register.render("page.user.create", null, true));
            } else {
                return ok(register.render("page.user.create", null, false));
            }
        } catch (Exception e) {
            Logger.error(Messages.get("info.error.user.create"), e);
            return ok(info.render("info.error.user.create", Html.apply(Messages.get("info.error.user.create")), null, null));
        }
    }

    @Transactional
    @With(ControllerHandler.class)
    public Result getPasswordResetUI(String token) {
        try {
            User user = getUserByToken(token, "password-recovery", 24);

            if (user == null) {
                return badRequest("Invalid token");
            }
            return ok(password_reset.render("", user.email, token));

        } catch (Exception e) {
            Logger.error("Failed to get password reset UI {}", e);
            return badRequest(e.getMessage());
        }
    }

    @Transactional
    @With(ControllerHandler.class)
    @BodyParser.Of(BodyParser.Json.class)
    public Result registerWithGoogleId() {
        try {
            JsonNode body = request().body().asJson();
            String email = body.has("email") ? body.findPath("email").textValue().toLowerCase() : StringUtils.BLANK;
            String name = body.has("name") ? body.findPath("name").textValue() : StringUtils.BLANK;
            String googleId = body.has("googleId") ? body.findPath("googleId").textValue() : StringUtils.BLANK;
            String googleToken = body.has("googleToken") ? body.findPath("googleToken").textValue() : StringUtils.BLANK;

            if (OAuthUtils.verifyGoogleToken(googleToken)) {
                User u = userService.findOneByEmail(email);
                if (u == null) {
                    User user = new User();
                    user.email = email;
                    user.name = name;
                    user.googleId = googleId;
                    user.role = User.UserRoleEnum.GENERAL;

                    userService.doCreate(user);

                    mailer.SendUserVerificationEmail(user, request().host(), null, false);
                    Logger.info("成功注册用户： " + email);
                    userContext.SetCurrentUser(user);
                    return ok(user.toJsonObject());
                }
                return badRequest("info.error.user.create");
            }
            return badRequest("info.error.user.validateGoogleToken");
        } catch (Exception e) {
            Logger.error(Messages.get("info.error.user.createOrUpdate"), e);
            return badRequest("info.error.user.createOrUpdate");
        }
    }

    @Transactional
    @With(ControllerHandler.class)
    @BodyParser.Of(BodyParser.Json.class)
    public Result registerWithFacebookId() {
        try {
            JsonNode body = request().body().asJson();
            String email = body.has("email") ? body.findPath("email").textValue().toLowerCase() : StringUtils.BLANK;
            String name = body.has("name") ? body.findPath("name").textValue() : StringUtils.BLANK;
            String facebookId = body.has("facebookId") ? body.findPath("facebookId").textValue() : StringUtils.BLANK;

            User u = userService.findOneByEmail(email);
            if (u == null) {
                User user = new User();
                user.email = email;
                user.name = name;
                user.facebookId = facebookId;
                user.role = User.UserRoleEnum.GENERAL;

                userService.doCreate(user);

                mailer.SendUserVerificationEmail(user, request().host(), null, false);
                Logger.info("成功注册用户： " + email);
                userContext.SetCurrentUser(user);
                return ok(user.toJsonObject());
            }
            return badRequest("info.error.user.create");
        } catch (Exception e) {
            Logger.error(Messages.get("info.error.user.createOrUpdate"), e);
            return badRequest("info.error.user.createOrUpdate");
        }
    }

    @Transactional
    @With(ControllerHandler.class)
    @BodyParser.Of(BodyParser.Json.class)
    public Result registerByUser() {
        try {
            JsonNode body = request().body().asJson();
            String email = body.has("email") ? body.findPath("email").textValue().toLowerCase() : StringUtils.BLANK;
            String password = body.has("password")? body.findPath("password").textValue() : StringUtils.BLANK;

            User u = userService.findOneByEmail(email);

            if(u != null){
                return badRequest(Messages.get("info.error.user.duplicate"));
            }
            if (StringUtils.isNotBlank(password)) {
                password = this.crpto.sign(password);
                User newUser = new User();
                newUser.email = email;
                newUser.password = password;
                newUser.role = User.UserRoleEnum.GENERAL;

                userService.doCreate(newUser);

                mailer.SendUserVerificationEmail(newUser, request().host(), null, false);
                Logger.info("成功注册用户： " + email);
                userContext.SetCurrentUser(newUser);
                return ok(newUser.toJsonObject());
            } else{
               return registerByAdmin();
            }
        } catch (Exception e) {
            Logger.error(Messages.get("info.error.user.createOrUpdate"), e);
            return badRequest("info.error.user.createOrUpdate");
        }
    }

    @Transactional
    @With(ControllerHandler.class)
    @BodyParser.Of(BodyParser.Json.class)
    public Result registerByAdmin(){
        JsonNode body = request().body().asJson();
        String email = body.has("email") ? body.findPath("email").textValue().toLowerCase() : StringUtils.BLANK;
        String role = body.has("role") ? body.findPath("role").textValue() : StringUtils.BLANK;

        User u = userService.findOneByEmail(email);

        if(u != null){
            return badRequest(Messages.get("info.error.user.duplicate"));
        }

        if (UserContext.GetCurrentUserEmail().isEmpty()) {
            Logger.warn("请先登录");
            return unauthorized("false");
        } else if (!UserContext.isCurrentUserAdministrator()) {
            Logger.warn("不是管理员请勿创建用户");
            return unauthorized("false");
        } else {

            String password = this.crpto.sign(encryptPassword(generateRandomPassword()));

            User newUser = new User();
            newUser.email = email;
            newUser.password = password;
            newUser.role = User.UserRoleEnum.valueOf(role);

            userService.doCreate(newUser);

            mailer.SendUserVerificationEmail(newUser, request().host(), password, true);
            Logger.info("User created: {}", email);
            return ok(newUser.toJsonObject());
        }
    }

    private String generateRandomPassword(){
        Random rand = new Random();
        return "p" + rand.nextInt(Integer.MAX_VALUE);
    }

    @Transactional
    @With(ControllerHandler.class)
    @BodyParser.Of(BodyParser.Json.class)
    public Result updateExistingUser(){
        JsonNode body = request().body().asJson();
        String email = body.has("email") ? body.findPath("email").textValue().toLowerCase() : StringUtils.BLANK;
        String bibliography = body.has("bibliography") ? body.findPath("bibliography").textValue() : StringUtils.BLANK;
        String name = body.has("name") ? body.findPath("name").textValue() : StringUtils.BLANK;
        String mobile = body.has("tel") ? body.findPath("tel").textValue() : StringUtils.BLANK;
        String gender = body.has("gender") ? body.findPath("gender").textValue() : StringUtils.BLANK;
        int age = body.has("age") ? body.findPath("age").asInt() : 0;
        String address = body.has("address") ? body.findPath("address").textValue() : StringUtils.BLANK;
        String role = body.has("role") ? body.findPath("role").textValue() : StringUtils.BLANK;

        String userImage = body.has("image")? body.findPath("image").textValue() : StringUtils.BLANK;

        User u = userService.findOneByEmail(email);

        if (UserContext.GetCurrentUserEmail().isEmpty()) {
            Logger.warn("User is not login " + email);
            return unauthorized("请先登录");
        } else if (UserContext.isCurrentUserAdministrator() || u.email.equalsIgnoreCase(UserContext.GetCurrentUserEmail())) {

            u.name = name;
            u.bibliography = bibliography;

            if(StringUtils.isNotBlank(role))
                u.role = User.UserRoleEnum.valueOf(role);

            u.mobile = mobile;
            u.gender = gender;
            u.age = age;
            u.address = address;
            if(StringUtils.isNotBlank(userImage)) {
                u.userImage = userImage;
            }else{
                u.userImage = null;
            }

            userService.doUpdate(u);

            Logger.info("Update user {} successful.", name);
            return ok(u.toJsonObject());
        } else {
            Logger.warn("Access denied to edit user " + email);
            return unauthorized("info.error.illegal");
        }
    }

    @Transactional
    @With(ControllerHandler.class)
    public Result resendUserVerificationEmail(String email) {
        try {
            User u = userService.findOneByEmail(email);
            if (u != null) {
                mailer.SendUserVerificationEmail(u, request().host(), null, false);
                return ok(info.render("info.success.user.verificationEmail.send", Html.apply(Messages.get("info.success.user.verificationEmail.send")), null, null));
            } else {
                return ok(info.render("info.error.user.notFound", Html.apply(Messages.get("info.error.user.notFound")), null, null));
            }
        } catch (Exception t) {
            return ok(info.render("info.error.user.verificationEmail.send", Html.apply(Messages.get("info.error.user.verificationEmail.send")), null, null));
        }
    }

    @Transactional
    @With(ControllerHandler.class)
    @BodyParser.Of(BodyParser.Json.class)
    public Result passwordReset() {

        try {
            Http.RequestBody body = request().body();
            String token = body.asJson().findPath("token").textValue();
            String newPassword = body.asJson().findPath("password").textValue();

            User user = getUserByToken(token, "password-recovery", 24);

            if (user == null) {
                return badRequest("Invalid token");
            }

            user.password = this.crpto.sign(newPassword);
            userService.doUpdate(user);

            UserContext.SetCurrentUser(null);

            ObjectNode result = Json.newObject();
            result.put("success", "true");
            return ok(result);

        } catch (Exception e) {
            Logger.error("Failed to get password reset UI {}", e);
            return badRequest(e.getMessage());
        }
    }

    @Transactional
    @With(ControllerHandler.class)
    @BodyParser.Of(BodyParser.Json.class)
    public Result sendPasswordResetRequest() {
        try {
            Http.RequestBody body = request().body();
            String email = body.asJson().findPath("email").textValue();

            User user = userService.findOneByEmail(email);
            if (user == null) {
                return notFound("info.error.user.notFound");
            }

            mailer.SendPasswordResetEmail(user, request().host());
            ObjectNode result = Json.newObject();
            result.put("success", "true");
            return ok(result);
        } catch (Exception e) {
            Logger.error("Failed to find password ", e);
            return badRequest("Failed");
        }
    }


    @With(ControllerHandler.class)
    private String encryptPassword(String password) {
        String sha1 = "";
        try {
            MessageDigest crypt = MessageDigest.getInstance("SHA-1");
            crypt.reset();
            crypt.update(password.getBytes("UTF-8"));
            sha1 = byteToHex(crypt.digest());
        } catch (NoSuchAlgorithmException e) {
            Logger.error("Failed to generate sha-1 NoSuchAlgorithmException", e);
        } catch (UnsupportedEncodingException e) {
            Logger.error("Failed to generate sha-1 UnsupportedEncodingException", e);
        }
        return sha1;
    }

    private String byteToHex(final byte[] hash) {
        Formatter formatter = new Formatter();
        for (byte b : hash) {
            formatter.format("%02x", b);
        }
        String result = formatter.toString();
        formatter.close();
        return result;
    }

    @With(ControllerHandler.class)
    private User getUserByToken(String token, String tokenKeyword, int validHours) {

        String plainToken;
        if (token.contains(" ")) {
            token = token.replace(' ', '+');
        }
        try {
            plainToken = this.crpto.decryptAES(token);
        } catch (Exception e) {
            Logger.debug("Failed to decrypt token {}", token);
            return null;
        }

        Logger.debug("Got token {}", plainToken);

        String[] result = plainToken.split("\\|");

        if (result.length != 3 || !result[1].equals(tokenKeyword)) {
            return null;
        }

        try {
            // token valid for specified time
            DateFormat df = new SimpleDateFormat("EEE MMM dd kk:mm:ss z yyyy", Locale.ENGLISH);
            Date tokenDate = df.parse(result[2]);
            Calendar c = Calendar.getInstance();
            c.setTime(tokenDate);
            c.add(Calendar.HOUR, validHours);
            if (c.getTime().before(new Date())) {
                Logger.debug("Token expired");
                return null;
            }
        } catch (Exception e) {
            Logger.debug("Failed to parse date {}", e);
            return null;
        }

        String email = result[0];
        return userService.findOneByEmail(email);
    }

    @Transactional
    @With(ControllerHandler.class)
    public Result verifyUserEmail(String token) {
        try {
            User user = getUserByToken(token, "account-verify", 24);

            if (user != null) {
                user.verified = true;
                userService.doUpdate(user);
                return ok(info.render("page.user.verifyEmail", Html.apply(Messages.get("info.success.user.verify")), null, null));
            }

            return ok(info.render("page.user.verifyEmail", Html.apply(Messages.get("info.error.link.expired")), null, null));

        } catch (Exception e) {
            Logger.error("Unexpected error occurred in redirectWithAuthenticateToken {}", e);
            return ok(info.render("page.user.verifyEmail", Html.apply(Messages.get("info.error.link.expired")), null, null));
        }
    }

    @Transactional
    @With(ControllerHandler.class)
    public Result redirectWithAuthenticateToken(String token, String redirectUrl) {
        try {
            User user = getUserByToken(token, "quick-auth", 24);

            if (user != null) {
                userContext.SetCurrentUser(user);
            }

            return redirect(redirectUrl);

        } catch (Exception e) {
            Logger.error("Unexpected error occurred in redirectWithAuthenticateToken {}", e);
            return redirect(redirectUrl);
        }
    }

    @Transactional
    @With(ControllerHandler.class)
    public Result viewUserPersonalPage(String email) {
        try {
            User user = userService.findOneByEmail(email);

            List<Activity> attendedActivities = activityService.findAllActivityByUserEmail(email);


            if(user.role.equals(User.UserRoleEnum.GENERAL)) {
                return ok(user_personal_page.render("page.user.personalCenter", user, attendedActivities, null));

            }else{

                List<Activity> organizedActivities = activityService.findAllByOrganizerEmail(email);

                return ok(user_personal_page.render("page.user.personalCenter", user, attendedActivities, organizedActivities));
            }
        } catch (Exception e) {
            return ok(info.render("page.user.personalCenter", Html.apply(Messages.get("info.error")), null, null));
        }
    }

    @Transactional
    @With({ControllerHandler.class, Authenticated.class})
    public Result showUserTickets() {
        try {

            List<Ticket> ticketList = ticketService.findAllPaidByUserEmail(UserContext.GetCurrentUserEmail());

            // Generate QR code when first use
            String server_url = Play.application().configuration().getString("server_url");

            Map<Integer, Map<Integer, List<Ticket>>> validTicketMaps = new LinkedHashMap<Integer, Map<Integer, List<Ticket>>>();
            List<Ticket> invalidTicketList = new ArrayList<Ticket>();

            Iterator ticketIterator = ticketList.iterator();
            while(ticketIterator.hasNext()){
                Ticket ticket = (Ticket) ticketIterator.next();
                if(ticket.qrcode == null){
                    try {
                        ticket.qrcode = QRCodeUtil.encodeBase64(server_url + "/verifyTicket/" + ticket.bzid, QRCodeUtil.GetIconImage(Play.application().configuration().getString("QRIcon"), true));
                        ticketService.doUpdate(ticket);
                    } catch (Exception e) {
                        Logger.warn("Failed to update ticket QRCode: " + ticket.bzid, e);
                    }
                }
                if(ticket.checked || ticket.activity.status.equals(Activity.ActivityStatusEnum.EXPIRED) || new Date().after(ticket.activity.endTime)){
                    invalidTicketList.add(ticket);
                }else{
                    Date activityStartTime = TimeUtils.getActivityLocalDate(ticket.activity.startTime, ticket.activity.venue.city.GetTimeZoneObject());
                    if(!validTicketMaps.keySet().contains(activityStartTime.getYear()+1900)){
                        validTicketMaps.put(activityStartTime.getYear()+1900, new HashMap<Integer, List<Ticket>>());
                    }
                    if(!validTicketMaps.get(activityStartTime.getYear()+1900).keySet().contains(ticket.activity.startTime.getMonth()+1)){
                        validTicketMaps.get(activityStartTime.getYear()+1900).put(activityStartTime.getMonth() + 1, new ArrayList<Ticket>());
                    }
                    validTicketMaps.get(activityStartTime.getYear()+1900).get(activityStartTime.getMonth()+1).add(ticket);
                }
            }
            return ok(user_personal_page_tickets.render("page.user.myTickets", validTicketMaps, invalidTicketList));
        } catch (Exception e) {
            Logger.error("Read ticket failed. ", e);
            return ok(info.render("info.error.ticket.list", Html.apply(Messages.get("info.error.ticket.list")), null, null));
        }
    }

    @Transactional
    @With({ControllerHandler.class, Authenticated.class})
    public Result showUserActivities() {
        try {

            List<Activity> attendedActivities = activityService.findAllActivityByUserEmail(UserContext.GetCurrentUserEmail());

            if(UserContext.isCurrentUserGeneralUser()) {
                return ok(user_personal_page_activity.render("page.user.myEvents", attendedActivities, null));

            }else{

                List<Activity> organizedActivities = activityService.findAllByOrganizerEmail(UserContext.GetCurrentUserEmail());

                return ok(user_personal_page_activity.render("page.user.myEvents", attendedActivities, organizedActivities));
            }
        } catch (Exception e) {
            return ok(info.render("page.user.myEvents", Html.apply(Messages.get("info.error")), null, null));
        }
    }

    @Transactional
    @With({ControllerHandler.class, Authenticated.class})
    public Result updateUserInformation() {
        try {

            User user = userService.findOneByEmail(UserContext.GetCurrentUserEmail());
            if(UserContext.isCurrentUserAdministrator()) {
                return ok(user_personal_page_update_information.render("page.user.update", user, true));
            }
            return ok(user_personal_page_update_information.render("page.user.update", user, false));
        } catch (Exception e) {
            return ok(info.render("page.user.update", Html.apply(Messages.get("info.error")), null, null));
        }
    }

    @Transactional
    @With({ControllerHandler.class, Authenticated.class})
    public Result updateUserPassword() {
        try {
            User user = userService.findOneByEmail(UserContext.GetCurrentUserEmail());
            return ok(user_personal_page_update_password.render("page.user.changePassword", user));

        } catch (Exception e) {
            return ok(info.render("page.user.update", Html.apply(Messages.get("info.error")), null, null));
        }
    }

    @Transactional
    @With({ControllerHandler.class, Authenticated.class})
    public Result updateOrganizerSetting() {
        try {
            User user = userService.findOneByEmail(UserContext.GetCurrentUserEmail());
            UserPaymentRequirement userPaymentRequirement = userPaymentRequirementService.findOneByUserEmail(UserContext.GetCurrentUserEmail());
            List<PaymentMethod> paymentMethods = paymentMethodService.findAll();
            return ok(user_personal_page_update_organizer_setting.render("page.user.organizerSetting", user, paymentMethods, userPaymentRequirement));

        } catch (Exception e) {
            return ok(info.render("page.user.update", Html.apply(Messages.get("info.error")), null, null));
        }
    }

    @Transactional
    @With(ControllerHandler.class)
    public Result viewOrganizerPage(String userDbid) {
        try {
            User organizer = userService.findOneByDbid(Long.valueOf(userDbid));

            Double organizerRating = OrganizerRate.getOrganizerRate(organizer.email);

            Map<Activity, ActivityImage> expiredOrganizedActivities = new HashMap<Activity, ActivityImage>();

            Map<Activity, Map<Long, ActivityImage>> activeOrganizedActivities = new HashMap<Activity, Map<Long, ActivityImage>>();

            List<Activity> expiredActivities = activityService.findAllByOrganizerEmailAndStatus(organizer.email, Activity.ActivityStatusEnum.EXPIRED);

            List<Activity> activeActivities = activityService.findAllByOrganizerEmailAndStatus(organizer.email, Activity.ActivityStatusEnum.PUBLISH);

            expiredActivities.forEach(a -> {
                expiredOrganizedActivities.put(a, activityImageService.findIndexImageByActivityDbid(a.dbid));
            });

            activeActivities.forEach(a -> {
                activeOrganizedActivities.put(a, new HashMap<Long, ActivityImage>(){{put(ticketService.findPaidUniqueUserByActivityDbid(a.dbid), activityImageService.findIndexImageByActivityDbid(a.dbid));}});
            });

            return ok(organizer_page.render("page.organizer.organizerPage", organizer, organizerRating, expiredOrganizedActivities, activeOrganizedActivities));

        } catch (Exception e) {
            System.out.println(e);
            return ok(info.render("info.error.organizer.loadPage", Html.apply(Messages.get("info.error.page.notFound")), null, null));
        }
    }
}
