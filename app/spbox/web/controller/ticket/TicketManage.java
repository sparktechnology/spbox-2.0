package spbox.web.controller.ticket;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import play.Logger;
import play.Play;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import play.twirl.api.Html;
import spbox.UserContext;
import spbox.model.entity.activity.Activity;
import spbox.model.entity.activity.ActivityImage;
import spbox.model.entity.ticket.Ticket;
import spbox.model.entity.ticket.TicketType;
import spbox.model.entity.user.User;
import spbox.model.service.activity.ActivityImageService;
import spbox.model.service.activity.ActivityService;
import spbox.model.service.ticket.TicketService;
import spbox.model.service.ticket.TicketTypeService;
import spbox.model.service.user.UserService;
import spbox.web.annotation.Authenticated;
import spbox.web.annotation.ControllerHandler;
import spbox.web.annotation.Organizer;
import spbox.web.utilities.QRCodeUtil;
import spbox.web.utilities.StringUtils;
import spbox.web.utilities.TimeUtils;
import spbox.web.utilities.ticket.TicketUtility;
import spbox.web.view.html.info;
import spbox.web.view.ticket.html.ticket_manager;

import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

public class TicketManage extends Controller {



    @Inject
    private ActivityService activityService;

    @Inject
    private ActivityImageService activityImageService;

    @Inject
    private UserService userService;

    @Inject
    private TicketTypeService ticketTypeService;

    @Inject
    private TicketService ticketService;

    @Inject
    private TicketUtility ticketUtility;

    @Transactional
    @With({ControllerHandler.class, Authenticated.class})
    public Result createTicket(String transactionInfo, String ticketsArray, String amount) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode tickets = mapper.readTree(ticketsArray);
            JsonNode tInfo = mapper.readTree(transactionInfo);
            String activityBzid = tInfo.path("activity_id").asText();
            String token = tInfo.path("token").asText();

            Activity a = activityService.findOneByBzid(activityBzid);
            User user = userService.findOneByEmail(UserContext.GetCurrentUserEmail());

            String genTicketResult = ticketUtility.processPayment(user, a, tickets, token, Integer.valueOf(amount));
            if (genTicketResult != null) {
                return badRequest(genTicketResult);
            }
        } catch(Exception e) {
            Logger.debug(e.getLocalizedMessage());
        }

        ObjectNode result = Json.newObject();
        result.put("success", true);

        return ok(result);
    }

    @Transactional  //to verify a ticket is valid or not
    @With({ControllerHandler.class, Authenticated.class})
    public Result verifyTicketUI(String bzid) {
        try {
            Ticket t = ticketService.findOneByBzid(bzid);
            if (t != null && t.paid) {
                if (t.checked == false) {
                    String shtml = ticketUtility.generateValidTicketUIDetail(t);
                    return ok(info.render("info.success.ticket.verify", Html.apply(shtml.toString()), "/ticket_manager/all", "page.ticketManager"));
                } else {
                    String shtml = ticketUtility.generateInvalidTicketUIDetail(t);
                    return ok(info.render("info.success.ticket.verify", Html.apply(shtml), "/ticket_manager/all", "page.ticketManager"));
                }
            } else {
                return ok(info.render("info.error.ticket.notFound", Html.apply("Ticket verification failed"), "/ticket_manager/all", "page.ticketManager"));
            }
        } catch (Exception e) {
            Logger.error("Verify ticket failed: {}", e);
            return ok(info.render("info.error.ticket.verify", Html.apply(Messages.get("info.error.ticket.verify")), "/ticket_manager/all", "page.ticketManager"));
        }
    }

    @Transactional
    @With({ControllerHandler.class, Organizer.class})
    public Result ticketManagerUI(String activityBzid) {
        try {
            if (StringUtils.isBlank(activityBzid)) {
                return ok(info.render("info.error.ticket.list", Html.apply(Messages.get("info.error.activity.id.empty")), null, null));
            }

            List<Activity> activityList = UserContext.isCurrentUserAdministrator() ? activityService.findAllBrief()
                    : activityService.findAllByOrganizerEmail(UserContext.GetCurrentUserEmail());

            Map<String, Map<TicketType, List<Ticket>>> ticketMap = new HashMap<String, Map<TicketType, List<Ticket>>>();


            List<Ticket> ticketList = ticketService.findAllByActivityBzid(activityBzid);

            List<TicketType> ticketTypes = ticketTypeService.findAllByActivityBzid(activityBzid);

            if (!ticketTypes.isEmpty()) {
                ticketMap.put(activityBzid, new HashMap<TicketType, List<Ticket>>());
                for (TicketType tt : ticketTypes) {
                    ticketMap.get(activityBzid).put(tt, ticketList.stream().filter(t -> t.ticketType == tt).collect(Collectors.toList()));
                }
            }

            long numberOfChecktedTicket = ticketList.stream()
                    .filter(t -> t.checked).count();

            return ok(ticket_manager.render("page.ticketManager.list", activityBzid, activityList, ticketMap, numberOfChecktedTicket));

        } catch (Exception e) {
            Logger.error(Messages.get("info.error.ticketManager.list"), e);
            return ok(info.render("info.error.ticketManager.list", Html.apply(Messages.get("info.error.ticketManager.list")), null, null));
        }
    }

    @Transactional
    @With({ControllerHandler.class, Organizer.class})
    public Result cutTicketUI(String bzid) {
        try {

            Ticket t = UserContext.isCurrentUserAdministrator() ? ticketService.findOneByBzid(bzid)
                    : ticketService.findOneByBzidAndOrganizerEmail(bzid, UserContext.GetCurrentUserEmail());
            if (t != null && t.deleted == false) {
                t.checked = true;
                t.checkedTime = TimeUtils.getUTCDate(new Date());
                t.checkedBy = UserContext.GetCurrentUserEmail();
                ticketService.doUpdate(t);
                return ok(info.render("info.success.ticket.cut", Html.apply(Messages.get("info.success.ticket.cut")), "/ticket_manager/"+t.activity.bzid, "page.ticketManager"));
            } else {
                return ok(info.render("info.error.ticket.notFound", Html.apply(Messages.get("info.error.ticket.notFound")), "/ticket_manager/all", "page.ticketManager"));
            }
        } catch (Exception e) {
            Logger.error(Messages.get("info.error.ticket.cut"), e);
            return ok(info.render("info.error.ticket.cut", Html.apply(Messages.get("info.error.ticket.cut")), "/ticket_manager/all", "page.ticketManager"));
        }
    }
}
