package spbox.web.controller.activity;

import com.amazonaws.services.s3.model.DeleteObjectsRequest;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import play.Logger;
import play.Play;
import play.data.DynamicForm;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import play.libs.Json;
import play.mvc.*;
import play.twirl.api.Html;
import spbox.BuiltInArticles;
import spbox.OrganizerRate;
import spbox.UserContext;
import spbox.model.entity.Image;
import spbox.model.entity.activity.*;
import spbox.model.entity.email.Article;
import spbox.model.entity.location.City;
import spbox.model.entity.ticket.Ticket;
import spbox.model.entity.ticket.TicketType;
import spbox.model.entity.ticket.TicketTypeManual;
import spbox.model.entity.user.User;
import spbox.model.s3.S3;
import spbox.model.service.ImageService;
import spbox.model.service.activity.*;
import spbox.model.service.location.CityService;
import spbox.model.service.location.CountryService;
import spbox.model.service.ticket.TicketService;
import spbox.model.service.ticket.TicketTypeManualService;
import spbox.model.service.ticket.TicketTypeService;
import spbox.model.service.user.UserService;
import spbox.web.annotation.Administrator;
import spbox.web.annotation.Authenticated;
import spbox.web.annotation.ControllerHandler;
import spbox.web.annotation.Organizer;
import spbox.web.utilities.*;
import spbox.web.utilities.activity.ActivityUtility;
import spbox.web.view.activity.html.activity;
import spbox.web.view.activity.html.activity_review;
import spbox.web.view.activity.html.activity_manager;
import spbox.web.view.activity.html.create;
import spbox.web.view.activity.html.activity_category_manager;
import spbox.web.view.activity.html.edit_activity_category;
import spbox.web.view.email.html.edit_email_settings;
import spbox.web.view.html.info;

import javax.inject.Inject;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static play.data.Form.form;

public class ActivityManage extends Controller {

    @Inject
    private ActivityService activityService;

    @Inject
    private ActivityImageService activityImageService;

    @Inject
    private ActivityCategoryService activityCategoryService;

    @Inject
    private ActivityUtility activityUtility;

    @Transactional
    @With({ControllerHandler.class, Administrator.class})
    public Result changeStar(String bzid) {
        try {
            Activity a = activityService.findOneByBzid(bzid);
            a.star = !a.star;

            ObjectNode result = HttpUtils.buildHttpResult("response", Messages.get("info.success.activity.star." + String.valueOf(a.star).toLowerCase()));
            return ok(result);
        } catch (Exception e) {
            Logger.error(Messages.get("info.error.activity.changeStar"), e);

            ObjectNode result = HttpUtils.buildHttpResult("error", "info.error.activity.changeStar");
            return badRequest(result);
        }
    }

    @Transactional
    @With({ControllerHandler.class, Administrator.class})
    public Result GetUnProcessedInfo(){

        Integer publishRequestCount = activityService.findCountByStatus(Activity.ActivityStatusEnum.REQUEST_PUBLISH).intValue();
        ObjectNode result = HttpUtils.buildHttpResult("publishRequests", publishRequestCount.toString());

        return ok(result);
    }

    @Inject
    private Mailer mailer;

    @Inject
    private CityService cityService;

    @Inject
    private UserService userService;

    @Inject
    private TicketTypeService ticketTypeService;

    @Inject
    private CountryService countryService;

    @Inject
    private BuiltInArticles builtInArticles;

    @Transactional
    @With({ControllerHandler.class, Authenticated.class})
    public Result createUI(String bzid) {
        try {
            if (bzid.equalsIgnoreCase("new")) {

                if (UserContext.isCurrentUserAdministrator()) {
                    User currentUser = userService.findOneByEmail(UserContext.GetCurrentUserEmail());
                    return ok(create.render("page.activity.create", null, null, null, null, null, currentUser, countryService.findAll(), activityCategoryService.findAll()));
                } else {
                    Article createActivityTerm = builtInArticles.GetCreateActivityTerm();
                    User currentUser = userService.findOneByEmail(UserContext.GetCurrentUserEmail());
                    return ok(create.render("page.activity.create", null, null, null, createActivityTerm, null, currentUser, countryService.findAll(), activityCategoryService.findAll()));
                }
            } else {
                Activity activity = activityService.findOneByBzid(bzid);
                if (activityUtility.isEditable(activity)) {

                    User organizer = userService.findOneByEmail(activity.organizer);
                    List<TicketType> ticket_type_list = ticketTypeService.findAllByActivityDbid(activity.dbid);

                    ActivityImage indexImages = activityImageService.findIndexImageByActivityDbid(activity.dbid);
                    List<ActivityImage> descriptionImages = activityImageService.findDescriptionImagesByActivityDbid(activity.dbid);

                    return ok(create.render("page.activity.create", activity, indexImages, descriptionImages, null, ticket_type_list, organizer, countryService.findAll(), activityCategoryService.findAll()));
                } else {
                    return ok(info.render("info.error.illegal", Html.apply(Messages.get("info.error.activity.notFound")), null, null));
                }
            }
        } catch (Exception e) {
            Logger.error(Messages.get("info.error.activity.createOrUpdate"), e);
            return ok(info.render("info.error.activity.createOrUpdate", Html.apply(Messages.get("info.error.activity.createOrUpdate")), "/activity_manager", "page.activity.manage"));
        }
    }

    @Transactional
    @With({ControllerHandler.class, Authenticated.class})
    public Result publishActivity(String bzid) {
        try {
            Activity activity = activityService.findOneByBzid(bzid);
            if (activity != null) {
                if (UserContext.isCurrentUserAdministrator()) {
                    activity.status = Activity.ActivityStatusEnum.PUBLISH;
                    activityService.doUpdate(activity);
                } else if (UserContext.GetCurrentUserEmail().equals(activity.organizer)) {
                    activity.status = Activity.ActivityStatusEnum.REQUEST_PUBLISH;
                    activityService.doUpdate(activity);
                } else {
                    return forbidden("info.error.illegal");
                }
                ObjectNode result = Json.newObject();
                result.put("success", true);
                return ok(result);
            } else {
                return badRequest("info.error.activity.id.invalid");
            }

        } catch (Exception e) {
            Logger.error("info.error.activity.publish", e);
            return badRequest("info.error.activity.publish");
        }
    }

    @Transactional
    @With({ControllerHandler.class, Authenticated.class})
    public Result createActivity() {
        try {
            DynamicForm activityForm = form().bindFromRequest();
            if (activityForm.hasErrors()) {
                return badRequest("info.error.activity.createOrUpdate");
            } else {

                String id = activityForm.get("id").trim();
                String name = activityForm.get("name").trim();
                Long categoryDbid = Long.valueOf(activityForm.get("category"));
                String brief_description = activityForm.get("brief_description").trim().replaceAll("\\r\\n|\\r|\\n", " ");
                String description = activityForm.get("description").trim();
                String status = UserContext.isCurrentUserAdministrator() ? activityForm.get("status") : Activity.ActivityStatusEnum.UNPUBLISH.name();

                Logger.info("start time input string " + activityForm.get("start_datetime").trim());
                Logger.info("end time input string " + activityForm.get("end_datetime").trim());

                String start_datetime_string = activityForm.get("start_datetime").trim();
                String end_datetime_string = activityForm.get("end_datetime").trim();

                String organizer = activityForm.get("organizer").trim();
                String organizer_phone = activityForm.get("organizer-phone").trim();
                String organizer_name = activityForm.get("organizer-name").trim();

                String venueName = activityForm.get("venue_name").trim();
                String venueStreetAddress = activityForm.get("venue_street_address").trim();
                String venuePostalCode = activityForm.get("venue_postal_code").trim();
                String cityBzid = activityForm.get("venue_city").trim();


                Boolean bname = Boolean.valueOf(activityForm.get("bname"));
                Boolean btel = Boolean.valueOf(activityForm.get("btel"));
                Boolean bgender = Boolean.valueOf(activityForm.get("bgender"));
                Boolean bage = Boolean.valueOf(activityForm.get("bage"));
                Boolean baddress = Boolean.valueOf(activityForm.get("baddress"));

                // create a new activity
                if (StringUtils.isBlank(id)) {

                    Activity activity = new Activity();
                    activity.name = name;
                    activity.category = activityCategoryService.findOneByDbid(categoryDbid);
                    activity.briefDescription = brief_description;
                    activity.description = description;
                    activity.status = Activity.ActivityStatusEnum.valueOf(status);
                    activity.organizer = organizer;

                    City city = cityService.findOneByBzid(cityBzid);
                    ActivityVenue activityVenue = new ActivityVenue();
                    activityVenue.city = city;
                    activityVenue.postalCode = venuePostalCode;
                    activityVenue.name = venueName;
                    activityVenue.streetAddress = venueStreetAddress;

                    activity.venue = activityVenue;

                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                    df.setTimeZone(city.GetTimeZoneObject());
                    activity.startTime = TimeUtils.getUTCDate(df.parse(start_datetime_string));
                    activity.endTime = TimeUtils.getUTCDate(df.parse(end_datetime_string));

                    activity.dateTime = activityUtility.extractDateTime(df.parse(start_datetime_string), df.parse(end_datetime_string));

                    ActivityProperty activityProperty = new ActivityProperty();
                    activityProperty.bname = bname;
                    activityProperty.btel = btel;
                    activityProperty.bgender = bgender;
                    activityProperty.bage = bage;
                    activityProperty.baddress = baddress;
                    activityProperty.emailToBuyerEnabled = true;
                    activityProperty.emailToOrganizerEnabled = true;
                    activityProperty.emailCustomized = false;

                    activity.property = activityProperty;

                    activityService.doCreate(activity);

                    if (UserContext.GetCurrentUserEmail().equalsIgnoreCase(organizer)) {
                        activityUtility.updateUserInformation(organizer_phone, organizer_name);
                    }

                    Result r = activityUtility.extractTicketTypeFromPost(activity, activityForm, "new");
                    if (r != null) return r;

                    Logger.debug("活动 {} 创建成功", name);
                    mailer.sendEmailNotificationToOrganizer(activity, request().host());

                    ObjectNode result;
                    result = HttpUtils.buildHttpResult("bzid", activity.bzid);

                    activityUtility.updateActivityPrice(activity.bzid);

                    return ok(result);
                }
                // update a activity
                else {
                    Activity activity = activityService.findOneByBzid(id);

                    if (activityUtility.isEditable(activity)) {

                        activity.name = name;
                        activity.category = activityCategoryService.findOneByDbid(categoryDbid);
                        activity.briefDescription = brief_description;
                        activity.description = description;
                        activity.status = Activity.ActivityStatusEnum.valueOf(status);

                        activity.organizer = organizer;

                        activity.venue.name = venueName;
                        activity.venue.streetAddress = venueStreetAddress;
                        activity.venue.postalCode = venuePostalCode;
                        if (activity.venue.city.bzid != cityBzid) {
                            activity.venue.city = cityService.findOneByBzid(cityBzid);
                        }

                        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");

                        df.setTimeZone(activity.venue.city.GetTimeZoneObject());

                        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm");
                        Date startTime = formatter.parseDateTime(start_datetime_string).toDate();
                        Date endTime = formatter.parseDateTime(end_datetime_string).toDate();


                        Logger.info("activity start time @ local timezone " + startTime);
                        Logger.info("activity end time @ local timezone " + endTime);

                        activity.dateTime = activityUtility.extractDateTime(startTime, endTime);

                        activity.startTime = TimeUtils.getUTCDate(df.parse(start_datetime_string));
                        activity.endTime = TimeUtils.getUTCDate(df.parse(end_datetime_string));

                        activity.property.bname = bname;
                        activity.property.btel = btel;
                        activity.property.bgender = bgender;
                        activity.property.bage = bage;
                        activity.property.baddress = baddress;

                        activityService.doUpdate(activity);

                        if (UserContext.GetCurrentUserEmail().equalsIgnoreCase(organizer)) {
                            User user = userService.findOneByEmail(UserContext.GetCurrentUserEmail());
                            if (organizer_phone != null) {
                                user.mobile = organizer_phone;
                            }
                            if (organizer_name != null) {
                                user.name = organizer_name;
                            }
                        }

                        Result r_delete = activityUtility.extractTicketTypeFromPost(activity, activityForm, "delete");
                        if (r_delete != null) return r_delete;

                        Result r_update = activityUtility.extractTicketTypeFromPost(activity, activityForm, "update");
                        if (r_update != null) return r_update;

                        Result r_new = activityUtility.extractTicketTypeFromPost(activity, activityForm, "new");
                        if (r_new != null) return r_new;

                        mailer.sendEmailNotificationToOrganizer(activity, request().host());

                        ObjectNode result = HttpUtils.buildHttpResult("success", "true");


                        if(activityForm.get("activity_image_count") != null){
                            ActivityImage toUpdateImage = activityImageService.findIndexImageByActivityDbid(activity.dbid);
                            if(toUpdateImage != null) {
                                activityImageService.doDelete(toUpdateImage);
                            }
                            
                            result = HttpUtils.buildHttpResult("bzid", activity.bzid);
                        }

                        if(activityForm.get("description_image_count") != null){
                            result = HttpUtils.buildHttpResult("bzid", activity.bzid);
                        }

                        activityUtility.updateActivityPrice(activity.bzid);

                        return ok(result);
                    } else {
                        return badRequest("info.error.activity.id.empty");
                    }
                }
            }
        } catch (Exception e) {
            Logger.error("info.error.activity.createOrUpdate", e);
            return ok(info.render("info.error.activity.createOrUpdate", Html.apply(Messages.get("info.error.activity.createOrUpdate")), "/activity_manager", "page.activity.manage"));
        }
    }


    @Inject
    private ReviewService reviewService;

    @Inject
    private ComplexEventUtils complexEventUtils;


    @Transactional
    @With(ControllerHandler.class)
    public Result getActivityUI(String bzid, String preview, String maxResult, String page) {
        try {
            Activity a = activityService.findOneByBzid(bzid);

            complexEventUtils.sendAndSavePageViewEvent(a.name, HttpUtils.getClientIP(request()));

            User user;
            if(StringUtils.isNotBlank(UserContext.GetCurrentUserEmail())){
                user = userService.findOneByEmail(UserContext.GetCurrentUserEmail());
            }else{
                user = null;
            }

            List<Review> review = reviewService.findAllByActivityBzid(bzid, maxResult, page);

            if (a != null) {

                Long reviewCount = reviewService.findCountByActivityBzid(a.bzid);

                if (preview.trim().equals("true")) {
                    Article terms = builtInArticles.GetTerms();

                    List<Activity> recommendedActivity = activityUtility.getRecommendedActivity(a, Activity.ActivityStatusEnum.PUBLISH);

                    ActivityImage indexImage = activityImageService.findIndexImageByActivityDbid(a.dbid);
                    List<ActivityImage> descriptionImages = activityImageService.findDescriptionImagesByActivityDbid(a.dbid);

                    User organizer = userService.findOneByEmail(a.organizer);
                    Double organizerRate = OrganizerRate.getOrganizerRate(a.organizer);

                    return ok(activity.render("page.activity.view", a, indexImage, descriptionImages, terms, ticketTypeService.findAllByActivityDbid(a.dbid), "preview", user, review, recommendedActivity, organizer, organizerRate, reviewCount));
                } else {
                    if (a.status.equals(Activity.ActivityStatusEnum.UNPUBLISH)) {
                        return ok(info.render("activity.status."+Activity.ActivityStatusEnum.UNPUBLISH, Html.apply(Messages.get("activity.status." + Activity.ActivityStatusEnum.UNPUBLISH)), "/activity_manager", "page.activity.manage"));
                    }

                    List<TicketType> theTicketType = ticketTypeService.findAllByActivityDbid(a.dbid);
                    String paymentApiKey = Play.application().configuration().getString("stripe_api_key_client");
                    Article terms = builtInArticles.GetTerms();

                    ActivityImage indexImage = activityImageService.findIndexImageByActivityDbid(a.dbid);
                    List<ActivityImage> descriptionImages = activityImageService.findDescriptionImagesByActivityDbid(a.dbid);

                    User organizer = userService.findOneByEmail(a.organizer);
                    Double organizerRate = OrganizerRate.getOrganizerRate(a.organizer);

                    List<Activity> recommendedActivityMap = activityUtility.getRecommendedActivity(a, Activity.ActivityStatusEnum.PUBLISH);

                    return ok(activity.render("page.activity.view", a, indexImage, descriptionImages, terms, theTicketType, paymentApiKey, user, review, recommendedActivityMap, organizer, organizerRate, reviewCount));
                }
            } else {
                return ok(info.render("info.error.activity.notFound", Html.apply(Messages.get("info.error.activity.notFound")), "/activity_manager", "page.activity.manage"));
            }
        } catch (Exception e) {
            Logger.error("Read activity details failed. ", e);
            return ok(info.render("info.error.activity.view", Html.apply(Messages.get("info.error.activity.view")), "/activity_manager", "page.activity.manage"));
        }
    }

    @Transactional
    @With(ControllerHandler.class)
    public Result getActivityReviewUI(String bzid, String maxResult, String page) {
        try {

            User user;
            if(StringUtils.isNotBlank(UserContext.GetCurrentUserEmail())){
                user = userService.findOneByEmail(UserContext.GetCurrentUserEmail());
            }else{
                user = null;
            }

            List<Review> review = reviewService.findAllByActivityBzid(bzid, maxResult, page);

            return ok(activity_review.render("page.activity.view", user, review));

        } catch (Exception e) {
            Logger.error("Read activity details failed. ", e);
            return ok(info.render("info.error.activity.view", Html.apply(Messages.get("info.error.activity.view")), "/activity_manager", "page.activity.manage"));
        }
    }

    @Inject
    private ImageService imageService;

    @Inject
    private TicketService ticketService;

    @Inject
    private ImageUtils imageUtils;

    @Inject
    private TicketTypeManualService ticketTypeManualService;


    @Transactional
    @With({ControllerHandler.class, Organizer.class})
    public Result activityManagerUI() {
        try {
            List<Activity> activity_list = UserContext.isCurrentUserAdministrator() ? activityService.findAllBrief()
                    : activityService.findAllByOrganizerEmail(UserContext.GetCurrentUserEmail());
            return ok(activity_manager.render("page.activity.list", activity_list));
        } catch (Exception e) {
            Logger.error("Read activity list failed: ", e);
            return ok(info.render("info.error.activity.list", Html.apply(Messages.get("info.error.activity.list")), null, null));
        }
    }

    @Transactional
    @With({ControllerHandler.class, Organizer.class})
    public Result getEmailSettingsUI(String bzid) {
        try {
            if (StringUtils.isBlank(bzid)) {
                return ok(info.render("info.error.activity.id.empty", Html.apply(Messages.get("info.error.activity.id.empty")), "/activity_manager", "page.activity.manage"));
            }

            Activity activity = activityService.findOneByBzid(bzid);
            if (activityUtility.isEditable(activity)) {
                return ok(edit_email_settings.render("page.email.setting", activity, builtInArticles.GetEmailTemplate(activity.bzid, true).content));
            } else {
                return ok(info.render("info.error.activity.notFound", Html.apply(Messages.get("info.error.activity.notFound")), "/activity_manager", "page.activity.manage"));
            }
        } catch (Exception e) {
            Logger.error("Failed to get email settings", e);
            String shtml = "获取邮件配置出错！";
            return ok(info.render("info.error.email.setting", Html.apply(Messages.get("info.error.email.setting")), "/activity_manager", "page.activity.manage"));
        }

    }

    @Transactional
    @With({ControllerHandler.class, Organizer.class})
    @BodyParser.Of(BodyParser.Json.class)
    public Result saveEmailSettings(String activityBzid)
    {
        try {
            Activity activity = activityService.findOneByBzid(activityBzid);

            if (activity == null) {
                return notFound();
            }

            JsonNode node = request().body().asJson();
            Boolean emailToBuyer = node.findPath("emailToBuyer").booleanValue();
            Boolean emailToOrganizer = node.findPath("emailToOrganizer").booleanValue();
            Boolean isEmailCustomized = node.findPath("isEmailCustomized").booleanValue();


            activity.property.emailToBuyerEnabled = emailToBuyer;
            activity.property.emailToOrganizerEnabled = emailToOrganizer;
            activity.property.emailCustomized = isEmailCustomized;

            activityService.doUpdate(activity);

            if (isEmailCustomized) {
                String template = node.findPath("content").textValue();
                builtInArticles.SetEmailTemplate(activity.bzid, template);
            }

            ObjectNode result = Json.newObject();
            result.put("success", true);
            return ok(result);
        }
        catch (Exception e)
        {
            Logger.error("Failed to save email settings", e);
            return badRequest("info.error.email.create");
        }
    }

    @Transactional
    @With({ControllerHandler.class, Organizer.class})
    public Result addImage(){
        try
        {
            Http.MultipartFormData body = request().body().asMultipartFormData();
            Http.MultipartFormData.FilePart file = body.getFile("file");
            String host = request().host();

            if (file != null) {
                File fileObj = file.getFile();

                FileInputStream fin = new FileInputStream(fileObj);
                byte fileContent[] = new byte[(int) fileObj.length()];
                fin.read(fileContent);

                Image image = new Image();
                image.contentType = file.getContentType();
                image.data = fileContent;

                imageService.doCreate(image);

                Logger.debug("New image is added " + image.bzid);
                return ok("http://" + host + "/image/" + image.bzid);
            } else {
                return badRequest("info.error.activity.image.add");
            }
        } catch (Exception e) {
            Logger.error("Failed to add image {}", e);
            return badRequest("info.error.activity.image.add");
        }
    }

    @Transactional
    @With({ControllerHandler.class, Authenticated.class})
    public Result deleteActivityImage(Long id) throws Exception
    {
        //TODO: permission check
        Logger.debug("Deleting activity description image " + id);

        ActivityImage activityImage = activityImageService.findOneByDbid(id);

        List<DeleteObjectsRequest.KeyVersion> keys = new ArrayList<DeleteObjectsRequest.KeyVersion>();
        keys.add(new DeleteObjectsRequest.KeyVersion(activityImage.keyname));

        S3.delete(keys);

        activityImageService.doDelete(activityImage);

        ObjectNode result = Json.newObject();
        result.put("success", true);
        return ok(result);

    }

    @Transactional
    @With({ControllerHandler.class, Authenticated.class})
    public Result uploadActivityImage(String bzid) throws Exception{

        Http.MultipartFormData body = request().body().asMultipartFormData();
        List<Http.MultipartFormData.FilePart> file = body.getFiles();

        if (file != null) {
            Activity activity = activityService.findOneByBzid(bzid);
            for(Http.MultipartFormData.FilePart f : file) {
                File fileObj = f.getFile();

                if(f.getKey().equalsIgnoreCase("activity_image")){
                    activityUtility.saveFileToS3(ActivityImage.ActivityImageCategoryEnum.INDEX, fileObj, activity);
                    BufferedImage preview= imageUtils.previewImageBufferedImage(fileObj);
                    activityUtility.saveBufferedImageAsIndexImageToS3(preview, activity);
                }else if(f.getKey().equalsIgnoreCase("description_image")){
                    activityUtility.saveFileToS3(ActivityImage.ActivityImageCategoryEnum.DESCRIPTION, fileObj, activity);
                }
            }
        }
        ObjectNode result = Json.newObject();
        result.put("success", true);
        String location = "/activity/" + bzid + "?preview=true";
        result.put("redirect", location);
        return ok(result);
    }

    @Transactional
    @With({ControllerHandler.class, Organizer.class})
    public Result deleteActivityCheck(String bzid) {
        try {
            if (StringUtils.isBlank(bzid)) {
                Logger.error("No activity defined");
                return badRequest();
            } else { // Check if any tickets have been bought
                List<Ticket> tickets = ticketService.findAllByActivityBzid(bzid);
                if (tickets.size()!=0){
                    ObjectNode result = HttpUtils.buildHttpResult("error", "one or more event tickets have been sold");
                    return forbidden(result);
                }
                ObjectNode result = HttpUtils.buildHttpResult("success", "true");
                return ok(result);
            }
        } catch (Exception e) {
            Logger.error("Delete activity check failed: ", e);
            return badRequest();
        }
    }

    @Transactional
    @With({ControllerHandler.class, Organizer.class})
    public Result deleteActivity(String bzid) {
        try {
            if (StringUtils.isBlank(bzid)) {
                return ok(info.render("info.error.activity.id.empty", Html.apply(Messages.get("info.error.activity.id.empty")), "/activity_manager", "page.activity.manage"));
            }
            // delete a activity
            else {


                Activity activity = activityService.findOneByBzid(bzid);
                List<TicketType> ticketTypes = ticketTypeService.findAllByActivityDbid(activity.dbid);
                List<Ticket> tickets = ticketService.findAllByActivityBzid(activity.bzid);
                List<ActivityImage> images = activityImageService.findAllByActivityDbid(activity.dbid);
                List<Review> reviews = reviewService.findAllByActivityDbid(activity.dbid);

                if (activityUtility.isEditable(activity)) {

                    for(Ticket ticket: tickets) {
                        ticketService.doDelete(ticket);
                    }

                    for(TicketType ticketType: ticketTypes){
                        TicketTypeManual ticketTypeManual = ticketTypeManualService.findOneByTicketTypeDbid(ticketType.dbid);
                        if(ticketTypeManual != null) {
                            ticketTypeManualService.doDelete(ticketTypeManual);
                        }

                        ticketTypeService.doDelete(ticketType);
                    }

                    List<DeleteObjectsRequest.KeyVersion> keys = new ArrayList<DeleteObjectsRequest.KeyVersion>();
                    for(ActivityImage image : images){
                        keys.add(new DeleteObjectsRequest.KeyVersion(image.keyname));
                        activityImageService.doDelete(image);
                    }
                    keys.add(new DeleteObjectsRequest.KeyVersion(activity.indexPreviewImage.keyname));;

                    S3.delete(keys);

                    for(Review review : reviews){
                        reviewService.doDelete(review);
                    }

                    activityService.doDelete(activity);

                    return ok(info.render("info.success.activity.delete", Html.apply(Messages.get("info.success.activity.delete")), "/activity_manager", "page.activity.manage"));
                } else {
                    return ok(info.render("info.error.activity.notFound", Html.apply(Messages.get("info.error.activity.notFound")), "/activity_manager", "page.activity.manage"));
                }
            }

        } catch (Exception e) {
            Logger.error("Delete activity failed: ", e);
            return ok(info.render("info.error.activity.delete", Html.apply(Messages.get("info.error.activity.delete")), "/activity_manager", "page.activity.manage"));
        }
    }

    @Transactional
    @With({ControllerHandler.class, Administrator.class})
    public Result activityCategoryManager() {
        try {
            List<ActivityCategory> activityCategories = activityCategoryService.findAll();

            return ok(activity_category_manager.render("page.activityCategoryManager", activityCategories));

        } catch (Exception e) {
            Logger.error(Messages.get("info.error.user.list"), e);
            return ok(info.render("info.error.user.list", Html.apply(Messages.get("info.error.user.list")), "/system_manager", "page.systemManager"));
        }
    }

    @Transactional
    @With({ControllerHandler.class, Administrator.class})
    public Result editActivityCategoryUI(String dbid) {
        try {
            if (dbid.equalsIgnoreCase("new")) {
                return ok(edit_activity_category.render("page.activityCategory.create", null));
            } else {

                ActivityCategory ac = activityCategoryService.findOneByDbid(Long.valueOf(dbid));

                if (ac != null) {
                    return ok(edit_activity_category.render("page.activityCategory.update", ac));
                } else {
                    return ok(info.render("info.error.activityCategory.notFound", Html.apply(Messages.get("info.error.activityCategory.notFound")), "/activity_category_manager", "page.activityCategoryManager.list"));
                }
            }
        } catch (Exception e) {
            Logger.error(Messages.get("info.error.activityCategory.createOrUpdate") + e.getLocalizedMessage());
            return ok(info.render("info.error.activityCategory.createOrUpdate", Html.apply(Messages.get("info.error.activityCategory.createOrUpdate")), "/activity_category_manager", "page.activityCategoryManager.list"));
        }
    }

    @Transactional
    @With({ControllerHandler.class, Administrator.class})
    @BodyParser.Of(BodyParser.Json.class)
    public Result editActivityCategory() {
        try {
            Http.RequestBody body = request().body();
            String id = body.asJson().findPath("id").textValue();
            String activityCategoryCN = body.asJson().findPath("activityCategoryCN").textValue();
            String activityCategoryEN = body.asJson().findPath("activityCategoryEN").textValue();
            int activityCategoryIdx = body.asJson().findPath("activityCategoryIdx").asInt();

            if (StringUtils.isBlank(activityCategoryCN) || StringUtils.isBlank(activityCategoryEN)) {
                return badRequest("info.error.activityCategory.createOrUpdate");
            }

            if (StringUtils.isBlank(id)) {
                if (StringUtils.isBlank(UserContext.GetCurrentUserEmail())) {
                    return unauthorized("false");
                } else if (!UserContext.isCurrentUserAdministrator()) {
                    return unauthorized("false");
                } else {

                    ActivityCategory activityCategory = new ActivityCategory();
                    activityCategory.CnName = activityCategoryCN;
                    activityCategory.EnName = activityCategoryEN;
                    activityCategory.idx = activityCategoryIdx;

                    activityCategoryService.doCreate(activityCategory);
                    return ok(activityCategory.toJsonObject());
                }

            } else {
                if (StringUtils.isBlank(UserContext.GetCurrentUserEmail())) {
                    return unauthorized("false");
                } else if (!UserContext.isCurrentUserAdministrator()) {
                    return unauthorized("false");
                } else {

                    ActivityCategory activityCategory = activityCategoryService.findOneByDbid(Long.valueOf(id));

                    activityCategory.CnName = activityCategoryCN;
                    activityCategory.EnName = activityCategoryEN;
                    activityCategory.idx = activityCategoryIdx;

                    activityCategoryService.doUpdate(activityCategory);
                    return ok(activityCategory.toJsonObject());
                }
            }
        } catch (Exception e) {
            Logger.error(Messages.get("info.error.activityCategory.createOrUpdate"), e);
            return ok("info.error.activityCategory.createOrUpdate");
        }
    }

    @Transactional
    @With({ControllerHandler.class, Administrator.class})
    public Result deleteActivityCategory(String dbid) {
        try {
            if (StringUtils.isBlank(dbid)) {
                return ok(info.render("info.error.activityCategory.id.empty", Html.apply(Messages.get("info.error.activityCategory.id.empty")), "/activity_category_manager", "page.activityCategoryManager"));
            } else {
                ActivityCategory activityCategory = activityCategoryService.findOneByDbid(Long.valueOf(dbid));
                if (activityCategory != null) {

                    List<Activity> activities = activityService.findAllBrief();

                    if(activities.stream().filter(a -> a.category.equals(activityCategory)).count() > 0){
                        return ok(info.render("info.error.activityCategory.notDeletable", Html.apply(Messages.get("info.error.activityCategory.notDeletable")), "/activity_category_manager", "page.activityCategoryManager"));
                    }


                    StringBuilder shtmlBuilder = new StringBuilder();
                    shtmlBuilder.append(" <").append(activityCategory.CnName ).append(" ")
                            .append(activityCategory.EnName).append("> 删除成功");


                    activityCategoryService.doDelete(activityCategory);

                    return ok(info.render("info.success.activityCategory.delete", Html.apply(shtmlBuilder.toString()), "/activity_category_manager", "page.activityCategoryManager"));
                } else {
                    return ok(info.render("info.error.activityCategory.notFound", Html.apply(Messages.get("info.error.activityCategory.notFound")), "/activity_category_manager", "page.activityCategoryManager"));
                }
            }
        } catch (Exception e) {
            Logger.error(Messages.get("info.error.city.delete") + e.getLocalizedMessage());
            return ok(info.render("info.error.city.delete", Html.apply(Messages.get("info.error.city.delete")), "/activity_category_manager", "page.activityCategoryManager"));
        }
    }

    @Transactional
    @With({ControllerHandler.class, Administrator.class})
    public Result deleteReview(String dbid) {
        try {
            if (StringUtils.isBlank(dbid)) {
                return notFound();
            }
            else {
                Review review = reviewService.findOneByDbid(Long.valueOf(dbid));

                reviewService.doDelete(review);

                return ok(HttpUtils.buildHttpResult("sucess", "true"));
            }
        } catch (Exception e) {
            Logger.error("Delete review failed: ", e);
            return ok(info.render("info.error.review.delete", Html.apply(Messages.get("info.error.review.delete")), "/", "page.index"));
        }
    }

}
