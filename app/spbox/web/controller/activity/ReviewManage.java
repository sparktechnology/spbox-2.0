package spbox.web.controller.activity;


import play.Logger;
import play.db.jpa.Transactional;
import play.mvc.*;
import spbox.model.entity.activity.Activity;
import spbox.model.entity.activity.Review;
import spbox.model.entity.user.User;
import spbox.model.service.activity.ActivityService;
import spbox.model.service.activity.ReviewService;
import spbox.model.service.user.UserService;
import spbox.web.annotation.Authenticated;
import spbox.web.annotation.ControllerHandler;
import spbox.web.utilities.ComplexEventUtils;
import spbox.web.utilities.StringUtils;

import javax.inject.Inject;

public class ReviewManage extends Controller {

    @Inject
    private ReviewService reviewService;

    @Inject
    private ActivityService activityService;

    @Inject
    private UserService userService;

    @Inject
    private ComplexEventUtils complexEventUtils;

    @Transactional
    @With({ControllerHandler.class, Authenticated.class})
    @BodyParser.Of(BodyParser.Json.class)
    public Result createReview() {

        try {

            Http.RequestBody body = request().body();
            String reviewContent = body.asJson().findPath("reviewContent").textValue();
            String rating = body.asJson().findPath("rating").textValue();
            Long activityDbid = body.asJson().findPath("activityDbid").longValue();
            Long userDbid = body.asJson().findPath("userDbid").longValue();

            if (StringUtils.isBlank(rating)) {
                return badRequest("info.error.review.rating.empty");
            }

            Activity currentActivity = activityService.findOneByDbid(activityDbid);
            User currentUser = userService.findOneByDbid(userDbid);

            Review review = new Review();
            review.reviewContent = reviewContent;
            review.rating = Integer.valueOf(rating);
            review.activity = currentActivity;
            review.user = currentUser;

            reviewService.doCreate(review);

            complexEventUtils.sendOrganizerRatingEvent(review.activity.organizer, review.rating);

            return ok("{}");
        } catch (Exception e) {
            Logger.error("Creating new review failed: ", e);
            return ok("info.error.review.create");
        }
    }
}
