package spbox.web.controller.email;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import play.Logger;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import play.twirl.api.Html;
import spbox.UserContext;
import spbox.model.entity.email.Article;
import spbox.model.service.email.ArticleService;
import spbox.web.annotation.Administrator;
import spbox.web.annotation.ControllerHandler;
import spbox.web.utilities.StringUtils;
import spbox.web.view.email.html.article_manager;
import spbox.web.view.email.html.edit_article;
import spbox.web.view.html.info;

import javax.inject.Inject;
import java.util.List;

public class ArticleManage extends Controller {

    @Inject
    private ArticleService articleService;

    @Transactional
    @With({ControllerHandler.class, Administrator.class})
    public Result articleManagerUI() {
        try {
            List<Article> article_list = articleService.findAll();
            return ok(article_manager.render("page.articleManager.list", article_list));
        } catch (Exception e) {
            Logger.error(Messages.get("info.error.articleManager.list") + e.getLocalizedMessage());
            return ok(info.render("info.error.articleManager.list", Html.apply(Messages.get("info.error.articleManager.list")), null, null));
        }
    }

    @Transactional
    @With({ControllerHandler.class, Administrator.class})
    public Result editArticleUI(String bzid) {
        try {
            if (bzid.equalsIgnoreCase("new")) {
                return ok(edit_article.render("page.article.create", null));
            } else {

                Article article = articleService.findOneByBzid(bzid);

                if (article != null) {
                    return ok(edit_article.render("page.article.update", article));
                } else {
                    return ok(info.render("info.error.article.notFound", Html.apply(Messages.get("info.error.article.notFound")), "/article_manager", "page.articleManager.list"));
                }
            }
        } catch (Exception e) {
            Logger.error(Messages.get("info.error.article.createOrUpdate") + e.getLocalizedMessage());
            return ok(info.render("info.error.article.createOrUpdate", Html.apply(Messages.get("info.error.article.createOrUpdate")), "/article_manager", "page.articleManager.list"));
        }
    }

    @Transactional
    @BodyParser.Of(BodyParser.Json.class)
    @With({ControllerHandler.class, Administrator.class})
    public Result editArticle() {
        try {
            JsonNode body = request().body().asJson();
            String id = body.findPath("id") != null ? body.findPath("id").textValue() : StringUtils.BLANK;
            String name = body.findPath("name") != null ? body.findPath("name").textValue() : StringUtils.BLANK;
            String title = body.findPath("title") != null ? body.findPath("title").textValue() : StringUtils.BLANK;
            String content = body.findPath("content") != null ? body.findPath("content").textValue() : StringUtils.BLANK;

            if (StringUtils.isBlank(id)) {

                //check current user role
                if (StringUtils.isBlank(UserContext.GetCurrentUserEmail())) {
                    return unauthorized("false");
                } else if (!UserContext.isCurrentUserAdministrator()) {
                    return unauthorized("false");
                } else {

                    Article checkDuplicated = articleService.findOneByName(name.toUpperCase());
                    if (checkDuplicated != null) {
                        ObjectNode result = Json.newObject();
                        result.put("message", "duplicated");
                        return forbidden(result);
                    } else {
                        Article article = new Article();
                        article.articleType = Article.ArticleType.GENERAL;
                        article.name = name.toUpperCase();
                        article.title = title;
                        article.content = content;

                        articleService.doCreate(article);
                        return ok(article.toJsonObject());
                    }
                }
            } else {
                Article a = articleService.findOneByBzid(id);

                if (StringUtils.isBlank(UserContext.GetCurrentUserEmail())) {
                    return unauthorized("false");
                } else if (!UserContext.isCurrentUserAdministrator()) {
                    return unauthorized("false");
                } else {

                    if (a.articleType != Article.ArticleType.BUILTIN) {
                        a.name = name.toUpperCase();
                    }
                    a.title = title;
                    a.content = content;
                    articleService.doUpdate(a);

                    Logger.info("Update article {} successful.", name);
                    return ok(a.toJsonObject());
                }

            }
        } catch (Exception e) {
            return ok("info.error.article.createOrUpdate");
        }
    }

    @Transactional
    @With({ControllerHandler.class, Administrator.class})
    public Result deleteArticle(String bzid) {
        try {
            if (StringUtils.isBlank(bzid)) {
                return ok(info.render("info.error.article.id.empty", Html.apply(Messages.get("info.error.article.id.empty")), "/article_manager", "page.articleManager.list"));
            } else {
                Article article = articleService.findOneByBzid(bzid);
                if (article != null) {
                    StringBuilder shtmlBuilder = new StringBuilder();
                    shtmlBuilder.append("文章 <").append(article.name).append("(").append(article.title).append(")").append("> 删除成功");

                    articleService.doDelete(article);

                    return ok(info.render("info.success.article.delete", Html.apply(shtmlBuilder.toString()), "/article_manager", "page.articleManager.list"));
                } else {
                    return ok(info.render("info.error.article.notFound", Html.apply(Messages.get("info.error.article.notFound")), "/article_manager", "page.articleManager.list"));
                }
            }

        } catch (Exception e) {
            Logger.error(Messages.get("info.error.article.delete") + e.getLocalizedMessage());
            return ok(info.render("info.error.article.delete", Html.apply(Messages.get("info.error.article.delete")), "/article_manager", "page.articleManager.list"));
        }
    }
}
