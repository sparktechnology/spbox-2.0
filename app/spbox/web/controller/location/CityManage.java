package spbox.web.controller.location;

import play.Logger;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import play.mvc.*;
import play.twirl.api.Html;
import spbox.UserContext;
import spbox.model.entity.location.City;
import spbox.model.entity.location.Country;
import spbox.model.service.location.CityService;
import spbox.model.service.location.CountryService;
import spbox.web.annotation.Administrator;
import spbox.web.annotation.ControllerHandler;
import spbox.web.utilities.StringUtils;
import spbox.web.view.html.info;
import spbox.web.view.location.html.city_manager;
import spbox.web.view.location.html.edit_city;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;

public class CityManage extends Controller {

    @Inject
    private CityService cityService;

    @Inject
    private CountryService countryService;


    @Transactional
    @With({ControllerHandler.class, Administrator.class})
    public Result cityManagerUI() {
        try {
            List<Country> countries = countryService.findAll();

            return ok(city_manager.render("page.cityManager.list", countries));

        } catch (Exception e) {
            Logger.error(Messages.get("info.error.cityManager.list") + e.getLocalizedMessage());
            return ok(info.render("info.error.cityManager.list", Html.apply(Messages.get("info.error.cityManager.list")), "/system_manager", "page.systemManager"));
        }
    }

    @Transactional
    @With({ControllerHandler.class, Administrator.class})
    public Result editCityUI(String bzid) {
        try {
            if (bzid.equalsIgnoreCase("new")) {
                return ok(edit_city.render("page.city.create", null, TimeZone.getAvailableIDs()));
            } else {
                City city = cityService.findOneByBzid(bzid);
                if (city != null) {
                    return ok(edit_city.render("page.city.update", city, TimeZone.getAvailableIDs()));
                } else {
                    return ok(info.render("info.error.city.notFound", Html.apply(Messages.get("info.error.city.notFound")), "/city_manager", "page.cityManager"));
                }
            }
        } catch (Exception e) {
            Logger.error(Messages.get("info.error.city.createOrUpdate"), e);
            return ok(info.render("info.error.city.createOrUpdate", Html.apply(Messages.get("info.error.city.createOrUpdate")), "/city_manager", "page.cityManager"));
        }
    }

    @Transactional
    @With({ControllerHandler.class, Administrator.class})
    @BodyParser.Of(BodyParser.Json.class)
    public Result editCity() {
        try {
            Http.RequestBody body = request().body();
            String id = body.asJson().findPath("id").textValue();
            String countryCN = body.asJson().findPath("countryCN").textValue();
            String countryEN = body.asJson().findPath("countryEN").textValue();
            String cityCN = body.asJson().findPath("cityCN").textValue();
            String cityEN = body.asJson().findPath("cityEN").textValue();
            String timezone = body.asJson().findPath("timezone").textValue();

            if (StringUtils.isBlank(countryCN) || StringUtils.isBlank(countryEN) || StringUtils.isBlank(cityCN) || StringUtils.isBlank(cityEN) || StringUtils.isBlank(timezone)) {
                return badRequest("info.error.city.createOrUpdate");
            }


            if (StringUtils.isBlank(id)) {
                // check if you are admin
                if (StringUtils.isBlank(UserContext.GetCurrentUserEmail())) {
                    return unauthorized("false");
                } else if (!UserContext.isCurrentUserAdministrator()) {
                    return unauthorized("false");
                } else {

                    Country country = countryService.findOneByEnName(countryEN);

                    if (country == null) {
                        country = new Country();
                        country.CnName = countryCN;
                        country.EnName = countryEN.toUpperCase();
                    }

                    City city = new City();
                    city.bzid = UUID.randomUUID().toString();
                    city.CnName = cityCN;
                    city.EnName = cityEN.toUpperCase();
                    city.timeZone = timezone;
                    city.country = country;

                    if (country.cities == null) {
                        country.cities = new ArrayList<City>();
                    }
                    country.cities.add(city);

                    countryService.doCreate(country);
                    return ok(city.toJsonObject());
                }

            } else {
                City c = cityService.findOneByBzid(id);
                if (StringUtils.isBlank(UserContext.GetCurrentUserEmail())) {
                    return unauthorized("false");
                } else if (!UserContext.isCurrentUserAdministrator()) {
                    return unauthorized("false");
                } else {

                    Country country = countryService.findOneByEnName(countryEN);

                    if (country == null) {
                        country = countryService.findOneByCnName(countryCN);
                        if (country == null) {
                            country = new Country();
                        }
                    }

                    country.CnName = countryCN;
                    country.EnName = countryEN.toUpperCase();

                    if(country.cities == null){
                        country.cities = new ArrayList<City>();
                    }
                    if(!country.cities.contains(c)){
                        country.cities.add(c);
                    }

                    c.CnName = cityCN;
                    c.EnName = cityEN.toUpperCase();
                    c.country = country;
                    c.timeZone = timezone;

                    countryService.doUpdate(country);
                    return ok(c.toJsonObject());
                }
            }
        } catch (Exception e) {
            Logger.error(Messages.get("info.error.city.createOrUpdate"), e);
            return ok("info.error.city.createOrUpdate");
        }
    }

    @Transactional
    @With({ControllerHandler.class, Administrator.class})
    public Result deleteCity(String bzid) {
        try {
            if (StringUtils.isBlank(bzid)) {
                return ok(info.render("info.error.city.id.empty", Html.apply(Messages.get("info.error.city.id.empty")), "/city_manager", "page.cityManager"));
            } else {
                City city = cityService.findOneByBzid(bzid);
                if (city != null) {
                    StringBuilder shtmlBuilder = new StringBuilder();
                    shtmlBuilder.append("城市 <").append(city.CnName ).append(" ")
                            .append(city.EnName).append("(").append(city.country.EnName).append(")").append("> 删除成功");

                    Country country = city.country;
                    country.cities.remove(city);
                    city.country = null;

                    countryService.doUpdate(country);

                    return ok(info.render("info.success.city.delete", Html.apply(shtmlBuilder.toString()), "/city_manager", "page.cityManager"));
                } else {
                    return ok(info.render("info.error.city.notFound", Html.apply(Messages.get("info.error.city.notFound")), "/city_manager", "page.cityManager"));
                }
            }
        } catch (Exception e) {
            Logger.error(Messages.get("info.error.city.delete") + e.getLocalizedMessage());
            return ok(info.render("info.error.city.delete", Html.apply(Messages.get("info.error.city.delete")), "/city_manager", "page.cityManager"));
        }
    }
}
