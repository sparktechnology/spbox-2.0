package spbox;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class PageViewCount {

    private static Map<String, Long> pageViewCount;

    public static void updatePageViewCount(String pageUrl, Long count){

        if(pageViewCount == null){
            pageViewCount = new ConcurrentHashMap<String, Long>();
        }

        pageViewCount.put(pageUrl, count);
    }

    public static Map<String, Long> getPageViewCount(){
        return pageViewCount;
    }
}
