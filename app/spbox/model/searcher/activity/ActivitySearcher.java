package spbox.model.searcher.activity;


import org.hibernate.search.query.dsl.BooleanJunction;
import org.hibernate.search.query.dsl.QueryBuilder;
import spbox.model.entity.activity.Activity;
import spbox.model.searcher.HibernateSearch;

import java.util.List;

public class ActivitySearcher extends HibernateSearch{

    public List<Activity> searchActivity(String keyword){
        QueryBuilder qb = this.getQueryBuilder(Activity.class);

        BooleanJunction<BooleanJunction> bool = qb.bool();
        BooleanJunction<BooleanJunction> boolStatus = qb.bool();

        bool.must(qb.keyword().onFields("name", "briefDescription", "description", "venue.city.CnName", "venue.city.EnName")
                .matching(keyword)
                .createQuery());

        boolStatus.should(qb.keyword().onFields("status")
                .matching(Activity.ActivityStatusEnum.EXPIRED)
                .createQuery()).should(qb.keyword().onFields("status")
                .matching(Activity.ActivityStatusEnum.PUBLISH)
                .createQuery());

        bool.must(boolStatus.createQuery());

        org.apache.lucene.search.Query luceneQuery = bool.createQuery();


        javax.persistence.Query jpaQuery = this.createJPAQuery(luceneQuery, Activity.class);

        return (List<Activity>) jpaQuery.getResultList();
    }
}
