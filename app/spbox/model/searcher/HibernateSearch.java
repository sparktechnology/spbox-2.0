package spbox.model.searcher;


import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.query.dsl.QueryBuilder;
import play.db.jpa.JPA;

public class HibernateSearch {

    private FullTextEntityManager fullTextEntityManager;

    public FullTextEntityManager getFullTextEntityManager(){
        if(fullTextEntityManager == null){
          fullTextEntityManager = org.hibernate.search.jpa.Search.getFullTextEntityManager(JPA.em());
        }
       return fullTextEntityManager;
    }

    public QueryBuilder getQueryBuilder(Class<?> entity){
        return getFullTextEntityManager().getSearchFactory()
                .buildQueryBuilder().forEntity(entity).get();
    }

    public javax.persistence.Query createJPAQuery(org.apache.lucene.search.Query luceneQuery, Class<?> entity){
        return getFullTextEntityManager().createFullTextQuery(luceneQuery, entity);
    }
}


