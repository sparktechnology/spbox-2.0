package spbox.model.searcher.user;

import org.hibernate.search.query.dsl.QueryBuilder;
import spbox.model.entity.user.User;
import spbox.model.searcher.HibernateSearch;

public class UserSearcher extends HibernateSearch{

    public User searchUser(String keyword){
        QueryBuilder qb = this.getQueryBuilder(User.class);

        keyword = keyword.replace("@" , "%40");
        org.apache.lucene.search.Query luceneQuery = qb.keyword().onFields("name", "email")
                .matching(keyword)
                .createQuery();

        javax.persistence.Query jpaQuery = this.createJPAQuery(luceneQuery, User.class);

        return (User) jpaQuery.getSingleResult();
    }
}
