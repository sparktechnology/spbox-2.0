package spbox.model.service.user;

import play.db.jpa.JPA;
import spbox.UserContext;
import spbox.model.entity.user.User;
import spbox.web.utilities.TimeUtils;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;

public class UserService {

    public List<User> findAll() {
        String sql = "select u from User u";

        return (List<User>) JPA.em().createQuery(sql).getResultList();
    }

    public List<User> findByRole(User.UserRoleEnum role) {
        String sql = "select u from User u where u.role = :role";

        Query query = JPA.em().createQuery(sql);
        query.setParameter("role", role);

        return (List<User>) query.getResultList();
    }

    public User authenticate(String email, String password) {
        try {
            String sql = "select distinct u from User u where u.email = :email and u.password = :password";

            Query query = JPA.em().createQuery(sql);
            query.setParameter("email", email);
            query.setParameter("password", password);

            return (User) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public User findOneByEmail(String email) {
        try {
            String sql = "select distinct u from User u where u.email = :email";

            return (User) JPA.em().createQuery(sql).setParameter("email", email).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public User findOneByDbid(Long dbid) {
        try {
            String sql = "select distinct u from User u where u.dbid = :dbid";

            return (User) JPA.em().createQuery(sql).setParameter("dbid", dbid).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public void doCreate(User user) {
        user.createTime = TimeUtils.getUTCDate(new Date());
        JPA.em().persist(user);
    }

    public void doUpdate(User user) {
        JPA.em().merge(user);
    }

    public void doDelete(User user) {
        JPA.em().remove(user);
    }
}
