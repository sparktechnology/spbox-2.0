package spbox.model.service.user;

import play.db.jpa.JPA;
import spbox.model.entity.user.UserPaymentRequirement;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;


public class UserPaymentRequirementService {

    public List<UserPaymentRequirement> findAll() {

        String sql = "select upr from UserPaymentRequirement upr";
        return (List<UserPaymentRequirement>) JPA.em().createQuery(sql).getResultList();
    }

    public UserPaymentRequirement findOneByDbid(Long dbid){
        try {
            String sql = "select distinct upr from UserPaymentRequirement upr where upr.dbid = :dbid";
            Query query = JPA.em().createQuery(sql);
            query.setParameter("dbid", dbid);

            return (UserPaymentRequirement) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public UserPaymentRequirement findOneByUserEmail(String email){
        try {
            String sql = "select distinct upr from UserPaymentRequirement upr where upr.user.email = :email";
            Query query = JPA.em().createQuery(sql);
            query.setParameter("email", email);

            return (UserPaymentRequirement) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public void doCreate(UserPaymentRequirement userPaymentRequirement) {
        JPA.em().persist(userPaymentRequirement);
    }

    public void doUpdate(UserPaymentRequirement userPaymentRequirement) {
        JPA.em().merge(userPaymentRequirement);
    }

    public void doDelete(UserPaymentRequirement userPaymentRequirement) {
        JPA.em().remove(userPaymentRequirement);
    }
}
