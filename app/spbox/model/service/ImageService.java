package spbox.model.service;

import play.db.jpa.JPA;
import spbox.model.entity.Image;

import javax.persistence.NoResultException;
import javax.persistence.Query;


public class ImageService {

    public Image findOneByBzid(String bzid){
        try {
            String sql = "select distinct i from Image i where i.bzid = :bzid";
            Query query = JPA.em().createQuery(sql);
            query.setParameter("bzid", bzid);

            return (Image) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public void doCreate(Image image) {
        JPA.em().persist(image);
    }

    public void doUpdate(Image image) {
        JPA.em().merge(image);
    }
}
