package spbox.model.service.ticket;

import play.db.jpa.JPA;
import spbox.model.entity.ticket.TicketType;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;
import java.util.UUID;

public class TicketTypeService {

    public List<TicketType> findAll() {

        String sql = "select distinct tt from TicketType tt";

        Query query = JPA.em().createQuery(sql);

        return (List<TicketType>) query.getResultList();

    }

    public List<TicketType> findAllByActivityDbid(Long activityDbid) {

        String sql = "select distinct tt from TicketType tt where tt.activity.dbid = :activityDbid";

        Query query = JPA.em().createQuery(sql);
        query.setParameter("activityDbid", activityDbid);

        return (List<TicketType>) query.getResultList();

    }

    public List<TicketType> findAllByActivityBzid(String activityBzid) {

        String sql = "select distinct tt from TicketType tt where tt.activity.bzid = :activityBzid";

        Query query = JPA.em().createQuery(sql);
        query.setParameter("activityBzid", activityBzid);

        return (List<TicketType>) query.getResultList();

    }

    public TicketType findOneByNameAndActivityBzid(String name, String activityBzid) {
        try {
            String sql = "select distinct tt from TicketType tt where tt.name = :name and tt.activity.bzid = :activityBzid";

            Query query = JPA.em().createQuery(sql);
            query.setParameter("name", name);
            query.setParameter("activityBzid", activityBzid);

            return (TicketType) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public TicketType findOneByBzid(String bzid) {
        try {
            String sql = "select distinct tt from TicketType tt where tt.bzid = :bzid";


            return (TicketType) JPA.em().createQuery(sql).setParameter("bzid", bzid).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }

    }

    public Double findCheapestUniqueTicketPriceByActivityBzid(String bzid){
        try {
            String sql = "SELECT tt.price FROM TicketType tt WHERE tt.activity.bzid= :bzid ORDER BY tt.price ASC";


            Object queryResult = JPA.em().createQuery(sql).setParameter("bzid", bzid).setMaxResults(1).getSingleResult();

            return (Double) queryResult;
        } catch (NoResultException e) {
            return null;
        }
    }

    public Double findMostExpensiveUniqueTicketPriceByActivityBzid(String bzid){
        try {
            String sql = "SELECT tt.price FROM TicketType tt WHERE tt.activity.bzid= :bzid ORDER BY tt.price DESC";

            Object queryResult = JPA.em().createQuery(sql).setParameter("bzid", bzid).setMaxResults(1).getSingleResult();

            return (Double) queryResult;
        } catch (NoResultException e) {
            return null;
        }
    }



    public void doCreate(TicketType ticketType) {
        ticketType.bzid = UUID.randomUUID().toString();
        JPA.em().persist(ticketType);
    }

    public void doUpdate(TicketType ticketType) {
        JPA.em().merge(ticketType);
    }

    public void doDelete(TicketType ticketType) {
        JPA.em().remove(ticketType);
    }
}
