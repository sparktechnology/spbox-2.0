package spbox.model.service.ticket;

import play.db.jpa.JPA;
import spbox.model.entity.ticket.TicketTypeManual;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;


public class TicketTypeManualService {

    public List<TicketTypeManual> findAll() {

        String sql = "select distinct ttm from TicketTypeManual ttm";

        Query query = JPA.em().createQuery(sql);

        return (List<TicketTypeManual>) query.getResultList();

    }

    public TicketTypeManual findOneByDbid(Long dbid) {
        try {
            String sql = "select distinct ttm from TicketTypeManual ttm where ttm.dbid = :dbid";

            Query query = JPA.em().createQuery(sql);
            query.setParameter("dbid", dbid);

            return (TicketTypeManual) query.getSingleResult();
        }catch(NoResultException e){
            return null;
        }
    }

    public TicketTypeManual findOneByTicketTypeDbid(Long dbid) {
        try {
            String sql = "select distinct ttm from TicketTypeManual ttm where ttm.ticketType.dbid = :dbid";

            Query query = JPA.em().createQuery(sql);
            query.setParameter("dbid", dbid);

            return (TicketTypeManual) query.getSingleResult();
        }catch(NoResultException e){
            return null;
        }
    }

    public void doCreate(TicketTypeManual ticketTypeManual) {
        JPA.em().persist(ticketTypeManual);
    }

    public void doUpdate(TicketTypeManual ticketTypeManual) {
        JPA.em().merge(ticketTypeManual);
    }

    public void doDelete(TicketTypeManual ticketTypeManual) {
        JPA.em().remove(ticketTypeManual);
    }
}
