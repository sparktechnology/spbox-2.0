package spbox.model.service.ticket;

import play.db.jpa.JPA;
import spbox.model.entity.ticket.Ticket;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.*;

public class TicketService {

    public List<Ticket> findAll() {
        String sql = "select t from Ticket t";

        Query query = JPA.em().createQuery(sql);

        return (List<Ticket>) query.getResultList();
    }

    public List<Ticket> findAllByActivityBzid(String activityBzid) {

        String sql = "select distinct t from Ticket t where t.activity.bzid = :activityBzid";

        Query query = JPA.em().createQuery(sql);
        query.setParameter("activityBzid", activityBzid);

        return (List<Ticket>) query.getResultList();
    }

    public Long findCountByTicketTypeDbid(Long ticketTypeDbid) {

        String sql = "select count(distinct t) from Ticket t where t.ticketType.dbid = :ticketTypeDbid";

        Query query = JPA.em().createQuery(sql);
        query.setParameter("ticketTypeDbid", ticketTypeDbid);

        return (Long) query.getSingleResult();
    }

    public Long findCountByTicketTypeBzid(String ticketTypeBzid) {

        String sql = "select count(distinct t) from Ticket t where t.ticketType.bzid = :ticketTypeBzid";

        Query query = JPA.em().createQuery(sql);
        query.setParameter("ticketTypeBzid", ticketTypeBzid);

        return (Long) query.getSingleResult();
    }

    public Long findPaidUniqueUserByActivityDbid(Long activityDbid) {

        String sql = "select count(distinct t.user) from Ticket t where t.paid = :paid and t.activity.dbid = :activityDbid";

        Query query = JPA.em().createQuery(sql);
        query.setParameter("paid", true);
        query.setParameter("activityDbid", activityDbid);

        return (Long) query.getSingleResult();
    }

    public List<Ticket> findAllPaidByUserEmail(String userEmail) {

        String sql = "select distinct t from Ticket t where t.paid = :paid and t.user.email = :userEmail order by t.activity.startTime asc";

        Query query = JPA.em().createQuery(sql);
        query.setParameter("paid", true);
        query.setParameter("userEmail", userEmail);

        return (List<Ticket>) query.getResultList();
    }

    public Ticket findOneByBzid(String bzid) {
        try {
            String sql = "select distinct t from Ticket t where t.bzid = :bzid";

            Query query = JPA.em().createQuery(sql);
            query.setParameter("bzid", bzid);

            return (Ticket) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public Ticket findOneByBzidAndOrganizerEmail(String bzid, String email) {
        try {
            String sql = "select distinct t from Ticket t where t.bzid = :bzid and t.activity.organizer = :email";

            Query query = JPA.em().createQuery(sql);
            query.setParameter("bzid", bzid);
            query.setParameter("email", email);

            return (Ticket) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public void doCreate(Ticket ticket) {
        ticket.bzid = UUID.randomUUID().toString();
        JPA.em().persist(ticket);
    }

    public void doUpdate(Ticket ticket) {
        JPA.em().merge(ticket);
    }

    public void doDelete(Ticket ticket) {
        ticket.ticketType.bookedQuantity = ticket.ticketType.bookedQuantity - 1;
        JPA.em().remove(ticket);
    }
}
