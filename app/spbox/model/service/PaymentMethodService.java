package spbox.model.service;

import play.db.jpa.JPA;
import spbox.model.entity.PaymentMethod;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;


public class PaymentMethodService {

    public List<PaymentMethod> findAll() {

        String sql = "select pm from PaymentMethod pm";
        return (List<PaymentMethod>) JPA.em().createQuery(sql).getResultList();
    }

    public PaymentMethod findOneByDbid(Long dbid){
        try {
            String sql = "select distinct pm from PaymentMethod pm where pm.dbid = :dbid";
            Query query = JPA.em().createQuery(sql);
            query.setParameter("dbid", dbid);

            return (PaymentMethod) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public void doCreate(PaymentMethod paymentMethod) {
        JPA.em().persist(paymentMethod);
    }

    public void doUpdate(PaymentMethod paymentMethod) {
        JPA.em().merge(paymentMethod);
    }

    public void doDelete(PaymentMethod paymentMethod) {
        JPA.em().remove(paymentMethod);
    }
}
