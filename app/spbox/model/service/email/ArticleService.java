package spbox.model.service.email;

import play.db.jpa.JPA;
import spbox.model.entity.email.Article;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;
import java.util.UUID;

public class ArticleService {

    public List<Article> findAll() {

        String sql = "select a from Article a";
        return (List<Article>) JPA.em().createQuery(sql).getResultList();

    }

    public Article findOneByBzid(String bzid) {

        try {
            String sql = "select distinct a from Article a where a.bzid = :bzid";

            Query query = JPA.em().createQuery(sql);
            query.setParameter("bzid", bzid);

            return (Article) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public Article findOneByName(String name) {
        try {
            String sql = "select distinct a from Article a where a.name = :name";

            Query query = JPA.em().createQuery(sql);
            query.setParameter("name", name);

            return (Article) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public Article findOneByNameAndType(String name, Article.ArticleType articleType) {
        try {
            String sql = "select distinct a from Article a where a.name = :name and a.articleType = :articleType";

            Query query = JPA.em().createQuery(sql);
            query.setParameter("name", name);
            query.setParameter("articleType", articleType);

            return (Article) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public void doCreate(Article article) {
        article.bzid = UUID.randomUUID().toString();
        JPA.em().persist(article);
    }

    public void doUpdate(Article article) {
        JPA.em().merge(article);
    }

    public void doDelete(Article article) {
        JPA.em().remove(article);
    }
}
