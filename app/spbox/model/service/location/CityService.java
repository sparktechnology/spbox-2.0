package spbox.model.service.location;

import play.db.jpa.JPA;
import spbox.model.entity.location.City;

import javax.persistence.NoResultException;
import javax.persistence.Query;

public class CityService {

    public City findOneByBzid(String bzid) {
        try {

            String sql = "select distinct c from City c where c.bzid = :bzid";


            return (City) JPA.em().createQuery(sql).setParameter("bzid", bzid).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
