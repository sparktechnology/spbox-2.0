package spbox.model.service.location;

import play.db.jpa.JPA;
import spbox.model.entity.location.Country;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;
import java.util.UUID;

public class CountryService {

    public List<Country> findAll(){

        String sql = "select c from Country c";
        return (List<Country>) JPA.em().createQuery(sql).getResultList();
    }

    public Country findOneByEnName(String enName){
        try {
            String sql = "select c from Country c where c.EnName = :enName";
            Query query = JPA.em().createQuery(sql);
            query.setParameter("enName", enName);

            return (Country) query.getSingleResult();
        }catch(NoResultException e ){
            return null;
        }
    }

    public Country findOneByCnName(String cnName){
        try {
            String sql = "select c from Country c where c.CnName = :cnName";
            Query query = JPA.em().createQuery(sql);
            query.setParameter("cnName", cnName);

            return (Country) query.getSingleResult();
        }catch(NoResultException e ){
            return null;
        }
    }

    public void doCreate(Country country) {
        country.bzid = UUID.randomUUID().toString();
        JPA.em().persist(country);
    }

    public void doUpdate(Country country) {
        JPA.em().merge(country);
    }

    public void doDelete(Country country) {
        JPA.em().remove(country);
    }
}
