package spbox.model.service.activity;

import play.db.jpa.JPA;
import spbox.model.entity.activity.ActivityImage;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;

public class ActivityImageService {

    public List<ActivityImage> findAllByActivityDbid(Long dbid) {
        String sql = "select ai from ActivityImage ai where ai.activity.dbid = :dbid";
        Query query = JPA.em().createQuery(sql);
        query.setParameter("dbid", dbid);

        return (List<ActivityImage>) query.getResultList();
    }

    public ActivityImage findIndexImageByActivityDbid(Long dbid) {
        try {
            String sql = "select distinct ai from ActivityImage ai where ai.activity.dbid = :dbid and ai.category = 'INDEX'";
            Query query = JPA.em().createQuery(sql);
            query.setParameter("dbid", dbid);

            return (ActivityImage) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<ActivityImage> findDescriptionImagesByActivityDbid(Long dbid) {
        String sql = "select distinct ai from ActivityImage ai where ai.activity.dbid = :dbid and ai.category = 'DESCRIPTION'";
        Query query = JPA.em().createQuery(sql);
        query.setParameter("dbid", dbid);

        return (List<ActivityImage>) query.getResultList();
    }

    public ActivityImage findOneByDbid(Long dbid) {
        try {
            String sql = "select distinct ai from ActivityImage ai where ai.dbid = :dbid";
            Query query = JPA.em().createQuery(sql);
            query.setParameter("dbid", dbid);

            return (ActivityImage) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public void doCreate(ActivityImage activityImage) {
        JPA.em().persist(activityImage);
    }

    public void doUpdate(ActivityImage activityImage) {
        JPA.em().merge(activityImage);
    }

    public void doDelete(ActivityImage activityImage) {
        JPA.em().remove(activityImage);
    }

    public void deleteImageById(Long dbid){
        String sql = "DELETE FROM ActivityImage WHERE dbid = :dbid";
        Query query = JPA.em().createQuery(sql);
        query.setParameter("dbid", dbid);
        query.executeUpdate();
    }

}
