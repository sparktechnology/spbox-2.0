package spbox.model.service.activity;

import play.db.jpa.JPA;
import spbox.model.entity.activity.*;
import spbox.web.utilities.TimeUtils;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.*;


public class ActivityService {

    public List<Activity> findAllBrief() {

        String sql = "select a.dbid, a.bzid, a.status, a.briefDescription, a.category, a.dateTime, a.name, a.organizer, a.venue, a.indexPreviewImage from Activity a order by a.category.idx, a.createTime DESC";
        List<Object[]> objectList = JPA.em().createQuery(sql).getResultList();

        List<Activity> activities = new ArrayList<Activity>();

        for(Object[] obj : objectList){
            Activity activity = new Activity();
            activity.dbid = (Long) obj[0];
            activity.bzid = (String) obj[1];
            activity.status = (Activity.ActivityStatusEnum) obj[2];
            activity.briefDescription = (String) obj[3];
            activity.category = (ActivityCategory) obj[4];
            activity.dateTime = (String) obj[5];
            activity.name = (String) obj[6];
            activity.organizer = (String) obj[7];
            activity.venue = (ActivityVenue) obj[8];
            activity.indexPreviewImage = (ActivityIndexPreviewImage) obj[9];

            activities.add(activity);
        }

        return activities;
    }

    public List<Activity> findAll() {

        String sql = "select a from Activity a group by a.category.idx order by a.category.idx, a.createTime DESC";
        return (List<Activity>) JPA.em().createQuery(sql).getResultList();
    }

    public List<Activity> findAllRecommended(Activity activity, Activity.ActivityStatusEnum... status){
        String sql = "select distinct a from Activity a where a.category.dbid = :categoryDbid and a.dbid <> :activityDbid and (";

        for (Activity.ActivityStatusEnum s : status) {
            sql = sql + "a.status = :status" + Arrays.asList(status).indexOf(s);
            if (Arrays.asList(status).indexOf(s) != Arrays.asList(status).size() - 1)
                sql = sql + " or ";
            else
                sql = sql + ")";
        }

        Query query = JPA.em().createQuery(sql);
        query.setParameter("categoryDbid", activity.category.dbid);
        query.setParameter("activityDbid", activity.dbid);

        for (Activity.ActivityStatusEnum s : status) {
            query.setParameter("status"+Arrays.asList(status).indexOf(s), s);
        }

        return (List<Activity>) query.getResultList();
    }

    public List<Activity> findAllBriefByCategoryAndStatusAndSort(Long categoryId, String priceType, Date sortStartDate, Date sortEndDate, String sort, Activity.ActivityStatusEnum... status) {

        String sql = "select distinct a.dbid, a.bzid, a.status, a.briefDescription, a.category, a.dateTime, a.name, a.venue, a.priceLeast, a.priceMost, a.indexPreviewImage from Activity a where ";


        // Category filter
        if (categoryId.equals(Long.valueOf(0))){

        } else {
            sql += "a.category.dbid = '" + categoryId + "' and " ;
        }

        // Status filter
        sql += "( ";
        for (Activity.ActivityStatusEnum s : status) {
            sql += "a.status = '" + s + "'";
            if (Arrays.asList(status).indexOf(s) != Arrays.asList(status).size() - 1)
                sql += " or ";
        }
        sql += " ) ";

        // Price filter
        if (priceType.equals("free")){
            sql += "and a.priceMost = '0' ";
        }else if(priceType.equals("paid")){
            sql += "and a.priceMost != '0' ";
        }

        if(sortEndDate != null){
            sql += "and a.startTime <= :sortEndDate and a.endTime >= :sortStartDate ";
        }

        sql += "order by a.status DESC";

        // Sort order
        if (sort.equals("default") || sort.equals("newest")){
            sql += ", a.createTime DESC";
        } else if (sort.equals("startingSoon")){
            sql += ", a.startTime ASC";
        }

        List<Object[]> objectList;
        if(sortEndDate != null) {
             objectList = JPA.em().createQuery(sql).setParameter("sortEndDate", sortEndDate).setParameter("sortStartDate", sortStartDate).getResultList();
        }else{
             objectList = JPA.em().createQuery(sql).getResultList();
        }

        List<Activity> activities = new ArrayList<Activity>();

        for(Object[] obj : objectList){
            Activity activity = new Activity();
            activity.dbid = (Long) obj[0];
            activity.bzid = (String) obj[1];
            activity.status = (Activity.ActivityStatusEnum) obj[2];
            activity.briefDescription = (String) obj[3];
            activity.category = (ActivityCategory) obj[4];
            activity.dateTime = (String) obj[5];
            activity.name = (String) obj[6];
            activity.venue = (ActivityVenue) obj[7];
            activity.priceLeast = (Double) obj[8];
            activity.priceMost = (Double) obj[9];
            activity.indexPreviewImage = (ActivityIndexPreviewImage) obj[10];

            activities.add(activity);
        }

        return activities;
    }

    public List<Activity> findAllBriefByCategoryAndStatusAndSortWithPagination(Long categoryId, String priceType, Date sortStartDate, Date sortEndDate, String sort, String maxResult, String page, Activity.ActivityStatusEnum... status) {

        String sql = "select distinct a.dbid, a.bzid, a.status, a.briefDescription, a.category, a.dateTime, a.name, a.venue, a.priceLeast, a.priceMost, a.indexPreviewImage from Activity a where ";

        // Category filter
        if (categoryId.equals(Long.valueOf(0))){

        } else {
            sql += "a.category.dbid = '" + categoryId + "' and " ;
        }

        // Status filter
        sql += "( ";
        for (Activity.ActivityStatusEnum s : status) {
            sql += "a.status = '" + s + "'";
            if (Arrays.asList(status).indexOf(s) != Arrays.asList(status).size() - 1)
                sql += " or ";
        }
        sql += " ) ";

        // Price filter
        if (priceType.equals("free")){
            sql += "and a.priceMost = '0' ";
        }else if(priceType.equals("paid")){
            sql += "and a.priceMost != '0' ";
        }

        if(sortEndDate != null){
            sql += "and a.startTime <= :sortEndDate and a.startTime >= :sortStartDate ";
        }

        // Sort order
        if (sort.equals("default") || sort.equals("newest")){
            sql += "order by a.createTime DESC";
        } else if (sort.equals("startingSoon")){
            sql += "order by a.startTime ASC";
        }

        List<Object[]> objectList;
        Query query = JPA.em().createQuery(sql);
        if(sortEndDate != null) {
            query.setParameter("sortEndDate", sortEndDate).setParameter("sortStartDate", sortStartDate);
        }

        Integer maxResultInt = 0;

        if(maxResult != null && Integer.valueOf(maxResult) != null){
            maxResultInt = Integer.valueOf(maxResult);
            query.setMaxResults(maxResultInt);
        }

        if(page != null && Integer.valueOf(page) != null){
            query.setFirstResult(Integer.valueOf(page) * maxResultInt);
        }

        objectList = query.getResultList();

        List<Activity> activities = new ArrayList<Activity>();

        for(Object[] obj : objectList){
            Activity activity = new Activity();
            activity.dbid = (Long) obj[0];
            activity.bzid = (String) obj[1];
            activity.status = (Activity.ActivityStatusEnum) obj[2];
            activity.briefDescription = (String) obj[3];
            activity.category = (ActivityCategory) obj[4];
            activity.dateTime = (String) obj[5];
            activity.name = (String) obj[6];
            activity.venue = (ActivityVenue) obj[7];
            activity.priceLeast = (Double) obj[8];
            activity.priceMost = (Double) obj[9];
            activity.indexPreviewImage = (ActivityIndexPreviewImage) obj[10];

            activities.add(activity);
        }

        return activities;
    }

    public List<Activity> findAllByOrganizerEmail(String email) {

        String sql = "select distinct a from Activity a where a.organizer = :email";
        Query query = JPA.em().createQuery(sql);
        query.setParameter("email", email);

        return (List<Activity>) query.getResultList();

    }

    public List<Activity> findAllByOrganizerEmailAndStatus(String email, Activity.ActivityStatusEnum status) {

        String sql = "select distinct a from Activity a where a.organizer = :email and a.status = :status";
        Query query = JPA.em().createQuery(sql);
        query.setParameter("email", email);
        query.setParameter("status", status);

        return (List<Activity>) query.getResultList();

    }

    public Activity findOneByBzid(String bzid) {
        try {
            String sql = "select distinct a from Activity a where a.bzid = :bzid";
            Query query = JPA.em().createQuery(sql);
            query.setParameter("bzid", bzid);

            return (Activity) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public Long findCountByStatus(Activity.ActivityStatusEnum... status) {
        try {
            String sql = "select count(distinct a) from Activity a where ";

            for (Activity.ActivityStatusEnum s : status) {
                sql = sql + "a.status = '" + s + "'";
                if (Arrays.asList(status).indexOf(s) != Arrays.asList(status).size() - 1)
                    sql = sql + " or ";
            }
            return (Long) JPA.em().createQuery(sql).getSingleResult();
        }catch (NoResultException e){
            return new Long(0);
        }
    }

    public Activity findOneByDbid(Long dbid) {
        try {
            String sql = "select distinct a from Activity a where a.dbid = :dbid";
            Query query = JPA.em().createQuery(sql);
            query.setParameter("dbid", dbid);

            return (Activity) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<Activity> findAllActivityByUserEmail(String email) {

        String sql = "select distinct t.activity from Ticket t where t.user.email = :email";

        Query query = JPA.em().createQuery(sql);
        query.setParameter("email", email);

        return (List<Activity>) query.getResultList();
    }

    public void doCreate(Activity activity) {
        activity.bzid = UUID.randomUUID().toString();
        activity.createTime = TimeUtils.getUTCDate(new Date());
        JPA.em().persist(activity);
    }

    public void doUpdate(Activity activity) {
        JPA.em().merge(activity);
    }

    public void doDelete(Activity activity) {
        JPA.em().remove(activity);
    }
}
