package spbox.model.service.activity;

import play.db.jpa.JPA;
import spbox.model.entity.activity.ActivityCategory;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;

public class ActivityCategoryService {

    public List<ActivityCategory> findAll() {
        String sql = "select ac from ActivityCategory ac order by ac.idx";
        return (List<ActivityCategory>) JPA.em().createQuery(sql).getResultList();
    }

    public ActivityCategory findOneByDbid(Long dbid) {
        try {
            String sql = "select distinct ac from ActivityCategory ac where ac.dbid = :dbid";
            Query query = JPA.em().createQuery(sql);
            query.setParameter("dbid", dbid);

            return (ActivityCategory) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public Long findOneDbidByEnName(String EnName) {
        try {
            String sql = "select distinct ac.dbid from ActivityCategory ac where ac.EnName = :EnName";
            Query query = JPA.em().createQuery(sql);
            query.setParameter("EnName", EnName);

            return (Long) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public void doCreate(ActivityCategory activityCategory) {
        JPA.em().persist(activityCategory);
    }

    public void doUpdate(ActivityCategory activityCategory) {
        JPA.em().merge(activityCategory);
    }

    public void doDelete(ActivityCategory activityCategory) {
        JPA.em().remove(activityCategory);
    }

}
