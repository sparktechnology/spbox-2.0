package spbox.model.service.activity;

import play.db.jpa.JPA;
import spbox.model.entity.activity.Review;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;

public class ReviewService {

    public Review findOneByDbid(Long dbid){
        try {
            String sql = "SELECT DISTINCT r FROM Review r WHERE r.dbid = :dbid";
            return (Review) JPA.em().createQuery(sql).setParameter("dbid", dbid).getSingleResult();
        }catch (NoResultException e) {
            return null;
        }
    }

    public List<Review> findAll(){
        String sql = "SELECT r FROM Review r ";
        return (List<Review>) JPA.em().createQuery(sql).getResultList();
    }

    public List<Review> findAllByActivityDbid(Long activtyDbid){

        String sql = "SELECT DISTINCT r FROM Review r WHERE r.activity.dbid = :dbid ORDER BY r.creationTime DESC";

        Query query = JPA.em().createQuery(sql).setParameter("dbid", activtyDbid);

        return (List<Review>) query.getResultList();
    }

    public List<Review> findAllByActivityBzid(String activtyBzid, String maxResult, String page){

        String sql= "SELECT DISTINCT r FROM Review r WHERE r.activity.bzid = :bzid ORDER BY r.creationTime DESC";

        Query query = JPA.em().createQuery(sql).setParameter("bzid", activtyBzid);

        Integer maxResultInt = 0;

        if(maxResult != null && Integer.valueOf(maxResult) != null){
            maxResultInt = Integer.valueOf(maxResult);
            query.setMaxResults(maxResultInt);
        }

        if(page != null && Integer.valueOf(page) != null){
            query.setFirstResult(Integer.valueOf(page) * maxResultInt);
        }

        return (List<Review>) query.getResultList();
    }

    public List<Review> findAllByUserEmail(String email){

        String sql= "SELECT DISTINCT r FROM Review r WHERE r.user.email = :email";

        Query query = JPA.em().createQuery(sql).setParameter("email", email);

        return (List<Review>) query.getResultList();
    }

    public Long findCountByActivityBzid(String activtyBzid) {
        try {
            String sql = "select count(distinct r) from Review r where r.activity.bzid = :bzid";

            Query query = JPA.em().createQuery(sql).setParameter("bzid", activtyBzid);

            return (Long) query.getSingleResult();

        }catch (NoResultException e){
            return new Long(0);
        }
    }

    public void doCreate(Review review) { JPA.em().persist(review); }

    public void doUpdate(Review review) {
        JPA.em().merge(review);
    }

    public void doDelete(Review review) {
        JPA.em().remove(review);
    }

}
