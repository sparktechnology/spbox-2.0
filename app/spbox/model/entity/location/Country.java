package spbox.model.entity.location;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="country",uniqueConstraints = {
        @UniqueConstraint(columnNames = "CnName"), @UniqueConstraint(columnNames = "EnName")})
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long dbid;

    @Version
    public Long objectVersion;

    public String bzid;

    public String CnName;

    public String EnName;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "country")
    public List<City> cities;
}
