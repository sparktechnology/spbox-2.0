package spbox.model.entity.location;

import com.fasterxml.jackson.databind.node.ObjectNode;
import org.hibernate.search.annotations.Field;
import play.libs.Json;

import javax.persistence.*;
import java.util.TimeZone;

@Entity
@Table(name="city")
public class City {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long dbid;

    @Version
    public Long objectVersion;

    public String bzid;

    @Field
    public String CnName;

    @Field
    public String EnName;

    @ManyToOne(optional = false)
    @JoinColumn(name="country_dbid")
    public Country country;

    public String timeZone;

    public ObjectNode toJsonObject()
    {
        ObjectNode result = Json.newObject();
        result.put("bzid", this.bzid);
        result.put("CnName", this.CnName);
        result.put("EnName", this.EnName);
        result.put("timeZone", this.timeZone);
        return result;
    }

    public TimeZone GetTimeZoneObject(){
        return TimeZone.getTimeZone(this.timeZone);
    }
}
