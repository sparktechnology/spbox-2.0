package spbox.model.entity.email;

import com.fasterxml.jackson.databind.node.ObjectNode;
import play.libs.Json;

import javax.persistence.*;

@Entity
@Table(name="article", uniqueConstraints = {
        @UniqueConstraint(columnNames = "bzid"), @UniqueConstraint(columnNames = "name")})
public class Article {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long dbid;

    @Version
    public Long objectVersion;

    public String bzid;

    public String name;

    public String title;

    @Column(columnDefinition = "LONGTEXT")
    public String content;

    @Enumerated(EnumType.STRING)
    @Column(name = "article_type")
    public ArticleType articleType;

    public ObjectNode toJsonObject()
    {
        ObjectNode result = Json.newObject();
        result.put("bzid", this.bzid);
        result.put("name", this.name);
        result.put("title", this.title);
        result.put("content", this.content);
        return result;
    }

    public enum ArticleType {
        BUILTIN,  // Admin should not change builtin article's name
        GENERAL;  // General type article (further use)
    }
}
