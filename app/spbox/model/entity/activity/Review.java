package spbox.model.entity.activity;

import spbox.model.entity.user.User;
import spbox.web.utilities.TimeUtils;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="review")
public class Review {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long dbid;

    @Version
    public Long objectVersion;

    @ManyToOne(optional = false)
    @JoinColumn(name="activity_dbid")
    public Activity activity;

    @ManyToOne(optional = false)
    @JoinColumn(name="user_dbid")
    public User user;

    @Column(name = "review_content", columnDefinition = "LONGTEXT")
    public String reviewContent;

    @Column
    public Integer rating;

    @Column (name = "creation_time")
    public Date creationTime;

    @PrePersist
    void creationTime() {
        this.creationTime = TimeUtils.getUTCDate(new Date());
    }

}
