package spbox.model.entity.activity;

import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.IndexedEmbedded;
import spbox.model.entity.location.City;

import javax.persistence.*;

@Entity
@Table(name ="activity_venue")
public class ActivityVenue {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long dbid;

    @Version
    public Long objectVersion;

    @Field
    public String name;

    @Column(name="street_address")
    public String streetAddress;

    @Column(name="postal_code")
    public String postalCode;

    @IndexedEmbedded
    @ManyToOne(optional = false)
    @JoinColumn(name="city_dbid")
    public City city;


}
