package spbox.model.entity.activity;

import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import spbox.web.utilities.StringUtils;
import spbox.web.utilities.TimeUtils;

import javax.persistence.*;
import java.util.Date;


@Indexed(index="activity")
@Entity
@Table(name ="activity", uniqueConstraints = {
        @UniqueConstraint(columnNames = "bzid")})
public class Activity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long dbid;

    @Version
    public Long objectVersion;

    public String bzid;

    @Field
    public String name;

    @ManyToOne(optional = false)
    @JoinColumn(name="act_category_dbid")
    public ActivityCategory category;

    @Field
    @Enumerated(EnumType.STRING)
    public ActivityStatusEnum status;

    public boolean deleted = false;

    public boolean star = false;

    @Field
    @Column(name = "brief_description", columnDefinition = "LONGTEXT")
    public String briefDescription;

    @Field
    @Column(columnDefinition = "LONGTEXT")
    public String description;

    @OneToOne(cascade = CascadeType.ALL, optional = false)
    @JoinColumn(name="act_prop_dbid")
    public ActivityProperty property;

    @IndexedEmbedded
    @OneToOne(cascade = CascadeType.ALL, optional = false)
    @JoinColumn(name="act_venue_dbid")
    public ActivityVenue venue;

    @Column(name = "start_time")
    public Date startTime;

    @Column(name = "end_time")
    public Date endTime;

    @Column(name = "date_time")
    public String dateTime;

    public String organizer;

    @Column(name = "create_time")
    public Date createTime;

    @Column(name = "preferred_pub_time")
    public Date preferredPublishTime;

    @Column(name = "price_least")
    public Double priceLeast;

    @Column(name = "price_most")
    public Double priceMost;

    @OneToOne(cascade = CascadeType.ALL, optional = true)
    @JoinColumn(name="act_idx_dbid")
    public ActivityIndexPreviewImage indexPreviewImage;

    public String GetVenueBrief() {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.venue.name).append(", ").append(this.venue.city.EnName);

        return stringBuilder.toString();
    }

    public String GetVenueFull() {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.venue.name).append(", ");
        if(StringUtils.isNotBlank(this.venue.streetAddress)) {
            stringBuilder.append(this.venue.streetAddress).append(", ");
        }
        if(StringUtils.isNotBlank(this.venue.postalCode)){
            stringBuilder.append(this.venue.postalCode).append(", ");
        }

        stringBuilder.append(this.venue.city.EnName).append(", ")
                .append(this.venue.city.country.EnName);

        return stringBuilder.toString();
    }

    public Date GetStartDateinActivityLocalTimeZone(){
        return TimeUtils.getActivityLocalDate(this.startTime, this.venue.city.GetTimeZoneObject());
    }

    public Date GetEndDateinActivityLocalTimeZone(){
        return TimeUtils.getActivityLocalDate(this.endTime, this.venue.city.GetTimeZoneObject());
    }

    public enum ActivityStatusEnum {
        UNPUBLISH("草稿"), PUBLISH("已发布"), EXPIRED("已过期"), REQUEST_PUBLISH("待审核");

        private final String msg;

        ActivityStatusEnum(String msg){
            this.msg = msg;
        }

        public String getMsg(){
            return msg;
        }
    }

}
