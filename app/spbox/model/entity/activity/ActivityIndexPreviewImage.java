package spbox.model.entity.activity;

import spbox.model.s3.S3;

import javax.persistence.*;
import java.sql.Blob;
import java.sql.SQLException;

@Entity
@Table(name ="activity_index_preview_image")
public class ActivityIndexPreviewImage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long dbid;

    @Version
    public Long objectVersion;

    public String keyname;

    @Transient
    public String getContentString() throws SQLException {
        if (keyname == null)
        {
            return null;
        }
        String contentString =  "data:" + "jpeg" + ";base64,"
                + com.ning.http.util.Base64.encode(S3.download(keyname));

        return contentString;
    }
}
