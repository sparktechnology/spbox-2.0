package spbox.model.entity.activity;

import com.fasterxml.jackson.databind.node.ObjectNode;
import play.libs.Json;

import javax.persistence.*;

@Entity
@Table(name ="activity_category")
public class ActivityCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long dbid;

    @Version
    public Long objectVersion;

    public String CnName;

    public String EnName;

    public int idx;

    public ObjectNode toJsonObject()
    {
        ObjectNode result = Json.newObject();
        result.put("dbid", this.dbid);
        result.put("CnName", this.CnName);
        result.put("EnName", this.EnName);
        return result;
    }

}
