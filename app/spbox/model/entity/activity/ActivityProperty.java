package spbox.model.entity.activity;

import javax.persistence.*;

@Entity
@Table(name ="activity_prop")
public class ActivityProperty {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long dbid;

    @Version
    public Long objectVersion;

    public Boolean bname;

    public Boolean btel;

    public Boolean bgender;

    public Boolean bage;

    public Boolean baddress;

    public Boolean emailToBuyerEnabled;

    public Boolean emailToOrganizerEnabled;

    public Boolean emailCustomized;

}
