package spbox.model.entity.activity;

import spbox.model.s3.S3;

import javax.persistence.*;
import java.sql.Blob;
import java.sql.SQLException;

@Entity
@Table(name ="activity_image")
public class ActivityImage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long dbid;

    @Version
    public Long objectVersion;

    @Enumerated(EnumType.STRING)
    public ActivityImageCategoryEnum category;

    public String keyname;

    @ManyToOne(optional = false)
    @JoinColumn(name="activity_dbid")
    public Activity activity;

    public enum ActivityImageCategoryEnum {
        INDEX("封面"), INDEX_PREVIEW("封面"), AD("广告栏"), DESCRIPTION("详细描述"), DESCRIPTION_PREVIEW("详细描述");

        private final String msg;

        ActivityImageCategoryEnum(String msg){
            this.msg = msg;
        }

        public String getMsg(){
            return msg;
        }
    }

    @Transient
    public String getContentString() throws SQLException {
        if (keyname == null)
        {
            return null;
        }
        String contentString =  "data:" + "jpeg" + ";base64,"
                + com.ning.http.util.Base64.encode(S3.download(keyname));

        return contentString;
    }
}
