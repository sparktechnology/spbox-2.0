package spbox.model.entity.ticket;

import javax.persistence.*;

@Entity
@Table(name="ticket_type_man")
public class TicketTypeManual {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long dbid;

    @Version
    public Long objectVersion;

    @OneToOne(optional = false)
    @JoinColumn(name="ticket_type_dbid")
    public TicketType ticketType;

    @Column(name = "booked_quantity")
    public int bookedQuantity;

}
