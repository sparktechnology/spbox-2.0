package spbox.model.entity.ticket;

import spbox.model.entity.activity.Activity;
import spbox.model.entity.user.User;
import spbox.web.utilities.StringUtils;
import spbox.web.utilities.TimeUtils;

import javax.persistence.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@Entity
@Table(name="ticket")
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long dbid;

    @Version
    public Long objectVersion;

    public String bzid;

    @Column(name="purchase_time")
    public Date purchaseTime;

    public String title;

    public double price;

    public String seat;

    public boolean checked = false;

    @Column(name="checked_time")
    public Date checkedTime = null;

    @Column(name="checked_by")
    public String checkedBy = null;

    // To identify what tickets have been bought together
    @Column(name="bundle_id")
    public String bundleId;

    // The ticket is only valid when it is marked as paid
    public boolean paid = false;

    // This property make sure we could know the ticket is correctly return to ticket pool
    @Column(name="payment_failure_handled")
    public boolean paymentFailureHandled = false;

    public boolean deleted = false;

    @Column(columnDefinition = "LONGTEXT")
    public String qrcode;

    @ManyToOne(optional = false)
    @JoinColumn(name="user_dbid")
    public User user;

    @ManyToOne(optional = false)
    @JoinColumn(name="activity_dbid")
    public Activity activity;

    @ManyToOne
    @JoinColumn(name="ticket_type_dbid")
    public TicketType ticketType;

    public String GetCheckedTimeInCn(Date date) {
        if (StringUtils.isBlank(checkedBy)) {
            return StringUtils.BLANK;
        }
        DateFormat df = new SimpleDateFormat("yyyy年MM月dd日", Locale.CHINA);
        df.setTimeZone(activity.venue.city.GetTimeZoneObject());
        return df.format(date);
    }

    public Date GetPurchaseTimeInActivityLocal(){
        return TimeUtils.getActivityLocalDate(purchaseTime, activity.venue.city.GetTimeZoneObject());
    }
}
