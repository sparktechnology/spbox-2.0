package spbox.model.entity.ticket;

import spbox.model.entity.activity.Activity;

import javax.persistence.*;

@Entity
@Table(name="ticket_type")
public class TicketType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long dbid;

    @Version
    public Long objectVersion;

    public String bzid;

    @ManyToOne(optional = false)
    @JoinColumn(name="activity_dbid")
    public Activity activity;

    public String name;

    @Column(name = "total_quantity")
    public int totalQuantity;

    @Column(name = "booked_quantity")
    public int bookedQuantity;

    public Double price;

    @Enumerated(EnumType.ORDINAL)
    public TicketTypeCategoryEnum category;

    @Column(name = "original_price")
    public Double originalPrice;

    public String currency; // The currency of the amount (3-letter ISO code). The default is USD.

    public String seat;

    @Column(name = "tax_included")
    public boolean taxIncluded = false;

    public boolean deleted = false;

    public enum TicketTypeCategoryEnum {
        FREE("免费"), PAID("收费");

        private final String msg;

        TicketTypeCategoryEnum(String msg){
            this.msg = msg;
        }

        public String getMsg(){
            return msg;
        }
    }
}
