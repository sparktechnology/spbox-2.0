package spbox.model.entity.user;

import spbox.model.entity.PaymentMethod;

import javax.persistence.*;

@Entity
@Table(name="user_payment_requirement")
public class UserPaymentRequirement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long dbid;

    @Version
    public Long objectVersion;

    @ManyToOne(optional = false)
    @JoinColumn(name="payment_method_dbid")
    public PaymentMethod paymentMethod;

    @OneToOne(optional = false)
    @JoinColumn(name="user_dbid")
    public User user;

    public String paymentEmail;

}
