package spbox.model.entity.user;

import com.fasterxml.jackson.databind.node.ObjectNode;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import play.libs.Json;
import spbox.UserContext;

import javax.persistence.*;
import java.util.Date;

@Indexed(index="user")
@Entity
@Table(name="user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long dbid;

    @Version
    public Long objectVersion;

    @Field
    public String email;

    @Field
    public String name;

    public String bibliography;

    public String password;

    @Enumerated(EnumType.STRING)
    public UserRoleEnum role;

    public String mobile;

    public String gender;

    public int age;

    public String address;

    public boolean verified;

    public String facebookId;

    public String weixinId;

    public String googleId;

    @Column(name = "create_time")
    public Date createTime;

    @Column(name = "user_image", columnDefinition = "LONGTEXT")
    public String userImage;

    public ObjectNode toJsonObject()
    {
        ObjectNode result = Json.newObject();
        result.put("email", email);
        result.put("name", name);
        result.put("role", role.name());
        result.put("mobile", mobile);
        result.put("gender", gender);
        result.put("age", age);
        result.put("address", address);
        result.put("verified", verified);
        result.put("userImage", userImage);
        return result;
    }

    public enum UserRoleEnum {
        ADMIN("超级管理员"), ORGANIZER("活动管理员"), GENERAL("普通用户"), BLOCKED("已屏蔽");

        private final String msg;

        UserRoleEnum(String msg){
            this.msg = msg;
        }

        public String getMsg(){
            return msg;
        }
    }

    public static String GetCurrentUserEmail(){
        return UserContext.GetCurrentUserEmail();
    }

    public static String GetCurrentUserRole(){
        return UserContext.GetCurrentUserRole();
    }
}
