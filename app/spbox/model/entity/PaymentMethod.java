package spbox.model.entity;

import javax.persistence.*;

@Entity
@Table(name ="payment_method")
public class PaymentMethod {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long dbid;

    @Version
    public Long objectVersion;

    public String CnName;

    public String EnName;
}
