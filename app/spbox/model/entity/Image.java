package spbox.model.entity;

import javax.persistence.*;

@Entity
@Table(name ="image")
public class Image {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long dbid;

    @Version
    public Long objectVersion;

    public String bzid;

    public String contentType;

    @Lob
    public byte[] data;
}
