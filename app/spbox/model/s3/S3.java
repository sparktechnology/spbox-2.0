package spbox.model.s3;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.Upload;
import com.amazonaws.util.IOUtils;
import play.Logger;
import play.Play;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.List;

public class S3 {

    private static final AmazonS3 s3Client =  new AmazonS3Client(awsCredential());
    private static final TransferManager s3TransferManager = new TransferManager(awsCredential());

    public static BasicAWSCredentials awsCredential(){
        BasicAWSCredentials awsCredentials = null;
        try {
            awsCredentials = new BasicAWSCredentials(Play.application().configuration().getString("aws.credential.id"), Play.application().configuration().getString("aws.credential.secrete"));

        } catch (Exception e) {
            Logger.error(
                    "Cannot load the credentials from the credential profiles file. " +
                            "Please make sure that your credentials file is at the correct " +
                            "location (~/.aws/credentials), and is in valid format.",
                    e);
        }

        return awsCredentials;
    }

    public static void upload(String keyName, byte[] buffer){
        InputStream is = new ByteArrayInputStream(buffer);
        ObjectMetadata meta = new ObjectMetadata();
        meta.setContentLength(buffer.length);
        try {
            s3Client.putObject(new PutObjectRequest(Play.application().configuration().getString("aws.s3.bucketName"), keyName, is, meta));
            System.out.println("Upload complete.");
        } catch (AmazonClientException amazonClientException) {
            System.out.println("Unable to upload file, upload was aborted.");
            amazonClientException.printStackTrace();
        } catch (Exception exception) {
            System.out.println("Unable to upload file, upload was aborted.");
            exception.printStackTrace();
        }
    }

    public static void upload(String keyName, File file){
        Upload upload = s3TransferManager.upload(Play.application().configuration().getString("aws.s3.bucketName"), keyName, file);

        try {
            // Or you can block and wait for the upload to finish
            upload.waitForCompletion();
            System.out.println("Upload complete.");
        } catch (AmazonClientException amazonClientException) {
            System.out.println("Unable to upload file, upload was aborted.");
            amazonClientException.printStackTrace();
        } catch (Exception exception) {
            System.out.println("Unable to upload file, upload was aborted.");
            exception.printStackTrace();
        }
    }

    public static byte[] download(String keyName){

        try {

            S3Object s3Object = s3Client.getObject(new GetObjectRequest(Play.application().configuration().getString("aws.s3.bucketName"), keyName));
            return IOUtils.toByteArray(s3Object.getObjectContent());

        }catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which" +
                    " means your request made it " +
                    "to Amazon S3, but was rejected with an error response" +
                    " for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
            return null;
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which means"+
                    " the client encountered " +
                    "an internal error while trying to " +
                    "communicate with S3, " +
                    "such as not being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
            return null;
        }catch (Exception exception) {
            System.out.println("Unable to download file, download was aborted.");
            exception.printStackTrace();
            return null;
        }
    }

    public static void delete(List<DeleteObjectsRequest.KeyVersion> keys){
        DeleteObjectsRequest multiObjectDeleteRequest = new DeleteObjectsRequest(Play.application().configuration().getString("aws.s3.bucketName"));
        multiObjectDeleteRequest.setKeys(keys);

        try {
            DeleteObjectsResult delObjRes = s3Client.deleteObjects(multiObjectDeleteRequest);
            System.out.format("Successfully deleted all the %s items.\n", delObjRes.getDeletedObjects().size());

        } catch (MultiObjectDeleteException e) {
            // Process exception.
        }
    }
}
