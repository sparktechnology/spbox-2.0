package spbox.cep;

import com.espertech.esper.client.EPAdministrator;
import com.espertech.esper.client.EPPreparedStatement;
import com.espertech.esper.client.EPStatement;
import com.espertech.esper.client.UpdateListener;

public class ComplexEvent {

    private EPStatement statement;

    public ComplexEvent(String epl, Object... objects){
        if(epl != null && !epl.trim().isEmpty()){

            EPAdministrator epAdministrator = ComplexEventProcessor.getEpProvider().getEPAdministrator();
            EPPreparedStatement preparedStatement = epAdministrator.prepareEPL(epl);
            if(objects != null && objects.length > 0){
                for(int i = 0; i< objects.length; i++){
                    preparedStatement.setObject(i+1, objects[i]);
                }
            }
            this.statement = epAdministrator.create(preparedStatement);
        }
    }

    public ComplexEvent addListener(UpdateListener updateListener){
        if(this.statement != null){
            this.statement.addListener(updateListener);
        }
        return this;
    }

    public void destroy(){
        if(this.statement != null){
            this.statement.destroy();
            this.statement = null;
        }
    }

}
