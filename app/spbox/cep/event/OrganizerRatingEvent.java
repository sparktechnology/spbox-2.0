package spbox.cep.event;


public class OrganizerRatingEvent {

    private String organizerEmail;
    private Integer rating;

    public OrganizerRatingEvent(String organizerEmail, Integer rating){
        this.organizerEmail = organizerEmail;
        this.rating = rating;
    }

    public String getOrganizerEmail() {
        return organizerEmail;
    }

    public Integer getRating() {
        return rating;
    }
}
