package spbox.cep.event;


import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import play.Play;
import spbox.cep.dynamodb.DynamoDB;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PageViewEvent {

    private String pageUrl;
    private String ipAddress;

    public PageViewEvent(String pageUrl, String ipAddress){
        this.pageUrl = pageUrl;
        this.ipAddress = ipAddress;
    }

    public String getPageUrl() {
        return this.pageUrl;
    }

    public String getIpAddress() {
        return this.ipAddress;
    }

    public void saveEventToDynamoDB(){
        if(Play.application().configuration().getBoolean("project.dynamo.save")) {

            Map<String, AttributeValue> item = new HashMap<String, AttributeValue>();
            item.put("ID", new AttributeValue().withS(UUID.randomUUID().toString()));
            item.put("pageUrl", new AttributeValue().withS(this.pageUrl));
            item.put("ipAddress", new AttributeValue().withS(this.ipAddress));

            DynamoDB.insert("spbox", item);
        }
    }
}
