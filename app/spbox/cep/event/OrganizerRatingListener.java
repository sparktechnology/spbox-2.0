package spbox.cep.event;

import com.espertech.esper.client.EventBean;
import com.espertech.esper.client.UpdateListener;
import spbox.OrganizerRate;


public class OrganizerRatingListener implements UpdateListener {

    @Override
    public void update(EventBean[] newEvents, EventBean[] oldEvents){
        for(EventBean event: newEvents){
            OrganizerRate.updateOrganizerRate((String) event.get("organizerEmail"), (Double) event.get("averageRate"));
        }
    }

}