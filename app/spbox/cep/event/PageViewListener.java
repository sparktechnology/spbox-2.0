package spbox.cep.event;

import com.espertech.esper.client.EventBean;
import com.espertech.esper.client.UpdateListener;
import spbox.PageViewCount;


public class PageViewListener implements UpdateListener {

    @Override
    public void update(EventBean[] newEvents, EventBean[] oldEvents){
        for(EventBean event: newEvents){
            PageViewCount.updatePageViewCount((String) event.get("pageUrl"), (Long) event.get("viewCount"));
        }
    }

}