package spbox.cep.dynamodb;


import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.*;
import com.amazonaws.services.dynamodbv2.util.Tables;
import play.Logger;
import play.Play;

import java.util.List;
import java.util.Map;

public class DynamoDB {

    private static final AmazonDynamoDBClient SPDynamo = init();

    private static AmazonDynamoDBClient init() {
        /*
         * The ProfileCredentialsProvider will return your [default]
         * credential profile by reading from the credentials file located at
         * (~/.aws/credentials).
         */
        BasicAWSCredentials awsCredentials = null;
        try {
            awsCredentials = new BasicAWSCredentials(Play.application().configuration().getString("aws.credential.id"), Play.application().configuration().getString("aws.credential.secrete"));

        } catch (Exception e) {
            Logger.error(
                    "Cannot load the credentials from the credential profiles file. " +
                            "Please make sure that your credentials file is at the correct " +
                            "location (~/.aws/credentials), and is in valid format.",
                    e);
        }
        AmazonDynamoDBClient dynamoDB = new AmazonDynamoDBClient(awsCredentials);
        Region usWest2 = Region.getRegion(Regions.US_WEST_2);
        dynamoDB.setRegion(usWest2);

        return dynamoDB;
    }

    public static void insert(String table, Map<String, AttributeValue> item) {
        try {
            if (Tables.doesTableExist(SPDynamo, table)) {
                PutItemRequest itemRequest = new PutItemRequest().withTableName(table).withItem(item);
                SPDynamo.putItem(itemRequest);
            }else{
                Logger.error("dynamo table does not exist");
            }
        }catch(AmazonServiceException ase){
            Logger.error(ase.getErrorMessage());
        }catch (AmazonClientException ace) {
            Logger.error(ace.getMessage());
        }
    }

    public static List<Map<String, AttributeValue>> scanTable(String table){

        ScanRequest scanRequest = new ScanRequest()
                .withTableName(table);
        ScanResult result = SPDynamo.scan(scanRequest);

        return result.getItems();
    }

}
