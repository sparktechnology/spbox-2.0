package spbox.cep;

import com.espertech.esper.client.Configuration;
import com.espertech.esper.client.EPRuntime;
import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPServiceProviderManager;
import spbox.cep.event.OrganizerRatingEvent;
import spbox.cep.event.PageViewEvent;

public class ComplexEventProcessor {

    private static EPRuntime epRuntime;
    private static EPServiceProvider epProvider;

    public static EPRuntime getEpRuntime(){
        if(epRuntime == null){
            epRuntime = getEpProvider().getEPRuntime();
        }
        return epRuntime;
    }

    public static EPServiceProvider getEpProvider(){
        if(epProvider == null){
            Configuration configuration = new Configuration();
            configuration.addEventType("PageViewEvent", PageViewEvent.class);
            configuration.addEventType("OrganizerRatingEvent", OrganizerRatingEvent.class);
            epProvider = EPServiceProviderManager.getProvider("spbox", configuration);
        }
        return epProvider;
    }

    public static void destoryProvider(){
        if(epProvider != null){
            epProvider.getEPAdministrator().destroyAllStatements();
            epProvider.destroy();
            epProvider = null;
        }
    }
}
