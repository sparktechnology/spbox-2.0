package spbox.cep;

import spbox.cep.event.OrganizerRatingListener;
import spbox.cep.event.PageViewListener;

public class EventMonitor {

    private static ComplexEvent pageViewMonitor;
    private static ComplexEvent organizerRatingMonitor;

    private static ComplexEvent buildPageViewMonitor(){
        ComplexEvent complexEvent = new ComplexEvent("select istream count(*) as viewCount, pageUrl, ipAddress from PageViewEvent group by pageUrl", (Object[]) null);
        complexEvent.addListener(new PageViewListener());
        return complexEvent;
    }

    private static ComplexEvent buildOrganizerRatingMonitor(){
        ComplexEvent complexEvent = new ComplexEvent("select istream avg(rating) as averageRate, organizerEmail from OrganizerRatingEvent group by organizerEmail", (Object[]) null);
        complexEvent.addListener(new OrganizerRatingListener());
        return complexEvent;
    }

    public static ComplexEvent getPageViewMonitor(){
        if(pageViewMonitor == null){
            pageViewMonitor = buildPageViewMonitor();
        }
        return pageViewMonitor;
    }

    public static ComplexEvent getOrganizerRatingMonitor(){
        if(organizerRatingMonitor == null){
            organizerRatingMonitor = buildOrganizerRatingMonitor();
        }
        return organizerRatingMonitor;
    }
}
