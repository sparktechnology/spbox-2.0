package spbox;


import spbox.model.entity.email.Article;
import spbox.model.service.email.ArticleService;

import javax.inject.Inject;

public class BuiltInArticles {

    @Inject
    private ArticleService articleService;

    public Article GetNotice() {

        Article notice = articleService.findOneByName("NOTICE");

        if (notice == null) {
            // We didn't create notice page, create it on demand
            notice = new Article();
            notice.articleType = Article.ArticleType.BUILTIN;
            notice.name = "NOTICE";
            notice.title = "NOTICE";
            notice.content = "<notice content>";

            articleService.doCreate(notice);
        }
        return notice;
    }

    public Article GetTerms() {
        Article terms = articleService.findOneByName("TERMS");

        if (terms == null) {
            // We didn't create terms page, create it on demand
            terms = new Article();
            terms.articleType = Article.ArticleType.BUILTIN;
            terms.name = "TERMS";
            terms.title = "TERMS";
            terms.content = "<Terms content>";

            articleService.doCreate(terms);
        }
        return terms;
    }

    public Article GetOrganizerTemplate() {
        Article template = articleService.findOneByNameAndType("NEW_TICKET_NOTIFICATION_TO_ORGANIZER", Article.ArticleType.BUILTIN);
        if (template == null) {
            template = new Article();
            template.articleType = Article.ArticleType.BUILTIN;
            template.name = "NEW_TICKET_NOTIFICATION_TO_ORGANIZER";
            template.title = "[{company_name}] - {activity_name} - 用户订购";
            template.content = "<div style=\"background-color: #f9f9f9; color:#666;\">\t<div style=\"min-width: 300px;  max-width: 800px; margin:0 auto auto; padding:3%;\">\t\t<p><a href=\"{official_site}\"><img src=\"{official_site}/assets/images/havefun_logo_square.png\" style=\"min-width: 50px;  max-width: 15%;  margin: 0px 80% 0px 0%;\"></a></p>\n" +
                    "\t\t<p style=\"font-size: 20px;  font-weight: 350;\">\t\t\tHI, {user} 恭喜您的 <br>    \t\t\t<a href=\"{activity_link}\">             {activity}             </a>            {type} 类别票务卖出{quantity}张\t\t</p>\n" +
                    "\t\t<hr style=\"border: 0;\t\theight: 0;\t\tborder-top: 1px solid rgba(0, 0, 0, 0.1);\t\tborder-bottom: 1px solid rgba(255, 255, 255, 0.3);\">\n" +
                    "\t\t<div style=\"background-color: #ededed;\t\tpadding: 5% 0; width: 100%;\">\t\t\t<div style=\"padding: 15px;\t\t\tborder-bottom: medium dotted #f9f9f9;\">\t\t\t\t<div style=\"font-size: 20px;\"> 票务卖出详情</div>\n" +
                    "<div style=\"font-size: 20px;\"><br></div>\n" +
                    "\t\t\t\t<div>客户{buyer}购买</div>\n" +
                    "\t\t\t</div>\n" +
                    "\t\t\t<div style=\"padding: 15px;\t\t\tborder-bottom: medium dotted #f9f9f9;\">\t\t\t\t<span style=\"width: 40%; display:inline-block;float:left;\">活动</span>\t\t\t\t<span style=\"width: 30%; display:inline-block;float:left;\">票类别</span>\t\t\t\t<span style=\"width: 20%; display:inline-block;float:left;\">数量</span>\t\t\t</div>\n" +
                    "\t\t\t<div style=\"padding: 15px;\t\t\tborder-bottom: medium dotted #f9f9f9;\">\t\t\t\t<span style=\" width: 40%; display:inline-block;\">{activity}</span>\t\t\t\t<span style=\" width: 30%; display:inline-block;\">{type} </span>\t\t\t\t<span style=\"width: 20%; display:inline-block;\">{quantity}张</span>\t\t\t</div>\n" +
                    "\t\t\t<div style=\"margin-top: 5%;\t\t\tfont-weight: 200;\t\t\tclear:both;\t\t\tfont-size: 12px;\t\t\tdisplay:block;\t\t\tposition: relative;\t\t\ttext-align: center;  \t\t\twidth: 100%;\">\t\t\tThis order is subject to {company_name_en} <a style=\"\t\t\ttext-decoration: none;\" href=\"{official_site}/about\">Terms of Service</a></div>\n" +
                    "\t\t</div>\n" +
                    "\t\t<div>\t\t\t<br>\t\t\t已售出票务可在管理操作中的票务管理查看与检票 <br><br>\t          如果您有任何问题 欢迎致电:<a href=\"tel:778-888-1961\">778-558-3900</a><br><br>\t          感谢您选用{company_name}购票。\t          <hr style=\"border: 0;\t\t\theight: 0;\t\t\tborder-top: 1px solid rgba(0, 0, 0, 0.1);\t\t\tborder-bottom: 1px solid rgba(255, 255, 255, 0.3);\">\n" +
                    "\t\t</div>\n" +
                    "\t\t <div style=\" display:block; clear:both; text-align:center;\"> \t\t  <br>          <div>             <a href=\"{official_site}\">              <img style=\"min-width: 300px;  max-width: 40%;\" src=\"{official_site}/assets/images/accont.png\">            </a>          </div>\n" +
                    "          <div>             <div style=\"font-size: 20px;\">管理账户</div>\n" +
                    "            <div>              <a href=\"{official_site}\">登陆</a>               账户去管理您的票务.            </div>\n" +
                    "          </div>\n" +
                    "          <div class=\"clear\"></div>\n" +
                    "          <hr style=\"border: 0;\t\theight: 0;\t\tborder-top: 1px solid rgba(0, 0, 0, 0.1);\t\tborder-bottom: 1px solid rgba(255, 255, 255, 0.3);\">\n" +
                    "        </div>\n" +
                    "        <div style=\"width:45%; float: left;\"> \t\t  <br>          <div>             <a href=\"{official_site}\">              <img style=\"min-width: 50px;  max-width: 35%; padding-left:10%;\" src=\"{official_site}/assets/images/creatEvent.png\">            </a>          </div>\n" +
                    "          <div>             <div style=\"font-size: 16px; margin-bottom:10px;\">创建自己的活动</div>\n" +
                    "              <div>每一个人都可以在{company_name}上发起活动或者买票。快来试试吧</div>\n" +
                    "              <p>                <a href=\"{official_site}\">更多信息</a>              </p>\n" +
                    "          </div>\n" +
                    "        </div>\n" +
                    "        <div style=\"width:45%; margin-left:10%;float: left;\"> \t\t  <br>          <div>             <a href=\"{official_site}\">              <img style=\"min-width: 50px;  max-width: 35%; padding-left:10%;\" src=\"{official_site}/assets/images/findMore.png\">            </a>          </div>\n" +
                    "          <div>             <div style=\"font-size: 16px; margin-bottom:10px;\">发现更多的活动</div>\n" +
                    "              <div>查找更多的活动，遇见更多的兴趣相同的朋友</div>\n" +
                    "              <p><a href=\"{official_site}\">马上登陆</a></p>\n" +
                    "          </div>\n" +
                    "                            </div>\n" +
                    "        <div style=\"clear: both;\"></div>\n" +
                    "        <br>        <hr style=\"border: 0;\t\theight: 0;\t\tborder-top: 1px solid rgba(0, 0, 0, 0.1);\t\tborder-bottom: 1px solid rgba(255, 255, 255, 0.3);\">\n" +
                    "\t\t<div style=\"margin-top: 5%;  font-weight: 200;  clear:both;  font-size: 12px;  display:block;  position: relative;  text-align: center;    width: 100%;\">\t\tThis email was sent to you by      <a href=\"{official_site}\">{company_name_en}</a> <br>      120B-13988 Maycrest Way Richmond<br>      <span class=\"glyphicon glyphicon-copyright-mark\" aria-hidden=\"true\"></span>       Copyright 2015 HaveFunIO Technology Inc.</div>\n" +
                    "\t</div>\n" +
                    "</div>";
            articleService.doCreate(template);

        }
        return template;
    }

    public Article GetNewActivityTemplate() {
        Article template = articleService.findOneByNameAndType("NEW_ACTIVITY_NOTIFICATION", Article.ArticleType.BUILTIN);
        if (template == null) {
            template = new Article();
            template.articleType = Article.ArticleType.BUILTIN;
            template.name = "NEW_ACTIVITY_NOTIFICATION";
            template.title = "[{company_name}] - {activity_name} - {activity_status}";
            template.content = "<div style=\"background-color: #f9f9f9; color:#666;\">\t<div style=\"min-width: 300px;  max-width: 800px; margin:0 auto auto; padding:3%;\">\t\t<p><a href=\"{official_site}\"><img src=\"{official_site}/assets/images/havefun_logo_square.png\" style=\"min-width: 50px;  max-width: 15%;  margin: 0px 80% 0px 0%;\"></a></p>\n" +
                    "\t\t<p style=\"font-size: 20px;  font-weight: 350;\">\t\t\tHI, {user} 恭喜您的 <br>    \t\t\t<a href=\"{activity_link}\">             {activity}             </a>\t\t\t活动已创建成功\t\t</p>\n" +
                    "\t\t<div style=\"background-color: #ededed;\t\tpadding: 5% 0; width: 100%;\">\t\t\t<div style=\"padding: 15px;\t\t\tborder-bottom: medium dotted #f9f9f9;\">\t\t\t\t<div style=\"font-size: 20px;\"> {activity}</div>\n" +
                    "\t\t\t\t<!-- @@ today's date -->\t\t\t\t<div>{date}</div>\n" +
                    "\t\t\t</div>\n" +
                    "\t\t\t<div style=\"padding: 15px;\t\t\tborder-bottom: medium dotted #f9f9f9;\">\t\t\t\t<span style=\"width: 30%; display:inline-block;float:left;\">票类别</span>\t\t\t\t<span style=\"width: 30%; display:inline-block;float:left;\">价格</span>\t\t\t\t<span style=\"width: 20%; display:inline-block;float:left;\">数量</span>\t\t\t</div>\n" +
                    "\t\t\t\t{ticket_type_list_begin}" +
                    "\t\t\t<div style=\"padding: 15px;\t\t\tborder-bottom: medium dotted #f9f9f9;\">\t\t\t\t<span style=\" width: 30%; display:inline-block;\">{type} </span>\t\t\t\t<span style=\" width: 30%; display:inline-block;\">{price} </span>\t\t\t\t<span style=\"width: 20%; display:inline-block;\">{quantity}张</span>\t\t\t</div>\n" +
                    "\t\t\t\t{ticket_type_list_end}" +
                    "\t\t</div>\n" +
                    "\t\t<div>\t\t\t<br>\t\t\t 感谢您选用{company_name}发布活动，请在活动管理中修改您的活动 <br><br>\t          如果您有任何问题 欢迎致电:<a href=\"tel:778-888-1961\">778-558-3900</a><br><br>\t        <hr style=\"border: 0;\t\t\theight: 0;\t\t\tborder-top: 1px solid rgba(0, 0, 0, 0.1);\t\t\tborder-bottom: 1px solid rgba(255, 255, 255, 0.3);\">\n" +
                    "\t\t</div>\n" +
                    "\t\t <div style=\" display:block; clear:both; text-align:center;\"> \t\t  <br>          <div>             <a href=\"{official_site}\">              <img style=\"min-width: 300px;  max-width: 40%;\" src=\"{official_site}/assets/images/accont.png\">            </a>          </div>\n" +
                    "          <div>             <div style=\"font-size: 20px;\">管理账户</div>\n" +
                    "            <div>              <a href=\"{official_site}\">登陆</a>               账户去管理您的活动.            </div>\n" +
                    "          </div>\n" +
                    "          <div class=\"clear\"></div>\n" +
                    "          <hr style=\"border: 0;\t\theight: 0;\t\tborder-top: 1px solid rgba(0, 0, 0, 0.1);\t\tborder-bottom: 1px solid rgba(255, 255, 255, 0.3);\">\n" +
                    "        </div>\n" +
                    "        <span style=\" width: 250px; display:inline-block;\">\t\t  <br>          <div>             <a href=\"{official_site}\">              <img style=\"min-width: 50px;  max-width: 35%; padding-left:10%;\" src=\"{official_site}/assets/images/creatEvent.png\">            </a>          </div>\n" +
                    "          <div>             <div style=\"font-size: 16px; margin-bottom:10px;\">创建自己的活动</div>\n" +
                    "              <div>每一个人都可以在{company_name}上发起活动或者买票。快来试试吧</div>\n" +
                    "              <p>                <a href=\"{official_site}\">更多信息</a>              </p>\n" +
                    "          </div>\n" +
                    "        </span>        <span style=\" width: 250px; display:inline-block; margin-left:10%\">\t\t  <br>          <div>             <a href=\"{official_site}\">              <img style=\"min-width: 50px;  max-width: 35%; padding-left:10%;\" src=\"{official_site}/assets/images/findMore.png\">            </a>          </div>\n" +
                    "          <div>             <div style=\"font-size: 16px; margin-bottom:10px;\">发现更多的活动</div>\n" +
                    "              <div>查找更多的活动，遇见更多的兴趣相同的朋友</div>\n" +
                    "              <p><a href=\"{official_site}\">马上登陆</a></p>\n" +
                    "          </div>\n" +
                    "                            </span>        <div style=\"clear: both;\"></div>\n" +
                    "        <br>        <hr style=\"border: 0;\t\theight: 0;\t\tborder-top: 1px solid rgba(0, 0, 0, 0.1);\t\tborder-bottom: 1px solid rgba(255, 255, 255, 0.3);\">\n" +
                    "\t\t<div style=\"margin-top: 5%;  font-weight: 200;  clear:both;  font-size: 12px;  display:block;  position: relative;  text-align: center;    width: 100%;\">\t\tThis email was sent to you by      <a href=\"{official_site}\">{company_name_en}</a> <br>      120B-13988 Maycrest Way Richmond<br>      <span class=\"glyphicon glyphicon-copyright-mark\" aria-hidden=\"true\"></span>       Copyright 2015 HaveFunIO Technology Inc.</div>\n" +
                    "\t</div>\n" +
                    "</div>";
            articleService.doCreate(template);


        }
        return template;
    }

    public Article GetNewTicketTemplate() {

        Article template = articleService.findOneByNameAndType("NEW_TICKET_NOTIFICATION", Article.ArticleType.BUILTIN);
        if (template == null) {

            template = new Article();
            template.articleType = Article.ArticleType.BUILTIN;
            template.name = "NEW_TICKET_NOTIFICATION";
            template.title = "[{company_name}] - {activity_name} - 活动门票";
            template.content = "<div style=\"background-color: #f9f9f9; color:#666;\">\t<div style=\"min-width: 300px;  max-width: 800px; margin:0 auto auto; padding:3%;\">\t\t<p><a href=\"{official_site}\"><img src=\"{official_site}/assets/images/havefun_logo_square.png\" style=\"min-width: 50px;  max-width: 15%;  margin: 0px 80% 0px 0%;\"></a></p>\n" +
                    "\t\t<p style=\"font-size: 20px;  font-weight: 350;\">\t\t\tHI, {user} 恭喜您的 <br>    \t\t\t<a href=\"{activity_link}\">             {activity}             </a>\t\t\t票务已购买成功\t\t</p>\n" +
                    "\t\t<p>\t\t\t<!-- @@ the organizer -->\t\t\t由 &nbsp;<span style=\"color:#888;\t\t\ttext-decoration: underline;\">{organizer}</span>&nbsp;主办\t\t</p>\n" +
                    "\t\t<hr style=\"border: 0;\t\theight: 0;\t\tborder-top: 1px solid rgba(0, 0, 0, 0.1);\t\tborder-bottom: 1px solid rgba(255, 255, 255, 0.3);\">\n" +
                    "\t\t<p style=\"font-size: 20px;  font-weight: 350;\">\t\t温馨留言，\t\t</p>\n" +
                    "\t\t<p>\t\t\t感谢您的订购，活动中见！ 如若您有任何问题欢迎联系主办方 {organizer}。<a href=\"mailto:{organizer_email}\">{organizer_email}</a>\t\t</p>\n" +
                    "\t\t<hr style=\"border: 0;\t\theight: 0;\t\tborder-top: 1px solid rgba(0, 0, 0, 0.1);\t\tborder-bottom: 1px solid rgba(255, 255, 255, 0.3);\">\n" +
                    "\t\t<div style=\"background-color: #ededed;\t\tpadding: 5% 0; width: 100%;\">\t\t\t<div style=\"padding: 15px;\t\t\tborder-bottom: medium dotted #f9f9f9;\">\t\t\t\t<div style=\"font-size: 20px;\"> 票务</div>\n" +
                    "\t\t\t\t<!-- @@ today's date -->\t\t\t\t<div>{date}</div>\n" +
                    "\t\t\t</div>\n" +
                    "\t\t\t<div style=\"padding: 15px;\t\t\tborder-bottom: medium dotted #f9f9f9;\">\t\t\t\t<span style=\"width: 40%; display:inline-block;float:left;\">活动</span>\t\t\t\t<span style=\"width: 30%; display:inline-block;float:left;\">票类别</span>\t\t\t\t<span style=\"width: 20%; display:inline-block;float:left;\">数量</span>\t\t\t</div>\n" +
                    "\t\t\t<div style=\"padding: 15px;\t\t\tborder-bottom: medium dotted #f9f9f9;\">\t\t\t\t<span style=\" width: 40%; display:inline-block;\">{activity}</span>\t\t\t\t<span style=\" width: 30%; display:inline-block;\">{type} </span>\t\t\t\t<span style=\"width: 20%; display:inline-block;\">{quantity}张</span>\t\t\t</div>\n" +
                    "\t\t\t{tickets_begin}\t\t\t<div style=\"padding: 15px;\t\t\tborder-bottom: medium dotted #f9f9f9;\">\t\t\t<!-- @@ detailed ticket information -->\t\t\t\t<div style=\"text-align:center;\t\t\t\tmargin: 0 auto; padding: 3% 0;\"><a href=\"{ticket_verify_link}\">票号： {ticket_number}</a></div>\n" +
                    "\t\t\t</div>\n" +
                    "   \t\t\t{tickets_end}\t\t\t<div style=\"margin-top: 5%;\t\t\tfont-weight: 200;\t\t\tclear:both;\t\t\tfont-size: 12px;\t\t\tdisplay:block;\t\t\tposition: relative;\t\t\ttext-align: center;  \t\t\twidth: 100%;\">\t\t\tThis order is subject to {company_name_en} <a style=\"\t\t\ttext-decoration: none;\" href=\"{official_site}/about\">Terms of Service</a></div>\n" +
                    "\t\t</div>\n" +
                    "\t\t<div>\t\t\t<br>\t\t\t 票已自动加到您{company_name}的票夹中，请到{company_name}票夹中查看。您可以长按二维码保存图片作为票据 <br><br>\t          如果您有任何问题 欢迎致电:<a href=\"tel:778-888-1961\">778-558-3900</a><br><br>\t          感谢您选用{company_name}购票，活动中间。\t          <hr style=\"border: 0;\t\t\theight: 0;\t\t\tborder-top: 1px solid rgba(0, 0, 0, 0.1);\t\t\tborder-bottom: 1px solid rgba(255, 255, 255, 0.3);\">\n" +
                    "\t\t</div>\n" +
                    "\t\t <div style=\" display:block; clear:both; text-align:center;\"> \t\t  <br>          <div>             <a href=\"{official_site}\">              <img style=\"min-width: 300px;  max-width: 40%;\" src=\"{official_site}/assets/images/accont.png\">            </a>          </div>\n" +
                    "          <div>             <div style=\"font-size: 20px;\">管理账户</div>\n" +
                    "            <div>              <a href=\"{official_site}\">登陆</a>               账户去管理您的票务.            </div>\n" +
                    "          </div>\n" +
                    "          <div class=\"clear\"></div>\n" +
                    "          <hr style=\"border: 0;\t\theight: 0;\t\tborder-top: 1px solid rgba(0, 0, 0, 0.1);\t\tborder-bottom: 1px solid rgba(255, 255, 255, 0.3);\">\n" +
                    "        </div>\n" +
                    "        <span style=\" width: 250px; display:inline-block;\">\t\t  <br>          <div>             <a href=\"{official_site}\">              <img style=\"min-width: 50px;  max-width: 35%; padding-left:10%;\" src=\"{official_site}/assets/images/creatEvent.png\">            </a>          </div>\n" +
                    "          <div>             <div style=\"font-size: 16px; margin-bottom:10px;\">创建自己的活动</div>\n" +
                    "              <div>每一个人都可以在{company_name}上发起活动或者买票。快来试试吧</div>\n" +
                    "              <p>                <a href=\"{official_site}\">更多信息</a>              </p>\n" +
                    "          </div>\n" +
                    "        </span>        <span style=\" width: 250px; display:inline-block; margin-left:10%\">\t\t  <br>          <div>             <a href=\"{official_site}\">              <img style=\"min-width: 50px;  max-width: 35%; padding-left:10%;\" src=\"{official_site}/assets/images/findMore.png\">            </a>          </div>\n" +
                    "          <div>             <div style=\"font-size: 16px; margin-bottom:10px;\">发现更多的活动</div>\n" +
                    "              <div>查找更多的活动，遇见更多的兴趣相同的朋友</div>\n" +
                    "              <p><a href=\"{official_site}\">马上登陆</a></p>\n" +
                    "          </div>\n" +
                    "                            </span>        <div style=\"clear: both;\"></div>\n" +
                    "        <br>        <hr style=\"border: 0;\t\theight: 0;\t\tborder-top: 1px solid rgba(0, 0, 0, 0.1);\t\tborder-bottom: 1px solid rgba(255, 255, 255, 0.3);\">\n" +
                    "\t\t<div style=\"margin-top: 5%;  font-weight: 200;  clear:both;  font-size: 12px;  display:block;  position: relative;  text-align: center;    width: 100%;\">\t\tThis email was sent to you by      <a href=\"{official_site}\">{company_name_en}</a> <br>      120B-13988 Maycrest Way Richmond<br>      <span class=\"glyphicon glyphicon-copyright-mark\" aria-hidden=\"true\"></span>       Copyright 2015 HaveFunIO Technology Inc.</div>\n" +
                    "\t</div>\n" +
                    "</div>";
            articleService.doCreate(template);
        }
        return template;

    }

    public Article GetUserConfirmTemplate() {

        Article template = articleService.findOneByNameAndType("EMAIL_CONFIRM", Article.ArticleType.BUILTIN);
        if (template == null) {
            template = new Article();
            template.articleType = Article.ArticleType.BUILTIN;
            template.name = "EMAIL_CONFIRM";
            template.title = "[{company_name}] - 用户验证";
            template.content = "<div style=\"background-color: rgb(249, 249, 249);\">\t<div style=\"min-width: 300px; margin: 0px auto auto; padding: 3%;\">\t\t<p style=\"color: rgb(102, 102, 102);\"><a href=\"{official_site}\"><img src=\"{official_site}/assets/images/havefun_logo_square.png\" style=\"min-width: 50px;  max-width: 15%;  margin: 0px 80% 0px 0%;\"></a></p>\n" +
                    "\t\t<p style=\"color: rgb(248, 166, 58); font-size: 20px;\">\t\t\t亲爱的 {user},\t\t</p>\n" +
                    "\t\t<p><span style=\"color: rgb(102, 102, 102);\">\t\t\t您的{company_name}账户已经创建成功。</span><br><span style=\"color: rgb(102, 102, 102);\">\t\t\t请您点击以下连接验证邮箱：<a href=\"{link_url}\">马上验证</a></span></p>\n" +
                    "\t\t<hr style=\"color: rgb(102, 102, 102); border-width: 1px 0px; border-right-style: initial; border-left-style: initial; border-right-color: initial; border-left-color: initial; border-image-source: initial; border-image-slice: initial; border-image-width: initial; border-image-outset: initial; border-image-repeat: initial; height: 0px; border-top-style: solid; border-top-color: rgba(0, 0, 0, 0.0980392); border-bottom-style: solid; border-bottom-color: rgba(255, 255, 255, 0.298039);\">\n" +
                    "\t\t<p style=\"color: rgb(102, 102, 102); font-size: 20px;\">\t\t温馨提示，\t\t</p>\n" +
                    "\t\t<p style=\"color: rgb(102, 102, 102);\">\t\t\t如若以后您需要更改密码，可以在{company_name}顶部的修改密码进行更改。\t\t</p>\n" +
                    "\t\t\t\t<div style=\"color: rgb(102, 102, 102);\">           <p>感谢您选用{company_name}</p>\n" +
                    "          <div class=\"signiture\"><img src=\"{official_site}/assets/images/signiture.png\" style=\" min-width: 60px; max-width: 15%;\"></div>\n" +
                    "                    <div>{company_name}团队上</div>\n" +
                    "        </div>\n" +
                    "        <hr style=\"color: rgb(102, 102, 102); border-width: 1px 0px; border-right-style: initial; border-left-style: initial; border-right-color: initial; border-left-color: initial; border-image-source: initial; border-image-slice: initial; border-image-width: initial; border-image-outset: initial; border-image-repeat: initial; height: 0px; border-top-style: solid; border-top-color: rgba(0, 0, 0, 0.0980392); border-bottom-style: solid; border-bottom-color: rgba(255, 255, 255, 0.298039);\">\n" +
                    "\t\t<div style=\"color: rgb(102, 102, 102); margin-top: 5%; font-weight: 200; clear: both; font-size: 12px; display: block; position: relative; text-align: center; width: 100%;\">\t\t\t如果您没有试图验证您的{company_name}账户，请无视这封邮件\t\t</div>\n" +
                    "\t\t<hr style=\"color: rgb(102, 102, 102); border-width: 1px 0px; border-right-style: initial; border-left-style: initial; border-right-color: initial; border-left-color: initial; border-image-source: initial; border-image-slice: initial; border-image-width: initial; border-image-outset: initial; border-image-repeat: initial; height: 0px; border-top-style: solid; border-top-color: rgba(0, 0, 0, 0.0980392); border-bottom-style: solid; border-bottom-color: rgba(255, 255, 255, 0.298039);\">\n" +
                    "\t\t<div style=\"color: rgb(102, 102, 102); margin-top: 5%; font-weight: 200; clear: both; font-size: 12px; display: block; position: relative; text-align: center; width: 100%;\">\t\tThis email was sent to you by      <a href=\"{official_site}\">{company_name_en}</a> <br>      120B-13988 Maycrest Way Richmond<br>      <span class=\"glyphicon glyphicon-copyright-mark\" aria-hidden=\"true\"></span>       Copyright 2015 HaveFunIO Technology Inc.</div>\n" +
                    "\t</div>\n" +
                    "</div>";
            articleService.doCreate(template);
        }
        return template;

    }

    public Article GetAdminGeneratedUserConfirmTemplate() {

        Article template = articleService.findOneByNameAndType("EMAIL_CONFIRM_BY_ADMIN", Article.ArticleType.BUILTIN);
        if (template == null) {
            template = new Article();
            template.articleType = Article.ArticleType.BUILTIN;
            template.name = "EMAIL_CONFIRM_BY_ADMIN";
            template.title = "[{company_name}] - 用户验证";
            template.content = "<div style=\"background-color: #f9f9f9; color:#666;\">\t<div style=\"min-width: 300px; max-width: 500; margin:0 auto auto; padding:3%;\">\t\t<p><a href=\"{official_site}\"><img src=\"{official_site}/assets/images/icon_circle.png\" style=\"min-width: 50px;  max-width: 15%;  margin: 0px 80% 0px 0%;\"></a></p>\n" +
                    "\t\t<p style=\"font-size: 20px;  font-weight: 350; color: #f8a63a;\">\t\t\t亲爱的 {user},\t\t</p>\n" +
                    "\t\t<p>\t\t\t您的{company_name}账户已经创建，初始密码为：{pwdinfo}<br>\t\t\t请您点击以下连接验证邮箱：\t\t\t<span style=\"color:#7ebc42\">              <a href=\"{link_url}\">马上验证 </a>             </span>\t\t</p>\n" +
                    "\t\t或点击一下链接更改密码: \t\t\t<span style=\"color:#7ebc42\">              <a href=\"{password_reset_link_url}\">马上更改 </a>             </span>\t\t</p>\n"+
                    "\t\t<hr style=\"border: 0;\t\theight: 0;\t\tborder-top: 1px solid rgba(0, 0, 0, 0.1);\t\tborder-bottom: 1px solid rgba(255, 255, 255, 0.3);\">\n" +
                    "\t\t<p style=\"font-size: 20px;  font-weight: 350;\">\t\t温馨提示，\t\t</p>\n" +
                    "\t\t<p>\t\t\t如若以后您需要更改密码，可以在{company_name}顶部的修改密码进行更改。\t\t</p>\n" +
                    "\t\t\t\t<div>           <p>感谢您选用{company_name}</p>\n" +
                    "          <div class=\"signiture\"><img src=\"{official_site}/assets/images/signiture.png\" style=\" min-width: 60px; max-width: 15%;\"></div>\n" +
                    "                    <div>{company_name}团队上</div>\n" +
                    "        </div>\n" +
                    "        <hr style=\"border: 0;\t\theight: 0;\t\tborder-top: 1px solid rgba(0, 0, 0, 0.1);\t\tborder-bottom: 1px solid rgba(255, 255, 255, 0.3);\">\n" +
                    "\t\t<div style=\"margin-top: 5%;  font-weight: 200;  clear:both;  font-size: 12px;  display:block;  position: relative;  text-align: center;    width: 100%;\">\t\t\t如果您没有试图验证您的{company_name}账户，请无视这封邮件\t\t</div>\n" +
                    "\t\t<hr style=\"border: 0;\t\theight: 0;\t\tborder-top: 1px solid rgba(0, 0, 0, 0.1);\t\tborder-bottom: 1px solid rgba(255, 255, 255, 0.3);\">\n" +
                    "\t\t<div style=\"margin-top: 5%;  font-weight: 200;  clear:both;  font-size: 12px;  display:block;  position: relative;  text-align: center;    width: 100%;\">\t\tThis email was sent to you by      <a href=\"{official_site}\">{company_name_en}</a> <br>      120B-13988 Maycrest Way Richmond<br>      <span class=\"glyphicon glyphicon-copyright-mark\" aria-hidden=\"true\"></span>       Copyright 2015 HaveFunIO Technology Inc.</div>\n" +
                    "\t</div>\n" +
                    "</div>";
            articleService.doCreate(template);
        }
        return template;

    }

    public Article GetResetPasswordTemplate() {

        Article template = articleService.findOneByNameAndType("FORGET_PASSWORD", Article.ArticleType.BUILTIN);

        if (template == null) {
            template = new Article();
            template.articleType = Article.ArticleType.BUILTIN;
            template.name = "FORGET_PASSWORD";
            template.title = "[{company_name}] - 密码重置";
            template.content = "<div style=\"background-color: #f9f9f9; color:#666;\">\t<div style=\"min-width: 300px; max-width: 500; margin:0 auto auto; padding:3%;\">\t\t<p><a href=\"{official_site}\"><img src=\"{official_site}/assets/images/havefun_logo_square.png\" style=\"min-width: 50px;  max-width: 15%;  margin: 0px 80% 0px 0%;\"></a></p>\n" +
                    "\t\t<p style=\"font-size: 20px;  font-weight: 350; color: #f8a63a;\">\t\t\t亲爱的 {user},\t\t</p>\n" +
                    "\t\t<p><span style=\"line-height: 1.42857143;\">请您点击以下链接更改密码：\t\t\t</span><span style=\"line-height: 1.42857143; color: rgb(126, 188, 66);\">              <a href=\"{link_url}\">马上更改</a></span><br></p>\n" +
                    "<p><span style=\"color:#7ebc42\"><a href=\"{link_url}\"> </a>             </span>\t\t</p>\n" +
                    "\t\t<hr style=\"border: 0;\t\theight: 0;\t\tborder-top: 1px solid rgba(0, 0, 0, 0.1);\t\tborder-bottom: 1px solid rgba(255, 255, 255, 0.3);\">\n" +
                    "\t\t<p style=\"font-size: 20px;  font-weight: 350;\">\t\t温馨提示，\t\t</p>\n" +
                    "\t\t<p><span style=\"line-height: 1.42857143;\">该链接只有24小时的有效时间</span>。\t\t</p>\n" +
                    "\t\t\t\t<div>           <p>感谢您选用{company_name}</p>\n" +
                    "          <div class=\"signiture\"><img src=\"{official_site}/assets/images/signiture.png\" style=\" min-width: 60px; max-width: 15%;\"></div>\n" +
                    "                    <div>{company_name}团队上</div>\n" +
                    "        </div>\n" +
                    "        <hr style=\"border: 0;\t\theight: 0;\t\tborder-top: 1px solid rgba(0, 0, 0, 0.1);\t\tborder-bottom: 1px solid rgba(255, 255, 255, 0.3);\">\n" +
                    "\t\t<div style=\"margin-top: 5%;  font-weight: 200;  clear:both;  font-size: 12px;  display:block;  position: relative;  text-align: center;    width: 100%;\">\t\t\t如果您没有试图重置您的{company_name}密码，请无视这封邮件\t\t</div>\n" +
                    "\t\t<hr style=\"border: 0;\t\theight: 0;\t\tborder-top: 1px solid rgba(0, 0, 0, 0.1);\t\tborder-bottom: 1px solid rgba(255, 255, 255, 0.3);\">\n" +
                    "\t\t<div style=\"margin-top: 5%;  font-weight: 200;  clear:both;  font-size: 12px;  display:block;  position: relative;  text-align: center;    width: 100%;\">\t\tThis email was sent to you by      <a href=\"{official_site}\">{company_name_en}</a> <br>      120B-13988 Maycrest Way Richmond<br>      <span class=\"glyphicon glyphicon-copyright-mark\" aria-hidden=\"true\"></span>       Copyright 2015 HaveFunIO Technology Inc.</div>\n" +
                    "\t</div>\n" +
                    "</div>";
            articleService.doCreate(template);
        }
        return template;

    }

    public Article GetCreateActivityTerm() {

        Article term = articleService.findOneByNameAndType("CREATE_ACTIVITY_TERM", Article.ArticleType.BUILTIN);
        if (term == null) {
            // We didn't create terms page, create it on demand
            term = new Article();
            term.articleType = Article.ArticleType.BUILTIN;
            term.name = "CREATE_ACTIVITY_TERM";
            term.title = "服务条款";
            term.content = "<p class=\"p1\"><span class=\"s1\">对用户因使用本网站而产生的与第三方之间的纠纷，本网站不负任何责任。</span></p>\n" +
                    "<p class=\"p1\"><span class=\"s1\">本服务条款的最终解释权归HaveFunIO Technologies Inc. 公司所有</span></p>";
            articleService.doCreate(term);
        }
        return term;

    }

    public Article GetEmailTemplate(String activityBzid, Boolean findCustomized)
    {
        if(findCustomized){
            Article article = articleService.findOneByName("EMAIL_CUSTOMIZED_" + activityBzid.toUpperCase());
            if(article == null){
                Article builtIn = GetNewTicketTemplate();
                Article customizedArticle = new Article();
                customizedArticle.articleType = Article.ArticleType.GENERAL;
                customizedArticle.name = "EMAIL_CUSTOMIZED_" + activityBzid.toUpperCase();
                customizedArticle.title = builtIn.title;
                customizedArticle.content = builtIn.content;
                articleService.doCreate(customizedArticle);
                return customizedArticle;
            }
            return article;
        }
        return GetNewTicketTemplate();
    }

    public void SetEmailTemplate(String activityBzid, String content){
        Article article = articleService.findOneByName("EMAIL_CUSTOMIZED_" + activityBzid.toUpperCase());
        if(article == null){
            Article builtIn = GetNewTicketTemplate();
            Article customizedArticle = new Article();
            customizedArticle.articleType = Article.ArticleType.GENERAL;
            customizedArticle.name = "EMAIL_CUSTOMIZED_" + activityBzid.toUpperCase();
            customizedArticle.title = builtIn.title;
            customizedArticle.content = content;
            articleService.doCreate(customizedArticle);
        }
        else{
            article.content = content;
            articleService.doUpdate(article);
        }
    }
}
