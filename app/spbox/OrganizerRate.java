package spbox;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class OrganizerRate {

    private static Map<String, Double> organizerRate;
    private static final Double MAX_RATE = 5.0;

    public static void updateOrganizerRate(String organizerEmail, Double averageRate){

        if(organizerRate == null){
            organizerRate = new ConcurrentHashMap<String, Double>();
        }

        organizerRate.put(organizerEmail, averageRate);
    }

    public static Map<String, Double> getOrganizerRate(){
        return organizerRate;
    }

    public static Double getOrganizerRate(String organizerEmail){
        if(organizerRate != null && organizerRate.containsKey(organizerEmail))
            return organizerRate.get(organizerEmail);
        else
            return MAX_RATE;
    }
}
