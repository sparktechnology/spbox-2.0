import com.github.play2war.plugin._

name := """WeTicket"""

version := "3.5-BETA"

Play2WarPlugin.play2WarSettings

Play2WarKeys.servletVersion := "3.1"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.7"

routesGenerator := InjectedRoutesGenerator

PlayKeys.externalizeResources := false

excludeFilter in (Assets, LessKeys.less) := "_*.less"

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs,
  javaJpa,
  "org.hibernate" % "hibernate-search-orm" % "5.5.0.Final",
  "org.hibernate" % "hibernate-entitymanager" % "5.0.4.Final",
  "com.espertech" % "esper" % "5.3.0",
  "mysql" % "mysql-connector-java" % "5.1.37",
  "com.google.zxing" % "core" % "3.2.0",
  "com.stripe" % "stripe-java" % "1.38.0",
  "com.typesafe.play" %% "play-mailer" % "3.0.1",
  "com.amazonaws" % "aws-java-sdk-dynamodb" % "1.10.38",
  "joda-time" % "joda-time" % "2.9.1"
)

PlayKeys.fileWatchService := play.runsupport.FileWatchService.sbt(pollInterval.value)